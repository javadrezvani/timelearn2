<?php
require_once 'model/Mwidget4.php';
$class=new widget4();
switch ($action){
    case 'list':
        $widget=$class->widget4_tbl();
    break;

    case 'add':
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $folder="widget-".rand();
            $img=uploader("image","../public/uploader/",$folder,"widget");
            $class->add_widget4_tbl($data,$img);
            header("location:dashbord.php?c=widget4&a=list");
        }
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->show_edit_widget4_tbl($id);
        if($_POST){
            $data=$_POST['frm'];
            $oldpic=$result['image'];
            $class->widget4_edit($data,$id,'image',$oldpic);
            header("location:dashbord.php?c=widget4&a=list");
        }
        break;

    case 'delete':
        $id=$_GET['id'];
        $class->delete_widget4_tbl($id);
        header("location:dashbord.php?c=widget4&a=list");
    break;

}

require_once "view/".$controller."/".$action.".php";