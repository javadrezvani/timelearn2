<?php
include_once 'model/Mfooter.php';
$class=new footer();

switch ($action) {
    case 'add':
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->add_footer($data);
        }
    break;

    case 'list':
        $listfooter=$class->list_footer();
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->delete_footer($id);
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->show_edit_footer($id);
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->edit_footer($data,$id);
            header("location:dashbord.php?c=footer&a=list");
        }
    break;

}


require_once "view/".$controller."/".$action.".php";