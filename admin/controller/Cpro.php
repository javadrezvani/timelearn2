<?php
require_once 'model/Mpro.php';
$class=new pro();

switch($action){
    case 'list':
        $pro=$class->pro_list();
    break;

    case 'add':
        $res=$class->procat_list();
        $teachers=$class->teachers_list();
        if($_POST){
            $data=$_POST['frm'];
            $folder="pro-".rand();
            $img=uploader("image","../public/uploader/",$folder,"pro");
            $class->pro_add($data,$img);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->pro_delete($id);
        header("location:dashbord.php?c=pro&a=list");
    break;

    case 'edit':
        $id=$_GET['id'];
        $res=$class->procat_list();
        $result=$class->pro_showedit($id);
        $teachers=$class->teachers_list();
        if($_POST){
            $data=$_POST['frm'];
            $oldpic=$result['image'];
            $class->pro_edit($data,$id,'image',$oldpic);
            header("location:dashbord.php?c=pro&a=list");
        }
    break;
}

require_once 'view/'.$controller."/".$action.'.php';