<?php
include_once 'model/Mfiles.php';
include_once 'model/Mpro.php';
$files=new files();
$pro=new pro();


switch ($action){
    case 'list-learns':
        $res=$pro->pro_list();
    break;

    case 'list-files':
        $product=$_GET['product'];
        $res=$files->pro_learn($product);
        $result=$files->files_list($product);
    break;

    case 'add':
        $pro=$files->pro_tbl();
        if (isset($_POST['btn'])){
            $folder="file".rand();
            $data=$_POST['frm'];
            $file=uploader("files","../public/files/",$folder,"file");
            $files->add_file($data,$file);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $files->delete_file($id);
        $product=$_GET['product'];
        header("location:dashbord.php?c=files&a=list-files&product=$product");
    break;

    case 'edit':
        $pro=$files->pro_tbl();
        $product=$_GET['product'];
        $id=$_GET['id'];
        $result=$files->show_edit_file($id);
        $res=$files->pro_learn($product);
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $oldfile=$result['file'];
            $files->file_edit($data,$id,'files',$oldfile);
            header("location:dashbord.php?c=files&a=list-files&product=$product");
        }
    break;
}


require_once 'view/'.$controller."/".$action.'.php';