<?php
require_once 'model/Mpro.php';
$class=new pro();

switch($action){
    case 'list':
        $teacher=$class->teachers_list();
    break;

    case 'add':
        if($_POST){
            $data=$_POST['frm'];
            $folder="teacher-".rand();
            $img=uploader("image","../public/uploader/",$folder,"teacher");
            $class->teachers_add($data,$img);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->teachers_delete($id);
        header("location:dashbord.php?c=teachers&a=list");
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->teachers_showedit($id);
        if($_POST){
            $data=$_POST['frm'];
            $oldpic=$result['image'];
            $class->teachers_edit($data,$id,'image',$oldpic);
            header("location:dashbord.php?c=teachers&a=list");
        }
    break;
}

require_once 'view/'.$controller."/".$action.'.php';