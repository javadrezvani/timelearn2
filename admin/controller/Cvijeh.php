<?php
require_once 'model/Mvijeh.php';
$class=new vijeh();

switch($action){
    case 'list':
        $vijeh=$class->vijeh_list();
    break;

    case 'add':
        if($_POST){
            $data=$_POST['frm'];
            $folder="vijeh-".rand();
            $img=uploader("image","../public/uploader/",$folder,"vijeh");
            $class->vijeh_add($data,$img);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->vijeh_delete($id);
        header("location:dashbord.php?c=vijeh&a=list");
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->vijeh_showedit($id);
        if($_POST){
            $data=$_POST['frm'];
            $oldpic=$result['image'];
            $class->vijeh_edit($data,$id,'image',$oldpic);
            header("location:dashbord.php?c=vijeh&a=list");
        }
    break;
}

require_once 'view/'.$controller."/".$action.'.php';