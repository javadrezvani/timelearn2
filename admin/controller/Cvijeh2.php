<?php
require_once 'model/Mvijeh2.php';
$class=new vijeh2();

switch($action){
    case 'list':
        $vijeh=$class->vijeh2_list();
    break;

    case 'add':
        if($_POST){
            $data=$_POST['frm'];
            $folder="vijeh2-".rand();
            $video=uploader("image","../public/uploader/",$folder,"vijeh2");
            $class->vijeh2_add($data,$video);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->vijeh2_delete($id);
        header("location:dashbord.php?c=vijeh2&a=list");
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->vijeh2_showedit($id);
        if($_POST){
            $data=$_POST['frm'];
            $oldpic=$result['video'];
            $class->vijeh2_edit($data,$id,'video',$oldpic);
            header("location:dashbord.php?c=vijeh2&a=list");
        }
    break;
}

require_once 'view/'.$controller."/".$action.'.php';