<?php
include_once 'model/Mblog.php';
$class=new blog();

switch($action){
    case 'list':
        $blog=$class->blog_list();
    break;

    case 'add':
        $res=$class->blog_cat_list();
        if($_POST){
            $data=$_POST['frm'];
            $folder="blog-".rand();
            $date=date("Y M d");
            $data['date']=$date;
            $img=uploader("image","../public/uploader/",$folder,"blog");
            $class->blog_add($data,$img);
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->blog_delete($id);
        header("location:dashbord.php?c=blog&a=list");
    break;

    case 'detail':
        $id=$_GET['id'];
        $result=$class->blog_detail($id);
    break;

    case 'edit':
        $id=$_GET['id'];
        $res=$class->blog_cat_list();
        $result=$class->blog_showedit($id);
        if($_POST){
            $data=$_POST['frm'];
            $date=$result['date'];
            $data['date']=$date;
            $oldpic=$result['image'];
            $class->blog_edit($data,$id,'image',$oldpic);
            header("location:dashbord.php?c=blog&a=list");
        }
    break;
}

require_once 'view/'.$controller."/".$action.'.php';