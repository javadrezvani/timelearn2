<?php
require_once 'model/Mindex.php';
$class=new main();
switch ($action){
    case 'list':
        $listmenus=$class->menu();
        break;

    case 'add':
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->add_menu($data);
            header("location:dashbord.php?c=menus&a=list");
        }
        break;

    case 'delete':
        $id=$_GET['id'];
        $class->delete_menu($id);
        header("location:dashbord.php?c=menus&a=list");
        break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->show_edit_menus($id);
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->edit_menus($data,$id);
            header("location:dashbord.php?c=menus&a=list");
        }
        break;
}

require_once "view/".$controller."/".$action.".php";