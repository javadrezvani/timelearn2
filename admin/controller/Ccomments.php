<?php
include_once 'model/Mpro.php';
include_once 'model/Mcomment.php';
$pro=new pro();
$class=new comment();

switch ($action){
    case 'list-not-active':
        $result=$class->list_comment_admin_not_active();
    break;
    case 'list-active':
        $result=$class->list_comment_admin_active();
    break;

    case 'detail':
        $id=$_GET['id'];
        $result=$class->show_comment_not_active($id);
    break;

    case 'active':
        $id=$_GET['id'];
        $class->comment_ok($id);
        header("location:dashbord.php?c=comments&a=list-active");
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->comment_delete($id);
        header("location:dashbord.php?c=comments&a=list-not-active");
    break;

    case 'deleted':
        $id=$_GET['id'];
        $class->comment_delete($id);
        header("location:dashbord.php?c=comments&a=list-active");
    break;
}


require_once 'view/'.$controller."/".$action.'.php';