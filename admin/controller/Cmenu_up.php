<?php
require_once 'model/menu_up.php';
$class=new menu_up();
switch ($action){
    case 'list':
        $listmenus=$class->menu_up();
    break;

    case 'add':
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->add_menu_up($data);
            header("location:dashbord.php?c=menu_up&a=list");
        }
    break;

    case 'delete':
        $id=$_GET['id'];
        $class->delete_menu_up($id);
        header("location:dashbord.php?c=menu_up&a=list");
    break;

    case 'edit':
        $id=$_GET['id'];
        $result=$class->show_edit_menus_up($id);
        if (isset($_POST['btn'])){
            $data=$_POST['frm'];
            $class->edit_menus_up($data,$id);
            header("location:dashbord.php?c=menu_up&a=list");
        }
    break;
}

require_once "view/".$controller."/".$action.".php";