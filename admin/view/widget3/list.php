<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست نظرات افراد موفق
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>شغل</th>
                    <th> عکس</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($widget as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['name']; ?></td>
                        <td><?php echo $val['job']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><a href="dashbord.php?c=widget3&a=delete&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>

                </tbody>
            </table>
        </section>
    </div>
</div>
