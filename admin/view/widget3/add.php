<h1 class="pageLables">افزودن نظر افراد موفق </h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                افزودن نظر افراد موفق
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام</label>
                        <input type="text" name="frm[name]" class="form-control" placeholder="نام را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">شغل</label>
                        <input type="text" name="frm[job]" class="form-control" placeholder="شغل را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">عکس</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">نظر</label>
                        <textarea type="text" name="frm[text]" rows="3" class="form-control"></textarea>
                    </div>

                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>