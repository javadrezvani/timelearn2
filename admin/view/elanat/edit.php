<h1 class="pageLables">
    ویرایش اعلان وب سایت
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش اعلان
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان </label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $row['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن عنوان </label>
                        <input type="text" name="frm[titletext]" class="form-control" value="<?php echo $row['titletext']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن</label>
                        <textarea rows="10" type="text" id="editor1" name="frm[text]" class="form-control ckeditor"><?php echo $row['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">لینک</label>
                        <input type="text" name="frm[link]" class="form-control" value="<?php echo $row['link']; ?>">
                    </div>
                    <label for="exampleInputPassword1">وضعیت نمایش</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" id="optionsRadios1" value="1">فعال
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" id="optionsRadios1" value="0"> غیر فعال
                        </label>
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>
            </div>
        </section>
    </div>
</div>

