<h1 class="pageLables">
    ویرایش تنظیمات وب سایت
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش تنظیمات
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان </label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $row['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">درباره وب سایت </label>
                        <input type="text" name="frm[description]" class="form-control" value="<?php echo $row['description']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن فوتر </label>
                        <textarea rows="10" type="text" name="frm[text]" class="form-control"><?php echo $row['text']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">کلمات کلیدی وب سایت </label>
                        <input type="text" name="frm[keywords]" class="form-control" value="<?php echo $row['keywords']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لوگو وب سایت</label>
                        <input type="file" name="img" class="form-control">
                        <img src="<?php echo $row['logo']; ?>" width="60" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">کپی رایت</label>
                        <input type="text" name="frm[copyright]" class="form-control" value="<?php echo $row['copyright']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">اینستاگرام</label>
                        <input type="text" name="frm[instgram]" class="form-control" value="<?php echo $row['instgram']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">گوگل پلاس</label>
                        <input type="text" name="frm[googleplus]" class="form-control" value="<?php echo $row['googleplus']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">توییتر</label>
                        <input type="text" name="frm[twitter]" class="form-control" value="<?php echo $row['twitter']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">تلفن</label>
                        <input type="text" name="frm[tel]" class="form-control" value="<?php echo $row['tel']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ایمیل</label>
                        <input type="text" name="frm[address]" class="form-control" value="<?php echo $row['address']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ساعت کاری</label>
                        <input type="text" name="frm[timetamas]" class="form-control" value="<?php echo $row['timetamas']; ?>">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>
            </div>
        </section>
    </div>
</div>

