<h1 class="pageLables">افزودن ویجت </h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                افزودن ویجت
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان</label>
                        <input type="text" name="frm[title]" class="form-control" placeholder="عنوان را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن</label>
                        <textarea type="text" id="editor1" name="frm[text]" class="form-control" ckeditor></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">تصویر ویجت</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>