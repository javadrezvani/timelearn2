<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                نظرات فعال و تایید شده
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th> عنوان دوره</th>
                    <th> عکس دوره</th>
                    <th>ایمیل کاربر</th>
                    <th>نام کاربر</th>
                    <th>حذف نظر</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $val):
                    ?>
                    <tr>
                        <td><?php $procat=$pro->pro_detail($val['pro_id']); echo $procat['title'];?></td>
                        <td><img src="<?php echo $procat['image']; ?>" width="60"></td>
                        <td><?php echo $val['email']; ?></td>
                        <td><?php echo $val['user_name']; ?></td>
                        <td><a href="dashbord.php?c=comments&a=deleted&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-trash"></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










