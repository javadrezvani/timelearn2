<h1 class="pageLables">
    ویرایش اطلاعات ادمین
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش اطلاعات ادمین
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام</label>
                        <input type="text" name="frm[name]" class="form-control"  value="<?php echo $result['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputName">نام خانوادگی</label>
                        <input type="text" name="frm[lastname]" class="form-control" value="<?php echo $result['lastname']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">ایمیل</label>
                        <input type="text" name="frm[email]" class="form-control"  value="<?php echo $result['email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">تغییر رمز عبور</label>
                        <input type="text" name="frm[password]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">عکس ادمین</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

