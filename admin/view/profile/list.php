<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                اطلاعات ادمین
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>نام ادمین</th>
                    <th>نام خانوادگی ادمین</th>
                    <th>ایمیل (نام کاربری)</th>
                    <th>عکس ادمین</th>
                    <th>ویرایش</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['name']; ?></td>
                        <td><?php echo $val['lastname']; ?></td>
                        <td><?php echo $val['email']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><a href="dashbord.php?c=profile&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










