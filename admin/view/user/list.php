<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست کاربران وبسایت
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>نام کاربر</th>
                    <th>نام خانوادگی</th>
                    <th>ایمیل</th>
                    <th>شماره تلفن</th>
                    <th>جنسیت</th>
                    <th>عکس</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($user as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['name']; ?></td>
                        <td><?php echo $val['lastname']; ?></td>
                        <td><?php echo $val['email']; ?></td>
                        <td><?php echo $val['phone']; ?></td>
                        <td>
                            <?php
                                if ($val['gender']==1){
                                    echo "مرد";
                                }elseif ($val['gender']==2){
                                    echo "زن";
                                }elseif ($val['gender']==0){
                                    echo "نا مشخص";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if ($val['image']=='بدون عکس'){
                                    echo "بدون عکس";
                                }else{
                                    echo "<img src=\"../$val[image]\" width=\"50\">";
                                }
                            ?>
                        </td>
                        <td><a href="dashbord.php?c=user&a=delete&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










