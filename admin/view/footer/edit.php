<h1 class="pageLables">
    ویرایش دسترسی
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش دسترسی <?php echo $result['title']; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان منو</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک اول</label>
                        <input type="text" name="frm[link1]" class="form-control" value="<?php echo $result['link1']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک دوم</label>
                        <input type="text" name="frm[link2]" class="form-control" value="<?php echo $result['link2']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک سوم</label>
                        <input type="text" name="frm[link3]" class="form-control" value="<?php echo $result['link3']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک چهارم</label>
                        <input type="text" name="frm[link4]" class="form-control" value="<?php echo $result['link4']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک پنجم</label>
                        <input type="text" name="frm[link5]" class="form-control" value="<?php echo $result['link5']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک ششم</label>
                        <input type="text" name="frm[link6]" class="form-control" value="<?php echo $result['link6']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک هفتم</label>
                        <input type="text" name="frm[link7]" class="form-control" value="<?php echo $result['link7']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس اول</label>
                        <input type="text" name="frm[url1]" class="form-control" value="<?php echo $result['url1']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس دوم</label>
                        <input type="text" name="frm[url2]" class="form-control" value="<?php echo $result['url2']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس سوم</label>
                        <input type="text" name="frm[url3]" class="form-control" value="<?php echo $result['url3']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس چهارم</label>
                        <input type="text" name="frm[url4]" class="form-control" value="<?php echo $result['url4']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس پنجم</label>
                        <input type="text" name="frm[url5]" class="form-control" value="<?php echo $result['url5']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس ششم</label>
                        <input type="text" name="frm[url6]" class="form-control" value="<?php echo $result['url6']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس هفتم</label>
                        <input type="text" name="frm[url7]" class="form-control" value="<?php echo $result['url7']; ?>">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

