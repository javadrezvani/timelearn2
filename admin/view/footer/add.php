<h1 class="pageLables">افزودن دسترسی جدید</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                افزودن دسترسی جدید به وب سایت
            </header>
            <div class="panel-body">
                <form role="form" method="post" action="dashbord.php?c=footer&a=add">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دسترسی سریع</label>
                        <input type="text" name="frm[title]" class="form-control" placeholder="عنوان دسترسی را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک اول</label>
                        <input type="text" name="frm[link1]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک دوم</label>
                        <input type="text" name="frm[link2]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک سوم</label>
                        <input type="text" name="frm[link3]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک چهارم</label>
                        <input type="text" name="frm[link4]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک پنجم</label>
                        <input type="text" name="frm[link5]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک ششم</label>
                        <input type="text" name="frm[link6]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">لینک هفتم</label>
                        <input type="text" name="frm[link7]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس اول</label>
                        <input type="text" name="frm[url1]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس دوم</label>
                        <input type="text" name="frm[url2]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس سوم</label>
                        <input type="text" name="frm[url3]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس چهارم</label>
                        <input type="text" name="frm[url4]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس پنجم</label>
                        <input type="text" name="frm[url5]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس ششم</label>
                        <input type="text" name="frm[url6]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">آدرس هفتم</label>
                        <input type="text" name="frm[url7]" class="form-control">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>