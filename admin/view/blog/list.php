<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست دوره ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th> عنوان مقاله</th>
                    <th> تصویر مقاله</th>
                    <th>دسته بندی مقاله</th>
                    <th>زمان انتشار</th>
                    <th>نویسنده</th>
                    <th>نمایش</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($blog as $key=>$val):
                    ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $val['title']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><?php
                            $parent=$class->blog_list_catid($val['cat_id']);
                            echo $parent['title'];
                            ?>
                        </td>
                        <td><?php echo $val['date']; ?></td>
                        <td><?php echo $val['nevisandeh']; ?></td>
                        <td><a href="dashbord.php?c=blog&a=detail&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-share"></i></a></td>
                        <td><a href="dashbord.php?c=blog&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                        <td><a href="dashbord.php?c=blog&a=delete&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










