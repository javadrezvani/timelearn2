<h1 class="pageLables">
    ویرایش مقاله
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش مقاله <?php echo $result['title']; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن ادامه مطلب</label>
                        <textarea type="text" id="editor1" rows="5" name="frm[titletext]" class="form-control ckeditor"><?php echo $result['titletext']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن مقاله</label>
                        <textarea type="text" id="editor1" rows="20" name="frm[text]" class="form-control ckeditor"><?php echo $result['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر مقاله</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">نویسنده</label>
                        <input type="text" name="frm[nevisandeh]" class="form-control" value="<?php echo $result['nevisandeh']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">دسته بندی</label>
                        <select class="form-control input-lg m-bot15" name="frm[cat_id]">
                            <?php foreach ($res as $value): ?>
                                <option value="<?php echo $value['id']; ?>" <?php if ($result['cat_id']==$value['id']){echo " selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

