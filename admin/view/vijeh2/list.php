<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                پیشنهاد ویژه
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th> عنوان پیشنهاد</th>
                    <th>قیمت دوره</th>
                    <th>لینک محصول</th>
                    <th>ویرایش پیشنهاد</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($vijeh as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['title']; ?></td>
                        <td><?php echo $val['price']; ?></td>
                        <td><?php echo $val['more']; ?></td>
                        <td><a href="dashbord.php?c=vijeh2&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










