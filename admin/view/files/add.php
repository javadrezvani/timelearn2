
<h1 class="pageLables">آپلود فایل جدید</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                آپلود فایل جدید به دوره
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان فایل</label>
                        <input type="text" name="frm[title]" class="form-control" placeholder="عنوان فایل را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">زمان جلسه</label>
                        <input type="text" name="frm[time]" class="form-control">
                    </div>
                    <label for="exampleInputPassword1">وضعیت نمایش</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" id="optionsRadios1" value="0">رایگان
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" id="optionsRadios1" value="1"> غیر رایگان
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">فایل</label>
                        <input type="file" name="files" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">دوره آموزشی</label>
                        <select class="form-control input-lg m-bot15" name="frm[pro_id]">
                            <?php
                            foreach ($pro as $val){
                                echo "<option value=\"$val[id]\">$val[title]</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">جلسه :</label>
                        <input type="text" name="frm[jalase]" class="form-control">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">آپلود فایل</button>
                </form>
            </div>
        </section>
    </div>
</div>