<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست فایل دوره ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>جلسه</th>
                    <th> عنوان فایل</th>
                    <th>زمان فایل</th>
                    <th>وضعیت</th>
                    <th>دوره آموزشی</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['jalase']; ?></td>
                        <td><?php echo $val['title']; ?></td>
                        <td><?php echo $val['time']; ?></td>
                        <td>
                            <?php
                                if ($val['status']==0){
                                    echo "رایگان";
                                }else{
                                    echo "ویژه";
                                }
                            ?>
                        </td>
                        <td><?php echo $res['title']; ?></td>
                        <td><a href="dashbord.php?c=files&a=edit&product=<?php echo $_GET['product']; ?>&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                        <td><a href="dashbord.php?c=files&a=delete&product=<?php echo $_GET['product']; ?>&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










