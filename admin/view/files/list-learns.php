<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست فایل دوره ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th> عنوان دوره</th>
                    <th> تصویر دوره</th>
                    <th>قیمت دوره</th>
                    <th>درصد تکمیل</th>
                    <th>لیست فایل ها</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($res as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['title']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><?php echo $val['price']; ?></td>
                        <td><?php echo $val['status']; ?></td>
                        <td><a href="dashbord.php?c=files&a=list-files&product=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-list"></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










