
<h1 class="pageLables">ویرایش فایل</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش فایل <?php $res=$files->pro_learn($product); echo $res['title']; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان فایل</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">زمان جلسه</label>
                        <input type="text" name="frm[time]" class="form-control" value="<?php echo $result['time']; ?>">
                    </div>
                    <label for="exampleInputPassword1">وضعیت نمایش</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" <?php if ($result['status']==0){echo "checked"; } ?> id="optionsRadios1" value="0">رایگان
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="frm[status]" <?php if ($result['status']==1){echo "checked"; } ?> id="optionsRadios1" value="1"> غیر رایگان
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تغییر فایل</label>
                        <input type="file" name="files" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">دوره آموزشی</label>
                        <select class="form-control input-lg m-bot15" name="frm[pro_id]">
                            <?php foreach ($pro as $val): ?>
                               <option value="<?php echo $val['id']; ?>"  <?php if ($result['pro_id']==$val['id']){echo " selected"; } ?> ><?php echo $val['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">جلسه :</label>
                        <input type="text" name="frm[jalase]" class="form-control" value="<?php echo $result['jalase']; ?>">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>
            </div>
        </section>
    </div>
</div>