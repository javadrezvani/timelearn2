<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست دوره ها
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th> عنوان دوره</th>
                    <th> تصویر دوره</th>
                    <th>قیمت دوره</th>
                    <th>درصد تکمیل</th>
                    <th>دسته بندی دوره</th>
                    <th>تعداد فروش</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($pro as $key=>$val):
                    ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $val['title']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><?php echo $val['price']; ?></td>
                        <td><?php echo $val['status']; ?></td>
                        <td><?php
                            $parent=$class->pro_list_catid($val['cat_id']);
                            echo $parent['title'];
                            ?>
                        </td>
                        <?php
                            $total=count($class->total_foroosh($val['id']));
                        ?>
                        <td><?php echo $total ?></td>
                        <td><a href="dashbord.php?c=pro&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                        <td><a href="dashbord.php?c=pro&a=delete&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










