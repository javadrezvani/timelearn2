<h1 class="pageLables">افزودن دوره جدید</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                افزودن دوره جدید به وب سایت
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دوره</label>
                        <input type="text" name="frm[title]" class="form-control" placeholder="عنوان دوره را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن دوره</label>
                        <textarea type="text" id="editor1" rows="20" name="frm[text]" class="form-control ckeditor"></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر دوره</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">مدرس دوره</label>
                        <select class="form-control input-lg m-bot15" name="frm[teacher]">
                            <?php
                            foreach ($teachers as $val){
                                echo "<option value=\"$val[id]\">$val[name]$val[lastname]</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">دسته بندی دوره</label>
                        <select class="form-control input-lg m-bot15" name="frm[cat_id]">
                            <?php
                            foreach ($res as $val){
                                echo "<option value=\"$val[id]\">$val[title]</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت</label>
                        <input type="text" name="frm[price]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">زمان دوره</label>
                        <input type="text" name="frm[time]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تعداد جلسات</label>
                        <input type="text" name="frm[videos]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">درصد تکمیل</label>
                        <input type="text" name="frm[status]" class="form-control">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>