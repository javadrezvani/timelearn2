<h1 class="pageLables">
    ویرایش محصول
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش محصول <?php echo $result['title']; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دسته بندی</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن محصول</label>
                        <textarea type="text" id="editor1" name="frm[text]" class="form-control ckeditor"><?php echo $result['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر محصول</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">مدرس محصول</label>
                        <select class="form-control input-lg m-bot15" name="frm[teacher]">
                            <?php foreach ($teachers as $value): ?>
                                <option value="<?php echo $value['id']; ?>" <?php if ($result['teacher']==$value['id']){echo " selected"; } ?>><?php echo $value['name'].$value['lastname']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">زمان محصول</label>
                        <input type="text" name="frm[time]" class="form-control" value="<?php echo $result['time']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تعداد ویدیو</label>
                        <input type="text" name="frm[videos]" class="form-control" value="<?php echo $result['videos']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت محصول</label>
                        <input type="text" name="frm[price]" class="form-control" value="<?php echo $result['price']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">درصد تکمیل</label>
                        <input type="text" name="frm[status]" class="form-control" value="<?php echo $result['status']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">دسته بندی</label>
                        <select class="form-control input-lg m-bot15" name="frm[cat_id]">
                            <?php foreach ($res as $value): ?>
                                <option value="<?php echo $value['id']; ?>" <?php if ($result['cat_id']==$value['id']){echo " selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

