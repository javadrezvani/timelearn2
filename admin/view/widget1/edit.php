<h1 class="pageLables">
    ویرایش ویجت
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش ویجت <?php echo $result['title']; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان ویجت</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن ویجت</label>
                        <textarea type="text" id="editor1" name="frm[text]" class="form-control ckeditor"><?php echo $result['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر ویجت</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">لینک ویجت</label>
                        <input type="text" name="frm[more]" class="form-control" value="<?php echo $result['more']; ?>">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

