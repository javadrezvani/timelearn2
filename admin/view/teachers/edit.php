<h1 class="pageLables">
    ویرایش محصول
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش اطلاعات مدرس <?php echo "$result[name]"."$result[lastname]"; ?>
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام مدرس</label>
                        <input type="text" name="frm[name]" class="form-control" value="<?php echo $result['name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام خانوادگی</label>
                        <input type="text" name="frm[lastname]" class="form-control" value="<?php echo $result['lastname']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">معرفی مختصر</label>
                        <textarea type="text" name="frm[moarefi]" rows="10" class="form-control"><?php echo $result['moarefi']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">رزومه مدرس</label>
                        <textarea type="text" rows="20" name="frm[rezomeh]" class="form-control">value="<?php echo $result['rezomeh']; ?>"</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">مدرس محصول</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>

                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>
            </div>
        </section>
    </div>
</div>

