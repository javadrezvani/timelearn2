<h1 class="pageLables">افزودن مدرس جدید</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                افزودن مدرس جدید به وب سایت
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام مدرس</label>
                        <input type="text" name="frm[name]" class="form-control" placeholder="نام مدرس را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">نام خانوادگی</label>
                        <input type="text" name="frm[lastname]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">معرفی مختصر</label>
                        <textarea type="text" name="frm[moarefi]" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">رزومه مدرس</label>
                        <textarea type="text" id="editor1" rows="20" name="frm[rezomeh]" class="form-control ckeditor"></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">مدرس محصول</label>
                        <input type="file" name="image" class="form-control">
                    </div>

                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>