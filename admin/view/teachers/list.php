<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                لیست مدرسان
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th>نام مدرس</th>
                    <th>نام خانوادگی مدرس</th>
                    <th>عکس مدرس</th>
                    <th>ویرایش</th>
                    <th>حذف</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($teacher as $val):
                    ?>
                    <tr>
                        <td><?php echo $val['name']; ?></td>
                        <td><?php echo $val['lastname']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><a href="dashbord.php?c=teachers&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                        <td><a href="dashbord.php?c=teachers&a=delete&id=<?php echo $val['id']; ?>" class="btn btn-danger btn-xs"><i class="icon-trash "></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










