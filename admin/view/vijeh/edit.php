<h1 class="pageLables">
    ویرایش محصول ویژه
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش محصول ویژه
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان پیشنهاد ویژه</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن عنوان</label>
                        <textarea type="text" id="editor1" rows="2" name="frm[titletext]" class="form-control ckeditor"><?php echo $result['titletext']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر پیشنهاد ویژه</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت</label>
                        <input type="text" name="frm[price]" class="form-control" value="<?php echo $result['price']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تخفیف</label>
                        <input type="text" name="frm[takhfif]" class="form-control" value="<?php echo $result['takhfif']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قابل پرداخت</label>
                        <input type="text" name="frm[pardakht]" class="form-control" value="<?php echo $result['pardakht']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن پیشنهاد ویژه</label>
                        <textarea type="text" id="editor1" rows="10" name="frm[text]" class="form-control ckeditor"><?php echo $result['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">لینک محصول</label>
                        <input type="text" name="frm[more]" class="form-control" value="<?php echo $result['more']; ?>">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

