<h1 class="pageLables">افزودن محصول ویژه</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
               افزودن محصول ویژه
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان محصول ویژه</label>
                        <input type="text" name="frm[title]" class="form-control" placeholder="عنوان محصول را وارد کنید">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن عنوان</label>
                        <textarea type="text" id="editor1" rows="2" name="frm[titletext]" class="form-control ckeditor"></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر محصول ویژه</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قیمت</label>
                        <input type="text" name="frm[price]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">لینک محصول ویژه</label>
                        <input type="text" name="frm[more]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تخفیف</label>
                        <input type="text" name="frm[takhfif]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">قابل پرداخت</label>
                        <input type="text" name="frm[pardakht]" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">متن اسلایدر</label>
                        <textarea type="text" id="editor1" rows="20" name="frm[text]" class="form-control ckeditor"></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">افزودن</button>
                </form>
            </div>
        </section>
    </div>
</div>