<h1 class="pageLables">
    ویرایش اسلایدر
</h1>
<div class="row">
    <div class="col-lg-8 col-lg-offset-2" >
        <section class="panel">
            <header class="panel-heading">
                ویرایش اسلایدر
            </header>
            <div class="panel-body">
                <form role="form" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">عنوان دسته بندی</label>
                        <input type="text" name="frm[title]" class="form-control" value="<?php echo $result['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">لینک اسلایدر</label>
                        <input type="text" name="frm[link]" class="form-control" value="<?php echo $result['link']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">متن محصول</label>
                        <textarea type="text" id="editor1" name="frm[text]" rows="10" class="form-control ckeditor"><?php echo $result['text']; ?></textarea>
                        <script>
                            CKEDITOR.replace( 'editor1', {
                                language: 'fa',
                                uiColor: '#9AB8F3'
                            });
                        </script>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">تصویر محصول</label>
                        <input type="file" name="image" class="form-control">
                        <img src="<?php echo $result['image']; ?>" width="60">
                    </div>
                    <button type="submit" name="btn" class="btn btn-info">ویرایش</button>
                </form>

            </div>
        </section>
    </div>
</div>

