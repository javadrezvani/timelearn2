<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                اسلایدر صفحه اصلی
            </header>
            <table class="table table-striped table-advance table-hover">
                <thead>
                <tr>
                    <th> عنوان اسلایدر</th>
                    <th>عکس اسلایدر</th>
                    <th>لینک اسلایدر</th>
                    <th>ویرایش</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($sliderr as $key=>$val):
                    ?>
                    <tr>
                        <td><?php echo $val['title']; ?></td>
                        <td><img src="<?php echo $val['image']; ?>" width="60"></td>
                        <td><?php echo $val['link']; ?></td>
                        <td><a href="dashbord.php?c=slider&a=edit&id=<?php echo $val['id']; ?>" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </section>
    </div>
</div>










