<?php
class main{
    public function __construct(){
        global $db;
        $this->db=$db;
    }

    public function slider(){
        $results=$this->db->query("SELECT * FROM slider_tbl");
        $row=$results->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function slider_tbl(){
        $results=$this->db->query("SELECT * FROM slider_tbl");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function slider_showedit($id){
        $results=$this->db->query("SELECT * FROM slider_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function slider_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"slider");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update slider_tbl set title='$data[title]',text='$data[text]',link='$data[link]',image='$pic' where id='$id'");
    }

    public function settings_show(){
        $results=$this->db->query("SELECT * FROM settings_tbl");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function edit_settings($data,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"settings");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update settings_tbl set title='$data[title]',logo='$pic',description='$data[description]',keywords='$data[keywords]',copyright='$data[copyright]',instgram='$data[instgram]',googleplus='$data[googleplus]',twitter='$data[twitter]',googleplus='$data[googleplus]',address='$data[address]',tel='$data[tel]',text='$data[text]',timetamas='$data[timetamas]'");
    }

   public function menu(){
        $results=$this->db->query("SELECT * FROM menu_tbl where status='1' order by sort ");
        $row=$results->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function add_menu($data){
        $sql=$this->db->query("INSERT INTO menu_tbl (title,url,status,sort) VALUES ('$data[title]','$data[url]','$data[status]','$data[sort]')");
    }

    public function delete_menu($id){
        $results=$this->db->query("delete from menu_tbl where id='$id'");
    }


    public function show_edit_menus($id){
        $results=$this->db->query("select * from menu_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function edit_menus($data,$id){
        $result=$this->db->query("UPDATE menu_tbl SET title='$data[title]',url='$data[url]',sort='$data[sort]',status='$data[status]' WHERE id='$id'");
        $result->execute();
    }

    public function basket_list_pro($id){
        $results=$this->db->query("SELECT * FROM basket_tbl LEFT JOIN pro_tbl ON basket_tbl.pro_id = pro_tbl.id  WHERE basket_tbl.user_id=$id and pardakht='0'");
        $results->execute();
        $results=$results->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public function elanat_show(){
        $sql=$this->db->query("SELECT * FROM elanat_tbl");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function edit_elanat($data){
        $this->db->query("update elanat_tbl set title='$data[title]',titletext='$data[titletext]',text='$data[text]',link='$data[link]',status='$data[status]'");
    }
}