<?php
include_once 'Mindex.php';
class blog_cat extends main{

    public function blog_cat_list(){
        $results=$this->db->query("SELECT * FROM blog_cat");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function blog_cat_list_default(){
        $results=$this->db->query("SELECT * FROM blog_cat order by id ASC");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function blog_under_list_default($id){
        $results=$this->db->query("SELECT * FROM blog_cat where chid='$id'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function blog_cat_add($data){
        $this->db->query("insert into blog_cat (title) values ('$data[title]')");
    }

    public function blog_cats_list(){
        $results=$this->db->query("SELECT * FROM blog_cat");
        return $results;
    }

    public function blog_cat_delete($id){
        $this->db->query("delete from blog_cat where id='$id'");
    }

    public function blog_cat_showedit($id){
        $results=$this->db->query("SELECT * FROM blog_cat where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function blog_cat_edit($data,$id){
        $this->db->query("update blog_cat set title='$data[title]' where id='$id'");
    }


}