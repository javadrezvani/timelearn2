<?php

include_once 'Mindex.php';
class user extends main{

    public function admin(){
        $sql=$this->db->query("SELECT * FROM admin_tbl");
        $row=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function row($a){
        echo $a;
        return $a;
    }

    public function admin_tbl(){
        $conn=mysqli_connect("localhost","root","","timelearn");
        $sql="SELECT * FROM admin_tbl";
        $row=mysqli_query($conn,$sql);
        $res=mysqli_fetch_assoc($row);
        return $res;
    }

    public function show_profile($id){
        $sql=$this->db->query("SELECT * FROM admin_tbl WHERE id='$id'");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function edit_profile($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            @$folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader",$folder,"admin");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update admin_tbl set lastname='$data[lastname]',email='$data[email]',password=sha1('$data[password]'),name='$data[name]',image='$pic' where id='$id'");
    }

    public function user_add($data){
        $this->db->query("insert into user_tbl (email,password,name,lastname,status,phone,gender,vc) values ('$data[email]',sha1('$data[password]'),'$data[name]','$data[lastname]','0','$data[phone]','$data[gender]','$data[vc]')");
        $id=$this->db->lastInsertId();
        return $id;
    }

    public function change_password($email,$data){
        $this->db->query("update user_tbl set password=sha1('$data[password]') where email='$email'");
    }

    public function user_login($data){
        $results=$this->db->query("SELECT * FROM user_tbl where email='$data[email]' AND password=sha1('$data[password]')");
        $result=$results->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function user_name($id){
        $results=$this->db->query("SELECT * FROM user_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }


    public function users(){
        $sql=$this->db->query("select * from user_tbl");
        $row=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function show_my_profile($id){
        $sql=$this->db->query("select * from user_tbl where id='$id'");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function users_profile($id){
        $sql=$this->db->query("select * from user_tbl where id='$id'");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function edit_my_profile($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            @$folder=$a[$total-2];
            $exts=array('image/jpg','image/jpeg','image/png');
            $pic=uploader($img,"public/uploader",$folder,"user",$exts);
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update user_tbl set name='$data[name]',lastname='$data[lastname]',phone='$data[phone]',image='$pic' where id='$id'");
    }

    public function edit_password($data,$id){
        $this->db->query("update user_tbl set password=sha1('$data[newpassword]') WHERE id='$id'");
    }

    public function delete_user($id){
        $this->db->query("delete from user_tbl where id='$id'");
    }

}