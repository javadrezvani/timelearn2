<?php
include_once 'Mindex.php';
class pro extends main {
    public function pro_list(){
        $results=$this->db->query("SELECT * FROM pro_tbl");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function pro_list_catid($cat_id){
        $results=$this->db->query("SELECT * FROM procat_tbl where id='$cat_id'");
        $result=$results->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function prolist_catid($cat_id){
        $results=$this->db->query("SELECT * FROM pro_tbl where cat_id='$cat_id'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public function pro_under_list_default($id){
        $results=$this->db->query("SELECT * FROM pro_tbl where cat_id='$id'");
        $result=$results->fetchALL(PDO::FETCH_ASSOC);
        return $result;
    }

    public function pro_add($data,$img){
        $sql=$this->db->query("insert into pro_tbl (title,text,image,teacher,time,videos,price,status,cat_id) values ('$data[title]','$data[text]','$img','$data[teacher]','$data[time]','$data[videos]','$data[price]','$data[status]','$data[cat_id]')");
    }

    public function procat_list(){
        $results=$this->db->query("SELECT * FROM procat_tbl");
        return $results;
    }

    public function pro_delete($id){
        $this->db->query("delete from pro_tbl where id='$id'");
    }

    public function pro_showedit($id){
        $results=$this->db->query("SELECT * FROM pro_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function pro_pay($id){
        $results=$this->db->query("SELECT * FROM pro_tbl where id='$id'");
        $row=$results->fetchALL(PDO::FETCH_ASSOC);
        return $row;
    }

    public function pro_basket_list($id){
        $results=$this->db->query("SELECT * FROM pro_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function pro_detail($id){
        $results=$this->db->query("SELECT * FROM pro_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function pro_search($search){
        $results=$this->db->query("SELECT title FROM pro_tbl where title like '%$search%'");
        $row=$results->fetchAll(PDO::FETCH_OBJ);
        return $row;
    }

    public function pro_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"pro");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update pro_tbl set title='$data[title]',text='$data[text]',cat_id='$data[cat_id]',image='$pic',teacher='$data[teacher]',time='$data[time]',videos='$data[videos]',price='$data[price]',status='$data[status]' where id='$id'");
    }

    public function teachers_add($data,$img){
        $this->db->query("insert into teachers_tbl (name,lastname,moarefi,rezomeh,image) values ('$data[name]','$data[lastname]','$data[moarefi]','$data[rezomeh]','$img')");
    }

    public function teachers_list(){
        $sql=$this->db->query("select * from teachers_tbl");
        $row=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function pro_list_teachers($teachers){
        $results=$this->db->query("SELECT * FROM teachers_tbl where id='$teachers'");
        $result=$results->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function teachers_delete($id){
        $this->db->query("delete from teachers_tbl where id='$id'");
    }

    public function teachers_showedit($id){
        $results=$this->db->query("SELECT * FROM teachers_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function teachers_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"teachers");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update teachers_tbl set name='$data[name]',lastname='$data[lastname]',moarefi='$data[moarefi]',rezomeh='$data[rezomeh]',image='$pic' where id='$id'");
    }

    public function files_list_pro($id){
        $sql=$this->db->query("select * from files_tbl where pro_id='$id'");
        $result=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function list_comments_page($pro_id){
        $sql=$this->db->query("select * from comments_tbl where status='1' and pro_id='$pro_id'");
        $row=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function list_comments($pro_id){
        $sql=$this->db->query("select * from comments_tbl where status='1' and pro_id='$pro_id'");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function total_foroosh($pro_id){
        $results=$this->db->query("SELECT * FROM basket_tbl where pardakht='1' and pro_id='$pro_id'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

}




