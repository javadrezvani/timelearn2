<?php
class widget4{
    public function __construct(){
        global $db;
        $this->db=$db;
    }
    public function widget4_tbl(){
        $results=$this->db->query("SELECT * FROM widget4_tbl");
        $row=$results->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function add_widget4_tbl($data,$img){
        $sql=$this->db->query("INSERT INTO widget4_tbl (title,text,image) VALUES ('$data[title]','$data[text]','$img')");
    }

    public function delete_widget4_tbl($id){
        $results=$this->db->query("delete from widget4_tbl where id='$id'");
    }

    public function show_edit_widget4_tbl($id){
        $results=$this->db->query("SELECT * FROM widget4_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function widget4_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"widget");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update widget4_tbl set title='$data[title]',text='$data[text]',image='$pic' where id='$id'");
    }
}
