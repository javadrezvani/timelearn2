<?php
include_once 'Mindex.php';
class basket extends main{

    public function basket_list($id){
        $results=$this->db->query("SELECT * FROM basket_tbl where user_id='$id' AND pardakht='0'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function basket_pay_list_pro($id){
        $results=$this->db->query("SELECT * FROM basket_tbl LEFT JOIN pro_tbl ON basket_tbl.pro_id = pro_tbl.id  WHERE basket_tbl.user_id=$id and pardakht='1'");
        $results->execute();
        $results=$results->fetchAll(PDO::FETCH_ASSOC);
        return $results;
    }

    public function basket_pay_list($user_id){
        $results=$this->db->query("SELECT * FROM basket_tbl where user_id='$user_id' and pardakht='1'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function pardakht_ok($pro_id,$user_id){
        $results=$this->db->query("SELECT * FROM basket_tbl where pardakht='1' and pro_id='$pro_id' and user_id='$user_id'");
        $result=$results->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function basket_add($user_id,$pro_id){
        $this->db->query("insert into basket_tbl (user_id,pro_id) values ('$user_id','$pro_id')");
    }

    public function basket_omit($id,$user_id){
        $this->db->query("delete from basket_tbl where pro_id='$id' AND user_id='$user_id'");
    }

    public function basket_pay($user_id){
        $this->db->query("update basket_tbl set pardakht='1' where user_id=$user_id");
    }

    public function basket_nahaei($user_id){
        $this->db->query("update basket_tbl set pardakht='1' where user_id=$user_id");
    }

    public function basket_delete($id){
        $this->db->query("delete from basket_tbl where id='$id'");
    }
}