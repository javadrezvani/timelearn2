<?php

include_once 'Mindex.php';
class blog extends main{
    public function blog_list(){
        $results=$this->db->query("SELECT * FROM blog_tbl");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function blog_list_catid($cat_id){
        $results=$this->db->query("SELECT * FROM blog_cat where id='$cat_id'");
        $result=$results->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function bloglist_catid($cat_id){
        $results=$this->db->query("SELECT * FROM blog_tbl where cat_id='$cat_id'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public function blog_under_list_default($id){
        $results=$this->db->query("SELECT * FROM blog_tbl where cat_id='$id'");
        $result=$results->fetchALL(PDO::FETCH_ASSOC);
        return $result;
    }

    public function blog_add($data,$img){
        $sql=$this->db->query("insert into blog_tbl (title,titletext,text,nevisandeh,date,cat_id,image) values ('$data[title]','$data[titletext]','$data[text]','$data[nevisandeh]','$data[date]','$data[cat_id]','$img')");
    }

    public function blog_cat_list(){
        $results=$this->db->query("SELECT * FROM blog_cat");
        $row=$results->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function blog_delete($id){
        $this->db->query("delete from blog_tbl where id='$id'");
    }

    public function blog_showedit($id){
        $results=$this->db->query("SELECT * FROM blog_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function blog_detail($id){
        $results=$this->db->query("SELECT * FROM blog_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function blog_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"blog");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update blog_tbl set title='$data[title]',titletext='$data[titletext]',text='$data[text]',nevisandeh='$data[nevisandeh]',date='$data[date]',cat_id='$data[cat_id]',image='$pic' where id='$id'");
    }

    public function blog_comments_page($blog_id){
        $sql=$this->db->query("select * from commentblog_tbl where status='1' and blog_id='$blog_id'");
        $row=$sql->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function blog_comments($blog_id){
        $sql=$this->db->query("select * from commentblog_tbl where status='1' and blog_id='$blog_id'");
        $row=$sql->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

}

