<?php
class widget1{
    public function __construct(){
        global $db;
        $this->db=$db;
    }
    public function widget1_tbl(){
        $results=$this->db->query("SELECT * FROM widget1_tbl");
        $row=$results->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    }

    public function add_widget1_tbl($data,$img){
        $sql=$this->db->query("INSERT INTO widget1_tbl (title,text,more,image) VALUES ('$data[title]','$data[text]','$data[more]','$img')");
    }

    public function delete_widget1_tbl($id){
        $results=$this->db->query("delete from widget1_tbl where id='$id'");
    }

    public function show_edit_widget1_tbl($id){
        $results=$this->db->query("SELECT * FROM widget1_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function widget1_edit($data,$id,$img,$oldpic){
        if($_FILES[$img]['name']!=''){
            $a=explode("/",$oldpic);
            $total=count($a);
            $folder=$a[$total-2];
            $pic=uploader($img,"../public/uploader/",$folder,"widget");
        }
        else{
            $pic=$oldpic;
        }
        $this->db->query("update widget1_tbl set title='$data[title]',text='$data[text]',more='$data[more]',image='$pic' where id='$id'");
    }
}
