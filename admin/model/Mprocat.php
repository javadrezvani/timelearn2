<?php
include_once 'Mindex.php';
class procat extends main {

    public function procat_list(){
        $results=$this->db->query("SELECT * FROM procat_tbl");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function procat_list_default(){
        $results=$this->db->query("SELECT * FROM procat_tbl order by id ASC");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function procat_under_list_default($id){
        $results=$this->db->query("SELECT * FROM procat_tbl where chid='$id'");
        $result=$results->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function procat_add($data){
        $this->db->query("insert into procat_tbl (title) values ('$data[title]')");
    }

    public function promaincat_list(){
        $results=$this->db->query("SELECT * FROM procat_tbl");
        return $results;
    }

    public function procat_delete($id){
        $this->db->query("delete from procat_tbl where id='$id'");
    }

    public function procat_showedit($id){
        $results=$this->db->query("SELECT * FROM procat_tbl where id='$id'");
        $row=$results->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function procat_edit($data,$id){
        $this->db->query("update procat_tbl set title='$data[title]' where id='$id'");
    }


}