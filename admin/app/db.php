<?php
class db{
    protected $pdo;
    private $db;
    private $user;
    private $password;
    private $tbl;

    public function __construct($db='timelearn',$user='root',$password=''){
        $this->db=$db;
        $this->user=$user;
        $this->password=$password;
        $this->connection();
    }

    public function connection(){
        try {
            $this->pdo=new PDO("mysql:host=localhost;dbname={$this->db}",$this->user,$this->password);
        }catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function setTBL($tbl){
        $this->tbl=$tbl;
    }

    public function searchData($name,$value){
        $sql=$this->pdo->prepare("select * from {$this->tbl} where $name='$value'");
        $sql->execute();
        $result=$sql->fetch(PDO::FETCH_OBJ);
        return $result;
    }


}