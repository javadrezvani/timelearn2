<?php
ob_start();
session_start();
include_once 'db.php';
class users extends db{
    protected $tbl='admin_tbl';
    public function login($data,$cookie){
        $email=$data['email'];
        $password=$data['password'];
        $this->setTBL($this->tbl);
        $userdata=$this->searchData('email',$email);
        if (@sha1($password)==$userdata->password){
            if (isset($cookie)){
                $_SESSION['cookie']=$userdata->name;
                setcookie('remember','user',time()+(60*60*24*7*4*12*10),'/');
            }
            $_SESSION['name']=$userdata->name;
            header("location:dashbord.php");
        }else{
            header("location:index.php?user=error");
        }
    }

    public function logout(){
        session_destroy();
        header("location:index.php?logout=ok");
    }
}