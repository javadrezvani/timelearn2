﻿<?php
ob_start();

include_once 'view/layout/header.php';

session_start();
if (!isset($_COOKIE['remember'])){
    if (!isset($_SESSION['name'])){
        header("location:index.php?user=first");
    }
}

if (isset($_GET['logout'])){
    if ($_GET['logout']=='ok'){
        include_once 'app/users.php';
        $obj=new users();
        $obj->logout();
    }
}

function uploader($file,$dir,$folder,$name){
    @$file=$_FILES[$file];
    if(!file_exists($folder)){
        @mkdir($dir.$folder);
    }
    $filename=$file['name'];
    $array=explode(".",$filename);
    $ext=end($array);
    $newname=$name."-".rand().".".$ext;
    $from=$file['tmp_name'];
    $to=$dir.$folder."/".$newname;
    move_uploaded_file($from,$to);
    return $to;
}

include_once '../public/include/config.php';
?>




<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="#"><i class="icon-home"></i>&nbsp;پنل مدیریتی وب سایت</a></li>
                </ul>
                <!--breadcrumbs end -->
            </div>
        </div>
        <?php
        $controller=@$_GET['c']?$_GET['c']:'index';
        $action=@$_GET['a']?$_GET['a']:'index';
        if(file_exists('controller/C'.$controller.'.php')){
            require_once 'controller/C'.$controller.'.php';
        }
        ?>
    </section>
</section>
<?php
include_once 'view/layout/footer.php';
?>

