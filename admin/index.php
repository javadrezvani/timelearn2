﻿<?php
ob_start();
include_once 'app/db.php';
if (isset($_COOKIE['remember'])){
    header("location:dashbord.php");
}

if (isset($_POST['btn'])){
    include_once 'app/users.php';
    $obj=new users();
    $data=$_POST['frm'];
    $cookie=$_POST['remember'];
    $obj->login($data,$cookie);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="icon" href="../public/admin/img/logo.png">
    <title>ورود به پنل مدیریت</title>
    <link href="../public/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/admin/css/bootstrap-reset.css" rel="stylesheet">
    <link href="../public/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="../public/admin/css/style.css" rel="stylesheet">
    <link href="../public/admin/css/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="../public/admin/js/html5shiv.js"></script>
    <script src="../public/admin/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-body">
<div class="container">
    <img src="../public/admin/img/logo.png" width="150" height="150" alt="" class="academyitLogo"/>
    <form class="form-signin" action="" method="post">
        <h2 class="form-signin-heading">همین حالا وارد شوید</h2>
        <div class="login-wrap">
            <input type="text" class="form-control" placeholder="نام کاربری" name="frm[email]" autofocus>
            <input type="password" class="form-control" name="frm[password]" placeholder="کلمه عبور">
            <label class="checkbox">
                <input type="checkbox" value="remember-me" name="remember"> مرا به خاطر بسپار
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit" name="btn">ورود</button>
        </div>
    </form>
</div>
</body>
</html>
