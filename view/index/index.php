<!-- START : NEWSLETTER POPUP -->
<?php if ($elanat['status']==1): ?>
<div class="gkl-dtte common-popup cpp-show trs-2">
    <form class="feld-cnt drlr-dgglr-frm common-popup-cont trs-2" method="post" action="#newsletter">

        <div class="mkf-scsc">
            <div class="klfg-lft">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="gfkvf-str">
                    <h2 class="fkl-3d3">
                        به تایم لرن خوش آمدید
                    </h2>
                    <div></div>
                    <div class="rtg-tdl">
                        اولین هدیه خودتو از ما دریافت کن
                    </div>
                </div>

                <div class="gfk-img-h">
                    <img src="../public/default/img/common/nw-ltt-img.jpg">
                    <a class="lds-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="<?php echo $elanat['link']; ?>">
                                <span class="flgf-svs common-effect-elem trs-2">
                                    <i class="fa fa-reply"></i>
                                </span>
                    </a>
                </div>

            </div>
            <div class="flf-rght">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="flfs-scp">
                    <div class="dlgf-head dh-dark-text dh-blue-color">
                        <h1 class="fgfl-vv">
                             <?php echo $elanat['title']; ?>
                        </h1>
                    </div>
                    <div class="flfer-ssc a-editor-block">
                        <h2 class="fgfl-vv-h2">
                            <?php echo $elanat['titletext']; ?>
                        </h2>
                        <?php echo $elanat['text']; ?>
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
<?php endif; ?>
<!-- START : IMAGE BLOCK -->
<section class="dkvlrg-simg">
    <div class="rlgf-bimg">
        <img src="<?php echo $slider['image']; ?>" class="rfflg-ggkm">
    </div>
    <div class="dfld-ss">
        <div id="particles-js"></div>
    </div>
    <div class="dfdep-sspo">
        <div class="dvvfgp-cc">
            <div class="sys-container glkfgr-cont">
                <div class="gfkt-ttdl">
                    <div class="fklf-spc">
                        <div class="dlgf-head">
                            <h1 class="fgfl-vv">
                                <?php echo $slider['title']; ?>
                            </h1>
                        </div>




                        <div class="rfkf-scte">
                            <p>
                               <?php echo $slider['text']; ?>
                            </p>

                            <ul class="vl-svt">
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    دیگه نیازی نیست ساعت ها سر کلاس درس حضور داشته باشید
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    همه آموزش ها از سطح کاملا مقدماتی شروع می شوند
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    آموزش ها رو تا سطح بسیار پیشرفته ادامه میدیم تا کاملا حرفه ای بشید
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    کلیه آموزش ها رو بر اساس نیاز بازار کار تهیه کردیم
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    میتونید سوالاتتون رو از ما بپرسید و در کمتر از 24 ساعت جواب بگیرید
                                </li>
                            </ul>

                            <div class="vl-scctt">
                                <a href="<?php echo $slider['link']; ?>" class="common-button cmb-icon cmb-large">
                                    <i class="fa fa-rocket"></i>
                                    همین حالا شروع کن!
                                </a>
                            </div>

                        </div>



                        <div class="rfkf-scte kfdf-summary">
                            <p>
                                <?php echo $slider['text']; ?>
                            </p>

                            <ul class="vl-svt">
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    دیگه نیازی نیست ساعت ها
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    همه آموزش ها از سطح کاملا
                                </li>
                                <li>
                                    <i class="fa fa-angle-double-left"></i>
                                    میتونید سوالاتتون رو از ما بپرسید
                                </li>
                            </ul>

                            <div class="vl-scctt">
                                <a href="<?php echo $slider['link']; ?>" class="common-button cmb-icon cmb-large">
                                    <i class="fa fa-rocket"></i>
                                    همین حالا شروع کن!
                                </a>
                            </div>

                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : IMAGE BLOCK -->





<!-- START : SERVICE BLOCK -->
<section class="fkvlg-ddtei">
    <div class="sys-container">
        <div class="sfkf-ssk f-parent">
           <?php foreach ($widget1 as $val): ?>
            <div class="fkft-trc">
                <div class="ldgrt-spc">
                    <div class="flfg-img-rr">
                        <img src="<?php echo $val['image']; ?>">
                        <a class="djri-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="<?php echo $val['more']; ?>">
                                    <span class="ddf-sdd">
                                        <span class="dvl-btn common-effect-elem trs-2">
                                            <i class="fa fa-angle-left"></i>
                                            ادامه مطلب
                                        </span>
                                    </span>
                        </a>
                    </div>
                    <div class="fdgf-sccp">
                        <a class="flr-rt trs-2" href="#">
                            <?php echo $val['title']; ?>
                        </a>
                        <p class="dfl-cc">
                            <?php echo $val['text']; ?>
                        </p>
                        <div class="rgf-tls">
                            <a href="<?php echo $val['more']; ?>" class="common-button cmb-icon">
                                <i class="fa fa-angle-left"></i>
                                ادامه مطلب
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- END : SERVICE BLOCK -->





<!-- START : TIMER BLOCK -->
<section class="dvdkt-timer">
    <div class="sys-container">
        <div class="dflg-fssc f-parent">
            <div class="dgg-cls dld-ww">
                <div class="dgrf-spc">
                    <div class="ffogf-img-h">
                        <img src="<?php echo $vijeh['image']; ?>">
                        <a class="sdle-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="<?php echo $vijeh['more']; ?>">
                                    <span class="flgf-svs common-effect-elem trs-2">
                                        <i class="fa fa-reply"></i>
                                    </span>
                        </a>
                    </div>
                    <!-- <div class="fdld-trim">
                         <div class="clock"></div>
                         <div class="fkl-ttls f-parent">
                             <span class="fdke-ss">ثانیه</span>
                             <span class="fdke-ss">دقیقه</span>
                             <span class="fdke-ss">ساعت</span>
                             <span class="fdke-ss">روز</span>
                         </div>
                         <script type="text/javascript">
                             var clock = $('.clock').FlipClock(timerSeconds, {
                                 clockFace: 'DailyCounter',
                                 countdown: true
                             });
                         </script>
                     </div> -->
                </div>
            </div>
            <div class="dgg-cls dld-ss">
                <div class="dgrf-spc">
                    <h2 class="fl-eedssl-s">
                        <a class="trs-2" href="<?php echo $vijeh['more']; ?>">
                            <?php echo $vijeh['title']; ?>
                        </a>
                    </h2>
                    <p class="fkff-ss">
                        <?php echo $vijeh['titletext']; ?>
                    </p>
                    <div class="fkf-ssdvv">
                                <span class="common-tag-link ctl-red">
                                     <?php echo $vijeh['price']; ?>
                                    <span class="ctl-frw-triangle"></span>
                                </span>
                        <span class="common-tag-link">
                            <?php echo $vijeh['takhfif']; ?>
                                    <span class="ctl-frw-triangle"></span>
                                </span>
                        <span class="common-tag-link ctl-green">
                            <?php echo $vijeh['pardakht']; ?>
                                    <span class="ctl-frw-triangle"></span>
                                </span>
                    </div>

                    <div class="dfkdkrr-ss a-editor-block">
                        <?php echo $vijeh['text']; ?>
                    </div>

                    <div class="ktrd-tlss">
                        <a href="<?php echo $vijeh['more']; ?>" class="common-button cmb-cl-red cmb-icon cmb-large">
                            <i class="fa fa-rocket"></i>
                            کسب اطلاعات بیشتر
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : TIMER BLOCK -->




<!-- START : VIDEO BLOCK -->
<section class="fler-errgr">
    <div class="sys-container">
        <div class="dfvf-dssd f-parent">
            <div class="gflf-cl fdle-ee">
                <div class="fddg-spc">
                    <h2 class="fl-eedssl-s">
                        <a class="trs-2" href="<?php echo $vijeh2['more']; ?>">
                            <?php echo $vijeh2['title']; ?>
                        </a>
                    </h2>
                    <p class="fkff-ss">
                        <?php echo $vijeh2['titletext']; ?>
                    </p>
                    <div class="lrre-ced a-editor-block">
                        <?php echo $vijeh2['text']; ?>
                    </div>
                    <div class="fglk-tlss f-parent">
                                <span class="common-tag-link ctl-green">
                                    <?php echo number_format($vijeh2['price']); ?> هزار تومان
                                    <span class="ctl-frw-triangle"></span>
                                </span>
                        <a href="<?php echo $vijeh2['more']; ?>" class="common-button cmb-cl-red cmb-icon cmb-large">
                            <i class="fa fa-rocket"></i>
                            شروع دوره!
                        </a>
                    </div>
                </div>
            </div>
            <div class="gflf-cl fdle-ww">
                <div class="fddg-spc">
                    <div class="flf-ssccp">
                        <p class="afklf-fee">
                                    <span class="fldsd-ds">
                                        <?php echo $vijeh2['takhfif']; ?>
                                    </span>
                            <span class="fldscs-ww">
                                        تخفیف ویژه!!!
                                    </span>
                        </p>
                        <div class='flowplayer' data-swf='flowplayer.swf' style="width:620px;">
                            <video width="620" height="350" poster="img/demo/video-poster.jpg" controls="controls" preload="none">
                                <source src="<?php echo $vijeh2['video']; ?>" type="video/mp4">
                            </video>
                        </div>
                        <div class="flgfs-slrr f-parent">
                            <div class="dfmkg-cl dld-ee">
                                <div class="fkf-spc">

                                </div>
                            </div>
                            <div class="dfmkg-cl dld-ww">
                                <div class="fkf-spc">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END : VIDEO BLOCK -->





<!-- START : CIRCULAR SERVICE BLOCK -->
<section class="gflf-circ-serv">
    <div class="sys-container dfl-cont">
        <div class="dfdls-spc">
            <?php foreach ($widget4 as $value): ?>
            <div class="gfkf-miid-cl flkgb-i-<?php echo $value['id']; ?>">
                <div class="fklgf-img-h trs-2">
                    <img src="<?php echo $value['image']; ?>">
                    <a class="ffkr-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="#">
                                <span class="gvkf-sov common-effect-elem trs-2">
                                    <i class="fa fa-reply"></i>
                                </span>
                    </a>
                </div>
                <p class="gffls-cc">
                    <a class="trs-2" href="#"><?php echo $value['title']; ?></a>
                </p>
                <div class="afkg-fsc">
                    <?php echo $value['text']; ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<!-- END : CIRCULAR SERVICE BLOCK -->





<!-- START : COMMENT BLOCK -->
<section class="dlfg-usr-cmnt">
    <div class="sys-container tofd-cont">

        <div class="fkf-soc">

            <div class="fkdd-ccl kfg-next trs-2">
                <i class="fa fa-angle-left"></i>
            </div>
            <div class="fkdd-ccl kfg-prev trs-2">
                <i class="fa fa-angle-right"></i>
            </div>

            <div class="cmnt-slider">
                <div class="ds-viewer">
                    <div class="ds-relator">
                        <div class="ds-dragger">


                            <!-- START : COMMENT LOOP -->
                            <?php foreach ($widget3 as $item):?>
                            <div class="ds-item">
                                <div class="dckd-spc">
                                    <div class="lpeg-img-cl">
                                        <div class="sdr-img-h">
                                            <img src="<?php echo $item['image']; ?>">
                                        </div>
                                    </div>
                                    <div class="fkfd-cdg">
                                        <div class="fkf-scc">
                                            <p class="fkf-frd">
                                                <?php echo $item['name']; ?>
                                            </p>
                                            <p class="hdr-ggd">
                                                <?php echo $item['job']; ?>
                                            </p>
                                            <div class="fvkf-vvfkd">
                                                <?php echo $item['text']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <!-- END : COMMENT LOOP -->

                            <div class="ds-clear"></div>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
<!-- END : COMMENT BLOCK -->




