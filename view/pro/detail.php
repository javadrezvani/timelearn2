<?php @$pardakht=$basket->pardakht_ok($_GET['id'],$_SESSION['user_id']); ?>
<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2"><?php echo $resultss['title']; ?></a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->




<!-- START : DETAIL BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont f-parent">

        <div class="fked-col fc-rr">
            <div class="dksd-spc">




                <!-- START : SUMMARY BOX -->
                <div class="fjsc-box fjsc-bg fjsc-fans-spc fjsc-no-pdd m-top-bottom-bug">
                    <div class="crs-fans-els">
                        <div class="fkdvc-tls">
                            <p class="kvc-lbl kfvcl-title">
                                خلاصه دوره
                            </p>
                            <div></div>
                            <p class="kvc-lbl kfvcl-desc">
                                <?php echo $result['title']; ?>
                            </p>
                        </div>
                        <div class="dor-img-h">
                            <img src="<?php echo $result['image']; ?>">
                        </div>
                    </div>

                    <div class="fkrd-factor fxlro-summ">
                        <div class="kdf-row">
                                    <span class="ke-drrr-tt dkrd-label">
                                        امتیاز
                                    </span>
                            <span class="ke-drrr-tt dkrd-value">
                                         <span class="common-star-rate csr-active1" data-value="5" data-max="5" data-icon-full="fa fa-star" data-icon-null="fa fa-star-o"></span>
                                    </span>
                        </div>
                        <div class="kdf-row">
                                    <span class="ke-drrr-tt dkrd-label">
                                        مدت زمان
                                    </span>
                            <span class="ke-drrr-tt dkrd-value">
                                        <?php echo $result['time']; ?>
                                    </span>
                        </div>
                        <div class="kdf-row">
                                    <span class="ke-drrr-tt dkrd-label">
                                        تعداد ویدیو ها
                                    </span>
                            <span class="ke-drrr-tt dkrd-value">
                                        <?php echo $result['videos']; ?> فایل
                                    </span>
                        </div>

                        <div class="kdf-row">
                                    <span class="ke-drrr-tt dkrd-label">
                                        درصد تکمیل
                                    </span>
                            <span class="ke-drrr-tt dkrd-value">
                                        <?php echo $result['status']; ?>
                                    </span>
                        </div>
                    </div>

                    <div class="gkddc-prcs">
                        <div class="prcs-bar prcs-midd"></div>
                        <div class="prcs-bar prcs-curr" style="width: <?php echo $result['status']; ?>;">
                            <span class="dldlc-line"></span>
                            <span class="kddc-label"><?php echo $result['status']; ?></span>
                        </div>
                    </div>

                    <div class="orlr-tls">
                        <a href="<?php  if ($pardakht==''): if ($result['price']!='رایگان'): ?>index.php?c=basket&a=add&pro_id=<?php echo $result['id']; ?><?php endif; endif; ?>" class="common-button cmb-icon cmb-cl-green">
                            <i class="fa fa-shopping-cart"></i>
                            <?php if ($result['price']!='رایگان'){
                                if (@$pardakht!=''){
                                    echo "شما قبلا این دوره را خریداری کرده اید";
                                }else{
                                    echo number_format($result['price'])." تومان";
                                }
                            }else{
                                echo "رایگان";
                            } ?>
                            <?php if ($result['price']!='رایگان'): ?>
                            <?php if (@$pardakht==''): ?>
                            <span class="sdkf-label">
                                        20% تخفیف
                            </span>
                            <?php endif;?>
                            <?php endif; ?>
                        </a>
                    </div>

                </div>
                <!-- END : SUMMARY BOX -->




                <!-- START : MASTER BOX -->
                <div class="fjsc-box fjsc-bg fjsc-fans-spc fjsc-no-pdd m-top-bottom-bug">
                    <div class="crs-fans-els">
                        <div class="fkdvc-tls">
                            <p class="kvc-lbl kfvcl-title kdv-green">
                                مدرس دوره
                            </p>
                            <div></div>
                            <p class="kvc-lbl kfvcl-desc">
                                <?php
                                    $teacher=$pro->pro_list_teachers($result['teacher']);
                                    echo $teacher['name'].$teacher['lastname'];
                                ?>
                            </p>
                        </div>
                        <div class="dor-img-h">
                            <img src="<?php echo $teacher['image']; ?>">
                        </div>
                    </div>

                    <div class="roed-summ a-editor-block">
                        <?php echo $teacher['moarefi']; ?>
                    </div>


                </div>
                <!-- END : MASTER BOX -->





                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            پرفروش ترین ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->






            </div>
        </div>
        <div class="fked-col fc-ll spc-top-large">
            <div class="dksd-spc">


                <!-- START : IMAGE BLOCK -->
                <div class="dual-img-bl">
                    <div class="fsdv-bg" style="background-image: url('../../public/default/img/demo/img-block-bg-2.jpg');"></div>

                    <div class="lfed-img-b">
                        <img src="../public/default/img/demo/img-block-circle.png" class="mrf-img">
                        <span class="dkvf-txt-h">
                                    <span class="dkdc-txt"><?php echo $resultss['title']; ?></span>
                                </span>
                    </div>

                    <div class="lfed-img-b ldd-large">
                        <img src="../public/default/img/demo/img-block-circle.png" class="mrf-img">
                        <span class="dkvf-txt-h">
                                    <span class="dkdc-txt"><?php echo $result['title']; ?></span>
                                </span>
                    </div>

                    <div class="fkvd-tls">
                        <a href="#" class="common-button cmb-icon">
                            <i class="fa fa-download"></i>
                            دانلود سرفصل
                        </a>
                        <?php if ($result['price']!='رایگان'): ?>
                        <a href="<?php  if ($pardakht==''): if ($result['price']!='رایگان'): ?>index.php?c=basket&a=add&pro_id=<?php echo $result['id']; ?><?php endif; endif; ?>" class="common-button cmb-icon cmb-cl-green">
                            <i class="fa fa-shopping-cart"></i>
                            <?php
                                if (@$pardakht!=''){
                                    echo "شما قبلا این دوره را خریداری کرده اید";
                                }else{
                                    echo number_format($result['price'])." تومان";
                                }
                            ?>
                        </a>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END : IMAGE BLOCK -->




                <!-- START : DESCRIPTION BLOCK -->
                <div class="fjsc-bg">
                    <div class="dfdg-ddke-spc">

                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <h2 class="fgfl-vv">
                                <?php echo $result['title']; ?>
                            </h2>
                        </div>

                        <div class="fkd-dvspc-cont a-editor-block aeb-no-after aeb-overflow-auto">
                            <?php echo $result['text']; ?>
                        </div>
                        <br />
                        <br />
                        <div class="rorrm-tls">
                            <div class="oee-lft">
                                <a href="<?php  if ($pardakht==''): if ($result['price']!='رایگان'): ?>index.php?c=basket&a=add&pro_id=<?php echo $result['id']; ?><?php endif; endif;?>" class="common-button cmb-icon cmb-large cmb-cl-green">
                                    <i class="fa fa-shopping-cart"></i>
                                    <?php
                                        if ($result['price']!='رایگان'){
                                            if (@$pardakht!=''){
                                                echo "شما قبلا این دوره را خریداری کرده اید";
                                            }else{
                                                echo "اضافه به سبد خرید";
                                            }
                                        }else{
                                            echo "دانلود دوره در لیست جلسات آموزشی";
                                        }
                                    ?>
                                </a>
                            </div>
                            <div class="oee-rgt">
                                <div class="fkf-ssdvv">
                                                                        <span class="common-tag-link">
                                                25% تخفیف
                                                <span class="ctl-frw-triangle"></span>
                                            </span>
                                            <span class="common-tag-link ctl-green">
                                                <?php if ($result['price']!='رایگان'): ?>
                                                <span class="ctl-frw-triangle"></span>
                                                    <?php echo number_format($result['price']); ?> هزار تومان
                                                <?php else:echo "رایگان"; endif; ?>
                                            </span>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
                <!-- END : DESCRIPTION BLOCK -->




                <!-- START : COURSES BLOCK -->
                <div class="fjsc-bg block-spc-b">
                    <div class="dfdg-ddke-spc">

                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <h2 class="fgfl-vv">
                                لیست جلسات آموزشی
                            </h2>
                        </div>



                        <!-- START : COURSE TABLE -->
                        <div class="a-editor-block">
                            <table border="0" cellpadding="0" cellspacing="0" class="common-tbl course-tbl">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        #
                                        جلسه
                                    </th>
                                    <th scope="col">
                                        سرفصل هر بخش
                                    </th>
                                    <th scope="col">
                                        زمان هر بخش
                                    </th>
                                    <th scope="col">
                                        دانلود فایل
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($files as $key=>$file): ?>
                                <tr>
                                        <td><?php echo $key+1 ?></td>
                                        <td>
                                                        <span class="square-icon">
                                                            <?php if (@$pardakht==''):?>
                                                                <?php if ($file['status']==0): ?>
                                                                    <i class="fa fa-download"></i>
                                                                <?php endif; ?>
                                                                <?php if ($file['status']==1): ?>
                                                                    <i class="fa fa-lock"></i>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                            <?php if (@$pardakht!=''):?>
                                                                <i class="fa fa-download"></i>
                                                            <?php endif; ?>
                                                        </span>
                                            جلسه <?php echo $file['jalase']; ?> : <?php echo $file['title']; ?>
                                        </td>
                                        <td><?php echo $file['time']; ?></td>
                                        <td>
                                            <?php if ($file['status']==1): ?>
                                                <?php if (@$pardakht!=''):?>
                                                <a href="<?php echo $file['file']; ?>">
                                                    <button class="common-button cmb-cl-green" href="#">
                                                        دانلود مستقیم
                                                    </button>
                                                </a>
                                                <?php endif; ?>
                                                <?php if (@$pardakht==''):?>
                                                <a>
                                                     ویژه خریداران دوره
                                                </a>
                                                <?php endif; ?>
                                            <?php endif;  ?>
                                            <?php if ($file['status']==0): ?>
                                            <a href="<?php echo $file['file']; ?>">
                                                <button class="common-button cmb-cl-orange" href="#">
                                                    دانلود رایگان
                                                </button>
                                            </a>
                                            <?php endif;  ?>
                                        </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- END : COURSE TABLE -->


                    </div>
                </div>
                <!-- END : COURSES BLOCK -->




                <!-- START : SIMILAR BLOCK -->
                <div class="block-spc-b course-sim-bl">
                    <div class="dlgf-head dh-dark-text dh-blue-color">
                        <h2 class="fgfl-vv">
                            دوره های مشابه
                        </h2>
                        <div class="sim-ctrl">
                                    <span class="ctrl ct-prev trs-2">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                            <span class="ctrl ct-next trs-2">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                        </div>
                    </div>



                    <!-- START : SIMILAR SLIDER -->
                    <div class="vdls-slid">

                        <div class="ds-viewer">
                            <div class="ds-relator">
                                <div class="ds-dragger">

                                    <!-- START : LOOP -->
                                    <?php foreach ($results as $val): ?>
                                            <div class="ds-item">
                                                <div class="lfffk-spc">
                                                    <div class="cmn-box-item">
                                                        <div class="cbi-spc">
                                                            <div class="cbi-img-h dotted-patt">

                                                                <img src="<?php echo $val['image']; ?>">

                                                                <a class="cmn-r-more-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="index.php?c=pro&a=detail&id=<?php echo $val['id']; ?>&procat=<?php echo $val['cat_id']; ?>">
                                                                            <span class="ddf-sdd">
                                                                                <span class="dvl-btn common-effect-elem trs-2">
                                                                                    <i class="fa fa-angle-left"></i>
                                                                                    ادامه مطلب
                                                                                </span>
                                                                            </span>
                                                                </a>

                                                                <span class="mkrv-label">
                                                                            <i class="fa fa-star"></i>
                                                                            <span class="kds-d-txt">
                                                                               <?php
                                                                               if ($val['price']==''){
                                                                                   echo "کاملا رایگان";
                                                                               }else{
                                                                                   echo "ویژه";
                                                                               }
                                                                               ?>
                                                                            </span>
                                                                        </span>

                                                                <div class="fksd-tls">
                                                                            <span class="fksd-itm fksd-discount">
                                                                                20% تخفیف
                                                                            </span><span class="fksd-itm fksd-price">
                                                                         <?php if ($result['price']!='رایگان'): ?>
                                                                                <?php echo number_format($val['price']); ?> تومان
                                                                        <?php else:echo "رایگان"; endif;?>
                                                                            </span>
                                                                </div>

                                                            </div>
                                                            <div class="lcps-tx-bl">
                                                                <a class="lcp-tx trs-2" href="index.php?c=pro&a=detail&id=<?php echo $val['id']; ?>&procat=<?php echo $val['cat_id']; ?>">
                                                                    <?php echo $val['title']; ?>
                                                                </a>
                                                            </div>
                                                            <div class="kfdd-tls fantasy-shadow">
                                                                <div class="lddv-st-h">
                                                                    <span class="common-star-rate csr-active1" data-value="3" data-max="5" data-icon-full="fa fa-star" data-icon-null="fa fa-star-o"></span>
                                                                </div>
                                                                <p class="ksdd-ts">
                                                                    <i class="fa fa-clock-o"></i>
                                                                    مدت دوره : <?php echo $val['time']; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                    <?php endforeach; ?>
                                    <!-- END : LOOP -->

                                    <div class="ds-clear"></div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- END : SIMILAR SLIDER -->



                </div>
                <!-- END : SIMILAR BLOCK -->














                <!-- START : COMMENTS -->
                <div class="fjsc-bg block-spc-b">
                    <div class="dfdg-ddke-spc">
                        <?php if ($comments!=''): ?>
                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <h2 class="fgfl-vv">
                                نظرات کاربران
                            </h2>
                        </div>


                        <ul class="rfmdo-cmtn">
                            <?php foreach ($comment as $value): ?>
                            <li class="cmtn-it">
                                <div class="cmtn-txt">
                                   <?php echo $value['text']; ?>
                                </div>
                                <div class="cmtn-tls">
                                            <span class="cmtn-name">
                                                “<?php echo $value['user_name']; ?>“ در <?php echo $value['date']; ?>
                                            </span>
                                    <span class="cmtn-rate">

                                            </span>
                                </div>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; if ($comments==''):?>
                            <div class="dlgf-head dh-dark-text dh-blue-color">
                                <h2 class="fgfl-vv">
                                    شما اولین نفری باشید که بعد از استفاده این محصول نظر خود را بیان کنید!
                                </h2>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END : COMMENTS -->














                <!-- START : SEND COMMENT -->
                <div class="fjsc-bg block-spc-b">
                    <div class="dfdg-ddke-spc">
                        <?php if (isset($_SESSION['user_id'])): ?>
                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <h2 class="fgfl-vv">
                                ارسال نظر
                            </h2>
                        </div>

                        <form class="cmtn-frm" method="post" action="index.php?c=pro&a=comment&id=<?php echo $_GET['id']; ?>&procat=<?php echo $_GET['procat']; ?>&user=<?php echo $_SESSION['user_id']; ?>">
                            <div class="kfdv-row">
                                <div class="clc first">
                                    <div class="dlc-spc">
                                        <input class="frm-el-inpt erow---name-inpt trs-2" name="frm[name]" type="text" placeholder="نام">
                                    </div>
                                </div>
                                <div class="clc second">
                                    <div class="dlc-spc">
                                        <input class="frm-el-inpt erow---email-inpt trs-2" name="frm[email]" type="text" placeholder="ایمیل">
                                    </div>
                                </div>
                            </div>
                            <div class="kfdv-row">
                                <textarea class="frm-el-inpt erow---comment-inpt trs-2" name="frm[text]" placeholder="نظر"></textarea>
                            </div>
                            <div class="kfdv-row rw-tls">
                                <div class="fdkt-conf" style="display: none">
                                    <input class="frm-el-inpt erow---captcha-inpt trs-2" value="10" type="text" placeholder="کد امنیتی">
                                    <span class="fkr-tms">
                                                = 2+3
                                            </span>
                                </div>
                                <div class="lgg-btn-h">
                                    <button class="common-button cmb-icon prtn-submitter" type="submit" name="btn">
                                        <i class="fa fa-envelope"></i>
                                        ارسال نظر
                                    </button>
                                </div>
                            </div>
                        </form>
                        <?php endif; if (!isset($_SESSION['user_id'])): ?>
                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <a href="index.php?c=user&a=login">
                                <h2 class="fgfl-vv">
                                    برای ارسال نظر ابتدا باید سایت وارد شوید.
                                </h2>
                            </a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END : SEND COMMENT -->

            </div>
        </div>

    </div>
</section>
<!-- END : DETAIL BLOCK -->



<!-- END : PAGE CONTENT -->

</body>
</html>