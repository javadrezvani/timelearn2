<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->


<!-- START : IMAGE ROW -->
<section class="rdxol-ugm">
    <div class="sys-container xelx-cont">
        <div class="xelx-spc f-parent">

            <div class="orsl-img-bl">
                <img src="../../public/default/img/demo/img-block-circle.png">
            </div>

            <div class="kdx-bg-bl"></div>

            <h2 class="fmce-title">فایل های آموزشی</h2>
            <p class="fmce-desc">شامل ده ها فایل آموزشی رایگان و پریمیوم</p>

            <span class="common-circ-btn trs-2 skcvc-btn">
                        <i class="fa fa-align-justify"></i>
                        <?php echo count($result); ?> دوره آموزشی
                    </span>

        </div>
    </div>
</section>
<!-- END : IMAGE ROW -->




<!-- START : DETAIL BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont f-parent">

        <div class="fked-col fc-rr">
            <div class="dksd-spc">
                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست مقاله ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->

                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست فایل ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->
            </div>
        </div>
        <div class="fked-col fc-ll spc-top-large">
            <div class="dksd-spc">


                <!-- START : BOX ITEMS -->
                <div class="ddc-itm-h">
                    <?php
                        foreach ($result as $value):
                    ?>
                    <div class="cmn-box-item">
                        <div class="cbi-spc">
                            <div class="cbi-img-h dotted-patt">

                                <img src="<?php echo $value['image']; ?>">

                                <a class="cmn-r-more-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="index.php?c=pro&a=detail&id=<?php echo $value['id']; ?>&procat=<?php echo $value['cat_id']; ?>">
                                            <span class="ddf-sdd">
                                                <span class="dvl-btn common-effect-elem trs-2">
                                                    <i class="fa fa-angle-left"></i>
                                                    ادامه مطلب
                                                </span>
                                            </span>
                                </a>

                                <span class="mkrv-label">
                                            <i class="fa fa-star"></i>
                                            <span class="kds-d-txt">
                                                <?php
                                                    if ($value['price']=='رایگان'){
                                                        echo "کاملا رایگان";
                                                    }else{
                                                        echo "ویژه";
                                                    }
                                                ?>
                                            </span>
                                        </span>

                                <div class="fksd-tls">
                                            <span class="fksd-itm fksd-discount">
                                                20% تخفیف
                                            </span><span class="fksd-itm fksd-price">
                                                <?php
                                                 if ($value['price']=='رایگان'){
                                                    echo "کاملا رایگان";
                                                 }else{
                                                    echo number_format($value['price'])." تومان";
                                                 }
                                                ?>
                                            </span>
                                </div>

                            </div>
                            <div class="lcps-tx-bl">
                                <a class="lcp-tx trs-2" href="index.php?c=pro&a=detail&id=<?php echo $value['id']; ?>&procat=<?php echo $value['cat_id']; ?>">
                                    <?php echo $value['title']; ?>
                                </a>
                            </div>
                            <div class="kfdd-tls fantasy-shadow">
                                <div class="lddv-st-h">
                                    <span class="common-star-rate csr-active1" data-value="3" data-max="5" data-icon-full="fa fa-star" data-icon-null="fa fa-star-o"></span>
                                </div>
                                <p class="ksdd-ts">
                                    <i class="fa fa-clock-o"></i>
                                    مدت دوره : <?php echo $value['time']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <!-- END : BOX ITEMS -->


<!--                <div class="not-found-block nfb-white">-->
<!--                    موردی یافت نشد!-->
<!--                </div>-->



            </div>
        </div>

    </div>
</section>
<!-- END : DETAIL BLOCK -->



<!-- END : PAGE CONTENT -->


