<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->




<!-- START : IMAGE ROW -->
<section class="rdxol-ugm">
    <div class="sys-container xelx-cont">
        <div class="xelx-spc f-parent">

            <div class="orsl-img-bl">
                <img src="../../public/default/img/demo/img-block-circle.png">
            </div>

            <div class="kdx-bg-bl"></div>

            <h2 class="fmce-title">لیست مقالات وب سایت</h2>
            <p class="fmce-desc">شامل ده ها مقاله مفید</p>

            <span class="common-circ-btn trs-2 skcvc-btn">
                        <i class="fa fa-align-justify"></i>
                        <?php echo count($result); ?> مقاله
                    </span>

        </div>
    </div>
</section>
<!-- END : IMAGE ROW -->




<!-- START : DETAIL BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont f-parent">

        <div class="fked-col fc-rr">
            <div class="dksd-spc">

                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست ویدیو ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->





                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست فایل ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->



            </div>
        </div>
        <div class="fked-col fc-ll spc-top-large">
            <div class="dksd-spc">


                <!-- START : BOX ITEMS -->
                <div class="ddc-itm-h ddc-row">
                    <?php foreach ($result as $val): ?>

                    <div class="ddc-row-bg fantasy-shadow f-parent">
                        <div class="dkdv-rc">
                            <div class="cmn-box-item">
                                <div class="cbi-spc">
                                    <div class="cbi-img-h dotted-patt">

                                        <img src="<?php echo $val['image']; ?>">

                                        <a class="cmn-r-more-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="index.php?c=blog&a=detail&id=<?php echo $val['id']; ?>&procat=<?php echo $val['cat_id']; ?>">
                                                    <span class="ddf-sdd">
                                                        <span class="dvl-btn common-effect-elem trs-2">
                                                            <i class="fa fa-angle-left"></i>
                                                            ادامه مطلب
                                                        </span>
                                                    </span>
                                        </a>


                                </div>
                                <div class="kfdd-tls dark-bg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dkdv-lc">
                            <div class="kdvd-brd">
                                <div class="fkdlt-top">
                                    <a class="dldf-lnk trs-2" href="#">
                                        <?php echo $val['title']; ?>
                                    </a>
                                    <ul class="fdlfc-its">
                                        <li>
                                            <i class="fa fa-calendar"></i>
                                            <?php echo $val['date']; ?>
                                        </li>
                                        <li>
                                            <i class="fa fa-user"></i>
                                            نویسنده : <?php echo $val['nevisandeh']; ?>
                                        </li>
                                    </ul>
                                </div>

                                <div class="kddc-desc a-editor-block aeb-no-after aeb-overflow-auto">
                                    <?php echo $val['titletext']; ?>
                                </div>
                            </div>
                            <div class="dkdc-tls">
                                <a href="index.php?c=blog&a=detail&id=<?php echo $val['id']; ?>&procat=<?php echo $val['cat_id']; ?>" class="common-button cmb-icon">
                                    <i class="fa fa-angle-left"></i>
                                    ادامه مطلب
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <!-- END : BOX ITEMS -->
                <?php foreach ($result as $item): ?>
                    <?php if ($item['title']==''): ?>
                        <div class="not-found-block nfb-white">
                            موردی یافت نشد!
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>


<!--                <ul class="cmn-pagination">-->
<!--                    <a class="cmn-p-ln cmn-p-w-auto trs-2" href="#">قبلی</a>-->
<!--                    <a class="cmn-p-ln trs-2" href="#">1</a>-->
<!--                    <a class="cmn-p-ln trs-2 active" href="#">2</a>-->
<!--                    <a class="cmn-p-ln trs-2" href="#">3</a>-->
<!--                    <a class="cmn-p-ln trs-2" href="#">4</a>-->
<!--                    <a class="cmn-p-ln cmn-p-w-auto trs-2" href="#">بعدی</a>-->
<!--                </ul>-->


            </div>
        </div>

    </div>
</section>
<!-- END : DETAIL BLOCK -->



<!-- END : PAGE CONTENT -->