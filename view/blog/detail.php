
<!-- START : PAGE CONTENT -->

<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">وبلاگ</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->




<!-- START : DETAIL BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont f-parent">

        <div class="fked-col fc-rr">
            <div class="dksd-spc">


                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست ویدیو ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->





                <!-- START : LIST BOX -->
                <div class="fjsc-box fjsc-bg">
                    <div class="common-linear-head cm-lh-mini">
                        <h3 class="cm-lh-title">
                            لیست فایل ها
                        </h3>
                    </div>

                    <ul class="grlrt-ul dkfx-ce">
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets توسط مدرس آکسفورد
                                        </span>
                                <!-- <span class="flder-cc trs-2">
                                    قیمت : 20,000 تومان
                                </span> -->
                            </a>
                        </li>
                        <li class="f-parent">
                            <a href="#" class="gfkrt-img-h">
                                <img src="../../public/default/img/demo/side-img.jpg">
                                <span class="gfkr-ssh trs-2">
                                            <span class="gflf-ccmi">
                                                <i class="fa fa-angle-left"></i>
                                            </span>
                                        </span>
                            </a>
                            <a href="#" class="gflr-gfkf">
                                        <span class="trrl-ss trs-2">
                                            فایل آموزشی کار با Brackets
                                        </span>
                                <span class="flder-cc trs-2">
                                            قیمت : 20,000 تومان
                                        </span>
                            </a>
                        </li>
                    </ul>

                    <div class="dks-sct-tls">
                        <a class="common-circ-btn trs-2" href="#">
                            <i class="fa fa-angle-left"></i>
                            لیست کامل
                        </a>
                    </div>

                </div>
                <!-- END : LIST BOX -->

            </div>
        </div>


        <div class="fked-col fc-ll spc-top-large">
            <div class="dksd-spc">

                <div class="fjsc-bg">
                    <div class="dfdg-ddke-spc">

                        <div class="dlgf-head dh-dark-text dh-blue-color">
                            <h2 class="fgfl-vv">
                                <?php echo $result['title']; ?>
                            </h2>
                        </div>

                        <div class="fkd-dvspc-cont a-editor-block aeb-no-after aeb-overflow-auto">
                            <?php echo $result['text']; ?>
                        </div>

                    </div>
                </div>

                <!-- START : COMMENTS -->
                <div class="fjsc-bg block-spc-b">
                    <div class="dfdg-ddke-spc">
                        <?php if ($comments!=''): ?>
                            <div class="dlgf-head dh-dark-text dh-blue-color">
                                <h2 class="fgfl-vv">
                                    نظرات کاربران
                                </h2>
                            </div>


                            <ul class="rfmdo-cmtn">
                                <?php foreach ($comment as $value): ?>
                                    <li class="cmtn-it">
                                        <div class="cmtn-txt">
                                            <?php echo $value['text']; ?>
                                        </div>
                                        <div class="cmtn-tls">
                                            <span class="cmtn-name">
                                                “<?php echo $value['user_name']; ?>“ در <?php echo $value['date']; ?>
                                            </span>
                                            <span class="cmtn-rate">

                                            </span>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; if ($comments==''):?>
                            <div class="dlgf-head dh-dark-text dh-blue-color">
                                <h2 class="fgfl-vv">
                                    شما اولین نفری باشید که درباره این مقاله نظر خود را بیان کنید!
                                </h2>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END : COMMENTS -->














                <!-- START : SEND COMMENT -->
                <div class="fjsc-bg block-spc-b">
                    <div class="dfdg-ddke-spc">
                        <?php if (isset($_SESSION['user_id'])): ?>
                            <div class="dlgf-head dh-dark-text dh-blue-color">
                                <h2 class="fgfl-vv">
                                    ارسال نظر
                                </h2>
                            </div>

                            <form class="cmtn-frm" method="post" action="index.php?c=blog&a=comment&id=<?php echo $_GET['id']; ?>&procat=<?php echo $_GET['procat']; ?>&user=<?php echo $_SESSION['user_id']; ?>">
                                <div class="kfdv-row">
                                    <div class="clc first">
                                        <div class="dlc-spc">
                                            <input class="frm-el-inpt erow---name-inpt trs-2" name="frm[name]" type="text" placeholder="نام">
                                        </div>
                                    </div>
                                    <div class="clc second">
                                        <div class="dlc-spc">
                                            <input class="frm-el-inpt erow---email-inpt trs-2" name="frm[email]" type="text" placeholder="ایمیل">
                                        </div>
                                    </div>
                                </div>
                                <div class="kfdv-row">
                                    <textarea class="frm-el-inpt erow---comment-inpt trs-2" name="frm[text]" placeholder="نظر"></textarea>
                                </div>
                                <div class="kfdv-row rw-tls">
                                    <div class="fdkt-conf" style="display: none">
                                        <input class="frm-el-inpt erow---captcha-inpt trs-2" value="10" type="text" placeholder="کد امنیتی">
                                        <span class="fkr-tms">
                                                = 2+3
                                            </span>
                                    </div>
                                    <div class="lgg-btn-h">
                                        <button class="common-button cmb-icon prtn-submitter" type="submit" name="btn">
                                            <i class="fa fa-envelope"></i>
                                            ارسال نظر
                                        </button>
                                    </div>
                                </div>
                            </form>
                        <?php endif; if (!isset($_SESSION['user_id'])): ?>
                            <div class="dlgf-head dh-dark-text dh-blue-color">
                                <a href="index.php?c=user&a=login">
                                    <h2 class="fgfl-vv">
                                        برای ارسال نظر ابتدا باید سایت وارد شوید.
                                    </h2>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END : SEND COMMENT -->

            </div>
        </div>

    </div>
</section>
<!-- END : DETAIL BLOCK -->



<!-- END : PAGE CONTENT -->



