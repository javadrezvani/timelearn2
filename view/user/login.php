

<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : MIDDLE BLOCK -->
<section class="fkfg-lgn-pup on-ground trs-2">
    <form class="frm-on-ground-login krrd-cont trs-2" method="post">

        <div class="fkf-rvvr">

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        ورود به سایت
                    </h1>
                </div>

                <div class="fdkr-rdsc">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt login-page---username-inpt trs-2" name="frm[email]" type="text" value="<?php if (isset($_COOKIE['remember1'])){echo $_COOKIE['remember1']; } ?>" placeholder="نام کاربری">
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt login-page---pass-inpt trs-2" name="frm[password]" value="<?php if (isset($_COOKIE['remember2'])){echo $_COOKIE['remember2']; } ?>" type="password" placeholder="رمز عبور">
                        <input type="submit" class="hidden-elem">
                    </div>
                </div>

                <div class="fkyf-rr">
                    <label class="krrd-dt">
                        <input class="fkrr-ddc" type="checkbox" name="frm[remember]">
                        <span class="fkr-ectg">
                                    مرا به خاطر بسپار
                                </span>
                    </label>
                    <div class="krrd-dt">
                        <a class="fkr-ectg trs-2" href="index.php?c=user&a=retrive-password">
                            <i class="fa fa-lock"></i>
                            رمز خود را فراموش کرده اید؟
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <div class="kft-tls f-parent">
            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter" name="btn" type="submit">
                <i class="fa fa-sign-in"></i>
               ورود به وب سایت
            </button>
        </div>
    </form>
</section>
<!-- END : MIDDLE BLOCK -->



<!-- END : PAGE CONTENT -->




