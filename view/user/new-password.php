<!-- START : PAGE CONTENT -->
<!-- START : NEWSLETTER POPUP -->
<div class="gkl-dtte common-popup cpp-show trs-2">
    <form class="feld-cnt drlr-dgglr-frm common-popup-cont trs-2" method="post" action="#newsletter">

        <div class="mkf-scsc">
            <div class="klfg-lft">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="gfkvf-str">
                    <h2 class="fkl-3d3">
                        به آکادمی آی تی خوش آمدید
                    </h2>
                    <div></div>
                    <div class="rtg-tdl">
                        اولین هدیه خودتو از ما دریافت کن
                    </div>
                </div>

                <div class="gfk-img-h">
                    <img src="../public/default/img/common/nw-ltt-img.jpg">
                    <a class="lds-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="https://academyit.net/قدم-صفر-طراحی-سایت">
                                <span class="flgf-svs common-effect-elem trs-2">
                                    <i class="fa fa-reply"></i>
                                </span>
                    </a>
                </div>

            </div>
            <div class="flf-rght">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="flfs-scp">
                    <div class="dlgf-head dh-dark-text dh-blue-color">
                        <h1 class="fgfl-vv">
                            رایگان :: دوره ویدیویی قدم صفر طراحی سایت
                        </h1>
                    </div>
                    <div class="flfer-ssc a-editor-block">
                        <h2 class="fgfl-vv-h2">
                            اما چرا از هر 10 نفری که تصمیم دارن طراح سایت بشن، 8 نفر هیچ وقت موفق  نمیشن!
                        </h2>
                        <p>&nbsp;</p>
                        طی این 8- 9 سالی که در زمینه آموزش توی حوزه های مختلف وب کار میکنم  با صدها دانشجو برخورد کردم که همگی روز اول قصد داشتن یه طراح سایت بشن ، یه شرکت بزنن و کلی کار انجام بدن
                        اما وقتی بعد چند ماه از اتمام کلاس ها باهاشون تماس میگرفتم و بررسی میکردم میدیدم معمولا دنبال یه شغل دیگه رفتن و یا اینکار رو دارن در کنار  یک کار دیگه انجام میدن ،
                        شاید بتونی حس کنی چقدر آدم سرخورده میشه و حالش بد میشه وقتی  که کلی زمان و هزینه صرف میکنی و به هیچ دستاوردی نمیرسی !
                        <p>
                            اگر دوست نداری این اتفاق برای تو دوست خوبم بیفته در این دوره  آموزشی کاملا رایگان با من همراه باش
                        </p>
                    </div>
                    <script type="text/javascript" src="https://app.mailerlite.com/data/webforms/546031/k9f6h2.js?v4"></script>
                </div>
            </div>
        </div>

    </form>
</div>

<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : MIDDLE BLOCK -->
<section class="fkfg-lgn-pup on-ground trs-2">
    <form class="frm-change-pass krrd-cont trs-2" method="post">

        <div class="fkf-rvvr">

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        تغییر رمز عبور
                    </h1>
                </div>

                <div class="fdkr-rdsc no-border">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt change-pass---new-inpt trs-2" type="password" placeholder="رمز عبور جدید">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt change-pass---repeat-inpt trs-2" name="frm[password]" type="password" placeholder="تکرار رمز عبور جدید">
                        <p class="frm-err-h"></p>
                        <input type="submit" class="hidden-elem">
                    </div>
                </div>

            </div>
        </div>

        <div class="kft-tls f-parent">
            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter" name="btn" type="submit">
                <i class="fa fa-sign-in"></i>
                تغییر رمز عبور
            </button>
        </div>

    </form>
</section>
<!-- END : MIDDLE BLOCK -->



<!-- END : PAGE CONTENT -->

