

<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : MIDDLE BLOCK -->
<section class="fkfg-lgn-pup on-ground trs-2">
    <form class="frm-signup krrd-cont trs-2" method="post" action="index.php?c=user&a=registered">

        <div class="fkf-rvvr">

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        عضویت در سایت
                    </h1>
                </div>

                <div class="fdkr-rdsc no-border">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---name-inpt trs-2" type="text" name="frm[name]" placeholder="نام">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---last-name-inpt trs-2" type="text" name="frm[lastname]" placeholder="نام خانوادگی">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---email-inpt trs-2" type="text" name="frm[email]" placeholder="آدرس ایمیل">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---mobile-inpt trs-2" type="text" name="frm[phone]" placeholder="شماره همراه">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <select class="frm-el-inpt signup---gender-inpt trs-2" name="frm[gender]">
                            <option value="0">جنسیت</option>
                            <option value="1">مرد</option>
                            <option value="2">زن</option>
                        </select>
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---pass-inpt trs-2" type="password" name="frm[password]" placeholder="رمز عبور">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt signup---pass-repeat-inpt trs-2" type="password" placeholder="تکرار رمز عبور">
                        <p class="frm-err-h"></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="kft-tls f-parent">
            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter" name="btn" type="submit">
                <i class="fa fa-sign-in"></i>
                ثبت نام
            </button>
        </div>

    </form>
</section>
<!-- END : MIDDLE BLOCK -->



<!-- END : PAGE CONTENT -->




