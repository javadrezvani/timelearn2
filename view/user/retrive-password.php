

<!-- START : NEWSLETTER POPUP -->
<div class="gkl-dtte common-popup cpp-show trs-2">
    <form class="feld-cnt drlr-dgglr-frm common-popup-cont trs-2" method="post" action="#newsletter">

        <div class="mkf-scsc">
            <div class="klfg-lft">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="gfkvf-str">
                    <h2 class="fkl-3d3">
                       بازیابی
                    </h2>
                    <div></div>
                    <div class="rtg-tdl">
                        بازیابی رمز عبور اکانت
                    </div>
                </div>

                <div class="gfk-img-h">
                    <img src="../public/default/img/common/nw-ltt-img.jpg">
                    <a class="lds-shell common-self-effect cse-fade trs-2 common-effect-parent ce-scale" href="index.php?c=user&a=retrive-password">
                                <span class="flgf-svs common-effect-elem trs-2">
                                    <i class="fa fa-reply"></i>
                                </span>
                    </a>
                </div>

            </div>
            <div class="flf-rght">

                        <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.gkl-dtte">
                            <i class="fa fa-times"></i>
                        </span>

                <div class="flfs-scp">
                    <div class="dlgf-head dh-dark-text dh-blue-color">
                        <h1 class="fgfl-vv">
                            بازیابی رمز عبور
                        </h1>
                    </div>
                    <div class="flfer-ssc a-editor-block">
                        <h2 class="fgfl-vv-h2">
                            بعد از وارد کردن ایمیل خودتان یک ایمیل بازیابی رمز عبور اکانت برای شما ارسال می شود که میتوانید رمز عبور خود را تغییر دهید.
                        </h2>
                        <p>&nbsp;</p>

                    </div>
                </div>
            </div>
        </div>

    </form>
</div>

<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : MIDDLE BLOCK -->
<section class="fkfg-lgn-pup on-ground trs-2">
    <form class="frm-retrive-pass krrd-cont trs-2" method="post">

        <div class="fkf-rvvr">

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        بازیابی رمز عبور
                    </h1>
                </div>

                <div class="fdkr-rdsc no-border">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt retrive-pass---email-inpt trs-2" name="frm[email]" type="text" placeholder="ایمیل شما">
                        <p class="frm-err-h"></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="kft-tls f-parent">
            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter" type="submit" name="btn">
                <i class="fa fa-sign-in"></i>
                دریافت ایمیل بازیابی
            </button>
        </div>

    </form>
</section>
<!-- END : MIDDLE BLOCK -->



<!-- END : PAGE CONTENT -->