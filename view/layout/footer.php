<!-- START : FOOTER -->
<footer class="fkf-footer">
    <div class="sys-container fkfg-cont">
        <div class="dfdkf-cl-h f-parent">
            <?php foreach ($footer as $val): ?>
            <div class="gflfd-cl">
                <div class="kffg-spc">
                    <h2 class="fkf-ccgrw"><?php echo $val['title']; ?></h2>
                    <ul class="fkfg-c-ul">
                        <li>
                            <a href="<?php echo $val['url1']; ?>"><?php echo $val['link1']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url2']; ?>"><?php echo $val['link2']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url3']; ?>"><?php echo $val['link3']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url4']; ?>"><?php echo $val['link4']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url5']; ?>"><?php echo $val['link5']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url6']; ?>"><?php echo $val['link6']; ?></a>
                        </li>
                        <li>
                            <a href="<?php echo $val['url7']; ?>"><?php echo $val['link7']; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="gflfd-cl">
                <div class="kffg-spc">
                    <h2 class="fkf-ccgrw">اطلاعات تماس</h2>
                    <ul class="dlf-fgg-ul">
                        <li>
                            <a class="trs-2" href="#">
                                <i class="fa fa-envelope"></i>
                                <?php echo $settings['address']; ?>
                            </a>
                        </li>
                        <li>
                            <i class="fa fa-phone"></i>
                            <?php echo $settings['tel']; ?>
                        </li>
                        <li>
                            <i class="fa fa-clock-o"></i>
                            <?php echo $settings['timetamas']; ?>
                        </li>
                    </ul>
                    <div class="fkvf-ffvb-txt">
                        <?php echo $settings['text']; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<!-- END : FOOTER -->




<!-- START : FOOTER BAR -->
<div class="dmkfd-m-footer">
    <div class="sys-container">
        <div class="fvkf-spcc">
            <p class="gfdf-ccp">
                <?php echo $settings['copyright']; ?>
            </p>
        </div>
    </div>
</div>
<!-- END : FOOTER BAR -->



</body>
</html>