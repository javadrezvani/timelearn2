<?php require_once 'Controller/Clayout.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $settings['title']; ?></title>
    <meta name="description" content="<?php echo $settings['description']; ?>">
    <meta name="keywords" content="<?php echo $settings['keywords']; ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo $settings['logo']; ?>">



    <!-- START : COMMON RESOURCES -->
    <link rel="stylesheet" type="text/css" href="../public/default/style/font/font-awesome-4.3.0/css/font-awesome.min.css">

    <script src="../public/default/script/library/plugin/pace/pace.min.js"></script>
    <link rel="stylesheet" href="../public/default/script/library/plugin/pace/pace.css">
    <!--<script type="text/javascript">
        paceOptions = {
            startOnPageLoad: true,
            restartOnRequestAfter: false
        }
    </script>
-->
    <script type="text/javascript" src="../public/default/script/library/common/IEHtml5.js"></script>
    <script type="text/javascript" src="../public/default/script/library/common/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/editor-block/editor.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/editor-block/editor.js"></script>

    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/flexnav-master/css/flexnav.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/flexnav-master/js/jquery.flexnav.min.js"></script>
    <!-- END : COMMON RESOURCES -->





    <!-- START : PAGE RESOURCES -->
    <script src="../public/default/script/library/plugin/particles.js-master/particles.js"></script>

    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/FlipClock-master/compiled/flipclock.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/FlipClock-master/compiled/flipclock.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/flowplayer-5.4.3/skin/minimalist.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/flowplayer-5.4.3/flowplayer.min.js"></script>


    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/jquery.dragItemSlider/main-files/jquery.dragItemSlider.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/jquery.dragItemSlider/main-files/jquery.dragItemSlider.js"></script>


    <link rel="stylesheet" type="text/css" href="../public/default/script/library/plugin/jquery.alertMessage/jquery.alertMessage.css">
    <script type="text/javascript" src="../public/default/script/library/plugin/jquery.alertMessage/jquery.alertMessage.js"></script>


    <script type="text/javascript">
        var particlesActive = false;
        var timerSeconds = 3600 * 24 * 3;

        // $(document).ready(function(){
        //     $.alertMessage('title','context',0,'info || success || error');
        //     $.alertMessage("عنوان","متن",0,"success");
        // });
    </script>
    <!-- END : PAGE RESOURCES -->




    <!-- START : COMMON RESOURCES -->
    <link rel="stylesheet" type="text/css" href="../public/default/style/style.css">
    <link rel="stylesheet" type="text/css" href="../public/default/style/media.css">

    <script type="text/javascript" src="../public/default/script/validator.js"></script>
    <script type="text/javascript" src="../public/default/script/validate.js"></script>
    <script type="text/javascript" src="../public/default/script/script.js"></script>
    <!-- END : COMMON RESOURCES -->


</head>
<body>




<!-- START : LOADING
<div class="pace-overlay"></div>
 END : LOADING -->




<!-- END : NEWSLETTER POPUP -->





<!-- START : LOGIN POPUP -->
<div class="fkfg-lgn-pup common-popup trs-2">
    <form class="fkf-fv-cnt common-popup-cont trs-2" method="post" action="#submitted">

        <div class="fkf-rvvr">

                    <span class="common-close-btn trs-2 common-popup-cls-btn" data-target-popup=".common-popup.fkfg-lgn-pup">
                        <i class="fa fa-times"></i>
                    </span>

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        ورود به سایت
                    </h1>
                </div>

                <div class="fdkr-rdsc">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt kft---username-inpt trs-2" type="text" placeholder="نام کاربری">
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt kft---pass-inpt trs-2" type="password" placeholder="رمز عبور">
                        <input type="submit" class="hidden-elem">
                    </div>
                </div>

                <div class="fkyf-rr">
                    <label class="krrd-dt">
                        <input class="fkrr-ddc" type="checkbox">
                        <span class="fkr-ectg">
                                    مرا به خاطر بسپار
                                </span>
                    </label>
                    <div class="krrd-dt">
                        <a class="fkr-ectg trs-2" href="#">
                            <i class="fa fa-lock"></i>
                            رمز خود را فراموش کرده اید؟
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="kft-tls f-parent">
            <a href="#" class="common-button cmb-icon cmb-cl-red fkew-rght">
                <i class="fa fa-google-plus"></i>
                ورود با اکانت گوگل
            </a>
            <span class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter">
                        <i class="fa fa-sign-in"></i>
                        ورود به سایت
                    </span>
        </div>

    </form>
</div>
<!-- END : LOGIN POPUP -->




<!-- START : HEADER & NAV -->
<section class="tyty-dlkrr">

    <div class="ykty-mini-h">
        <div class="sys-container gklt-cont f-parent">




            <!-- START : MINI NAV DESKTOP -->
            <div class="tmkth-ccm">
                <ul class="ylyy-ul f-parent">
                    <?php foreach ($menu_up as $vals): ?>
                    <?php if ($vals['title']!='دوره های آموزشی'): ?>
                    <li class="thlt-li">
                        <a href="<?php echo $vals['url']; ?>" class="trs-2 active"><?php echo $vals['title']; ?></a>
                    </li>
                    <?php endif; ?>
                    <?php if ($vals['title']=='دوره های آموزشی'): ?>
                    <li class="thlt-li">
                        <a href="<?php echo $vals['url']; ?>" class="trs-2"><?php echo $vals['title']; ?></a>
                        <ul>
                            <?php foreach ($procat as $val): ?>
                            <li>
                                <a href="index.php?c=pro&a=list&procat=<?php echo $val['id'];?>"><?php echo $val['title']; ?></a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <?php endif; endforeach; ?>
                </ul>
            </div>
            <!-- END : MINI NAV DESKTOP -->





            <!-- START : MINI NAV MOBILE -->
            <div class="tgf-m-nav-holder fkte-mini-nav">
                <div class="mini-flexnav-menu-button menu-button">
                    منو کمکی
                </div>
                <ul class="mini-flexnav flexnav" data-breakpoint="1200" style="z-index: 10000;position:relative;">
                    <?php foreach ($mens as $val): ?>
                    <li>
                        <a href="<?php echo $val['url']; ?>" target="_self"><?php echo $val['title']; ?></a>
                    </li>
                    <?php endforeach; ?>
                    <li>
                        <a href="" target="_self"></a>
                    </li>
                </ul>
            </div>
            <!-- END : MINI NAV MOBILE -->







            <ul class="thty-scl">
                <li>
                    <a href="<?php echo $settings['twitter']; ?>" class="trs-2">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $settings['instgram']; ?>" class="trs-2">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $settings['googleplus']; ?>" class="trs-2">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>


        </div>
    </div>
    <div class="khtfg-logo-h">
        <div class="sys-container rrtgk-cont">
            <a class="lrr-lg" href="index.php">
                <img src="<?php echo $settings['logo']; ?>">
            </a>
            <div class="fktt-tls">
                <?php if (isset($_SESSION['user_id'])){
                    echo "                <a href=\"index.php?c=panel&a=basket-profile&id=$_SESSION[user_id]\" class=\"common-button cmb-icon\">
                    <i class=\"fa fa-shopping-cart\"></i>
                    سبد خرید
                </a>
                <a href=\"index.php?c=panel&a=profile&id=$_SESSION[user_id]\" class=\"common-button cmb-icon cmb-cl-green common-popup-o-btn\" data-target-popup=\".common-popup.fkfg-lgn-pup\">
                    <i class=\"fa fa-user\"></i>
                   پنل کاربری من
                </a>";
                }else{
                    echo "                <a href=\"index.php?c=user&a=register\" class=\"common-button cmb-icon\">
                    <i class=\"fa fa-plus\"></i>
                    عضویت
                </a>
                <a href=\"index.php?c=user&a=login\" class=\"common-button cmb-icon cmb-cl-green common-popup-o-btn\" data-target-popup=\".common-popup.fkfg-lgn-pup\">
                    <i class=\"fa fa-sign-in\"></i>
                    ورود به سایت
                </a>";
                } ?>
            </div>
        </div>
    </div>
    <div class="lgfr-nav f-parent">
        <div class="sys-container fltht-cont">
            <div class="mfgr-se-bl">
                <span class="gfkrr-gg"></span>
                <span class="flfg-gscc refl-le"></span>

                <div class="thlty-se-sc">
                    <input type="text" class="gflr-rv" placeholder="جستجو...">
                    <span class="gflr-btn trs-2">
                                <i class="fa fa-search"></i>
                            </span>
                </div>

                <div class="kttb-fdd trs-2">


                    <div class="gkrdf-row f-parent">
                        <div class="gkfg-col">
                            <div class="fktrt-head">
                                <div class="kfltr-dc">
                                    <h3 class="vflt-rcc">
                                        دوره های آموزشی
                                    </h3>
                                </div>
                            </div>
                            <div class="sid-spc">
                                <ul class="tlgff-ul">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="kltr-tools">
                                <a href="#" class="common-button cmb-icon cmb-cl-gray cmb-mini">
                                    <i class="fa fa-angle-left"></i>
                                    لیست کامل
                                </a>
                            </div>
                        </div>
                        <div class="gkfg-col">
                            <div class="fktrt-head fkh-blue">
                                <div class="kfltr-dc">
                                    <h3 class="vflt-rcc">
                                        دوره های آموزشی
                                    </h3>
                                </div>
                            </div>
                            <div class="sid-spc">
                                <ul class="tlgff-ul">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-angle-left"></i>
                                            آموزش CSS
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="kltr-tools">
                                <a href="#" class="common-button cmb-icon cmb-cl-gray cmb-mini">
                                    <i class="fa fa-angle-left"></i>
                                    لیست کامل
                                </a>
                            </div>
                        </div>
                    </div>




                    <div class="gkrdf-row f-parent">
                        <div class="gkfg-col">
                            <div class="fktrt-head">
                                <div class="kfltr-dc">
                                    <h3 class="vflt-rcc">
                                        دوره های آموزشی
                                    </h3>
                                </div>
                            </div>
                            <div class="sid-spc">
                                <ul class="grlrt-ul">
                                    <li class="f-parent">
                                        <a href="#" class="gfkrt-img-h">
                                            <img src="../public/default/img/demo/se-img.jpg">
                                            <span class="gfkr-ssh trs-2">
                                                        <span class="gflf-ccmi">
                                                            <i class="fa fa-angle-left"></i>
                                                        </span>
                                                    </span>
                                        </a>
                                        <a href="#" class="gflr-gfkf">
                                                    <span class="trrl-ss trs-2">
                                                        فایل آموزشی کار با Brackets
                                                    </span>
                                            <span class="flder-cc trs-2">
                                                        قیمت : 20,000 تومان
                                                    </span>
                                        </a>
                                    </li>
                                    <li class="f-parent">
                                        <a href="#" class="gfkrt-img-h">
                                            <img src="../public/default/img/demo/se-img.jpg">
                                            <span class="gfkr-ssh trs-2">
                                                        <span class="gflf-ccmi">
                                                            <i class="fa fa-angle-left"></i>
                                                        </span>
                                                    </span>
                                        </a>
                                        <a href="#" class="gflr-gfkf">
                                                    <span class="trrl-ss trs-2">
                                                        فایل آموزشی کار با Brackets
                                                    </span>
                                            <span class="flder-cc trs-2">
                                                        قیمت : 20,000 تومان
                                                    </span>
                                        </a>
                                    </li>
                                    <li class="f-parent">
                                        <a href="#" class="gfkrt-img-h">
                                            <img src="../public/default/img/demo/se-img.jpg">
                                            <span class="gfkr-ssh trs-2">
                                                        <span class="gflf-ccmi">
                                                            <i class="fa fa-angle-left"></i>
                                                        </span>
                                                    </span>
                                        </a>
                                        <a href="#" class="gflr-gfkf">
                                                    <span class="trrl-ss trs-2">
                                                        فایل آموزشی کار با Brackets
                                                    </span>
                                            <span class="flder-cc trs-2">
                                                        قیمت : 20,000 تومان
                                                    </span>
                                        </a>
                                    </li>
                                    <li class="f-parent">
                                        <a href="#" class="gfkrt-img-h">
                                            <img src="../public/default/img/demo/se-img.jpg">
                                            <span class="gfkr-ssh trs-2">
                                                        <span class="gflf-ccmi">
                                                            <i class="fa fa-angle-left"></i>
                                                        </span>
                                                    </span>
                                        </a>
                                        <a href="#" class="gflr-gfkf">
                                                    <span class="trrl-ss trs-2">
                                                        فایل آموزشی کار با Brackets
                                                    </span>
                                            <span class="flder-cc trs-2">
                                                        قیمت : 20,000 تومان
                                                    </span>
                                        </a>
                                    </li>
                                    <li class="f-parent">
                                        <a href="#" class="gfkrt-img-h">
                                            <img src="../public/default/img/demo/se-img.jpg">
                                            <span class="gfkr-ssh trs-2">
                                                        <span class="gflf-ccmi">
                                                            <i class="fa fa-angle-left"></i>
                                                        </span>
                                                    </span>
                                        </a>
                                        <a href="#" class="gflr-gfkf">
                                                    <span class="trrl-ss trs-2">
                                                        فایل آموزشی کار با Brackets
                                                    </span>
                                            <span class="flder-cc trs-2">
                                                        قیمت : 20,000 تومان
                                                    </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="kltr-tools">
                                <a href="#" class="common-button cmb-icon cmb-cl-gray cmb-mini">
                                    <i class="fa fa-angle-left"></i>
                                    لیست کامل
                                </a>
                            </div>
                        </div>
                        <div class="gkfg-col">
                            <div class="fktrt-head fkh-blue">
                                <div class="kfltr-dc">
                                    <h3 class="vflt-rcc">
                                        دوره های آموزشی
                                    </h3>
                                </div>
                            </div>


                            <div class="sid-spc">
                                <div class="not-found-block nfb-mini">
                                    موردی یافت نشد!
                                </div>
                            </div>

                        </div>
                    </div>


                </div>

            </div>

            <nav class="rktt-b-nav">
                <span class="flfg-gscc refl-ri"></span>





                <!-- START : DESKTOP NAV -->
                <ul class="trtk-ul f-parent">
                    <?php foreach ($mens as $men): ?>
                    <?php if ($men['title']!='دوره های آموزشی'): ?>
                        <?php if ($men['title']!='وبلاگ'): ?>
                            <li class="tktmk-li">
                                <a class="kote-it trs-2 active" href="<?php echo $men['url']; ?>" target="_self"><?php echo $men['title']; ?></a>
                            </li>
                         <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($men['title']=='وبلاگ'): ?>
                        <li class="tktmk-li parent rtt-norm-nav">
                            <a class="kote-it trs-2" href="<?php echo $men['url'];?>" target="_self"><?php echo $men['title'];?></a>
                            <div class="gt-spc trs-2">
                                <ul class="ttkd-subm">
                                    <?php foreach ($blogcat as $val): ?>
                                    <li class="bt-slt-li">
                                        <a class="hytd-a trs-2" href="index.php?c=blog&a=list&procat=<?php echo $val['id'];?>" target="_self">
                                            <?php echo $val['title']; ?>
                                            <i class="grr-i fa fa-angle-left"></i>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif; ?>
                    <?php if ($men['title']=='دوره های آموزشی'): ?>
                    <li class="tktmk-li">
                        <a class="kote-it trs-2" href="<?php echo $men['url']; ?>" target="_self"><?php echo $men['title']; ?></a>
                        <div class="tktot-nav trs-2 f-parent">
                            <div class="tgltg-spc">
                                <div class="ttkt-img-h">
                                    <img src="../public/default/img/demo/nav-img.png">
                                </div>
                                <div class="tmktt-cl-h f-parent">
                                    <?php foreach ($procat as $val): ?>
                                    <div class="njtt-cl">
                                        <div class="tktd-spc">
                                            <a href="index.php?c=pro&a=list&procat=<?php echo $val['id'];?>">
                                                <h3 class="mrktt-hl"><?php echo $val['title']; ?></h3>
                                            </a>
                                            <ul class="ktkt-ul">
                                                <?php
                                                $prot=$pro->pro_under_list_default($val['id']);
                                                foreach ($prot as $pros):
                                                ?>
                                                <li>
                                                    <a href="index.php?c=pro&a=detail&id=<?php echo $pros['id']; ?>&procat=<?php echo $pros['cat_id']; ?>" target="_self"><?php echo $pros['title']; ?></a>
                                                </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php endif; endforeach; ?>

                </ul>
                <!-- END : DESKTOP NAV -->





                <!-- START : NAV MOBILE -->
                <div class="tgf-m-nav-holder fkte-org-nav">
                    <div class="org-flexnav-menu-button menu-button">
                        منو کاربری
                    </div>
                    <?php foreach ($mens as $val): ?>
                    <ul class="org-flexnav flexnav" data-breakpoint="1200" style="z-index: 10000;position:relative;">
                        <?php if ($val['title']!='دوره های آموزشی'): ?>
                        <?php if ($val['title']!='وبلاگ'): ?>
                        <li>
                            <a href="<?php echo $val['url']; ?>" target="_self"><?php echo $val['title']; ?></a>
                        </li>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($val['title']=='دوره های آموزشی'): ?>
                        <li>
                            <a href="<?php echo $val['url']; ?>" target="_self"><?php echo $val['title']; ?></a>
                            <ul>
                                <?php foreach ($procat as $item): ?>
                                <li>
                                    <a href="index.php?c=pro&a=list&procat=<?php echo $item['id'];?>" target="_self"><?php echo $item['title']; ?></a>
                                    <ul>
                                        <?php
                                        $prot=$pro->pro_under_list_default($item['id']);
                                        foreach ($prot as $pros):
                                        ?>
                                        <li>
                                            <a href="index.php?c=pro&a=detail&id=<?php echo $pros['id']; ?>&procat=<?php echo $pros['cat_id']; ?>" target="_self"><?php echo $pros['title']; ?></a>
                                        </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if ($val['title']=='وبلاگ'): ?>
                            <li>
                                <a href="<?php echo $val['url']; ?>" target="_self"><?php echo $val['title']; ?></a>
                                <ul>
                                    <?php foreach ($blogcat as $item): ?>
                                        <li>
                                            <a href="index.php?c=blog&a=list&procat=<?php echo $item['id'];?>" target="_self"><?php echo $item['title']; ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <?php endforeach; ?>
                        <ul class="org-flexnav flexnav" data-breakpoint="1200" style="z-index: 10000;position:relative;">
                            <li>
                                <a href="" target="_self"></a>
                            </li>
                        </ul>
                </div>
                <!-- END : NAV MOBILE -->
            </nav>
        </div>
    </div>


</section>
<!-- END : HEADER & NAV -->