<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : PROFILE BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont">

        <div class="kdsf-spc f-parent">
            <ul class="ndk-nav">
                <li class="rksd-current">
                    <a href="index.php?c=panel&a=profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user-secret dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <span class="dkd-not"></span>
                    <a href="index.php?c=panel&a=change-password&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-lock dkd-icon"></i>
                        تغییر رمز عبور
                    </a>
                </li>
                <li>
                    <a href="index.php?c=panel&a=basket-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-shopping-cart dkd-icon"></i>
                        سبد خرید
                    </a>
                </li>
                <li>
                    <a href="index.php?c=panel&a=basket-pay&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-list-ul dkd-icon"></i>
                        دوره های خریداری شده
                    </a>
                </li>
                <li>
                    <span class="dkd-not blue red">ویرایش اطلاعات</span>
                    <a href="index.php?c=panel&a=edit-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user dkd-icon"></i>
                        ویرایش اطلاعات
                    </a>
                </li>
                <li class="dkkd-logout">
                    <a href="index.php?c=panel&a=logout">
                        <i class="fa fa-power-off dkd-icon"></i>
                        خروج
                    </a>
                </li>
            </ul>
            <div class="ndk-cont">
                <div class="fldfd-spc">

                    <div class="fksd-t-img" style="background-image:url('../../public/default/img/demo/profile-header-img.jpg');">
                        <div class="ddkf-sccp">
                            <div class="fkd-tf">
                                <h2 class="fkmd-iggg">
                                    <i class="fa fa-money"></i>
                                    پروفایل
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="fle-ds-spc">

                        <div class="fkrf-sc-cls">
                            <div class="kffg-img-h">
                                <div class="kd-img-box">
                                    <img src="<?php echo $result['image']; ?>">
                                </div>
                            </div>
                            <div class="kfc-ddgg-cnt">
                                <table class="fkr-clrr-tlb" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                                    <span class="fkrd-i">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                            <span class="kdf-txt fkd-title">
                                                        نام و نام خانوادگی :
                                                    </span>
                                            <span class="kdf-txt fmkd-value">
                                                        <?php echo "$mypanel[name]"." "."$mypanel[lastname]";  ?>
                                                    </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                    <span class="fkrd-i">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                            <span class="kdf-txt fkd-title">
                                                        جنسیت :
                                                    </span>
                                            <span class="kdf-txt fmkd-value">
                                                        <?php
                                                            if ($mypanel['gender']==1){
                                                                echo "مرد";
                                                            }elseif ($mypanel['gender']==2){
                                                                echo "زن";
                                                            }else{
                                                                echo "نا مشخص";
                                                            }
                                                        ?>
                                                    </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                    <span class="fkrd-i">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                            <span class="kdf-txt fkd-title">
                                                        ایمیل (نام کاربری) :
                                                    </span>
                                            <span class="kdf-txt fmkd-value">
                                                        <?php echo $mypanel['email']; ?>
                                                    </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                    <span class="fkrd-i">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                            <span class="kdf-txt fkd-title">
                                                        شماره همراه :
                                                    </span>
                                            <span class="kdf-txt fmkd-value">
                                                        <?php echo $mypanel['phone']; ?>
                                                    </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- START : PROFILE BLOCK -->



<!-- END : PAGE CONTENT -->


