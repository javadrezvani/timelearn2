

        <!-- START : PAGE CONTENT -->


        <!-- START : HEADER IMAGE -->
        <section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
            <span class="fker-edge"></span>
            <div class="sys-container fkre-sc-cont">
                <div class="rlpr-brd-crm"></div>
                <ul class="ldkie-ccl">
                    <li>
                        <a href="#" class="trs-2">صفحه نخست</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">فایل های آموزشی</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">طراحی سایت</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                </ul>
            </div>
        </section>
        <!-- END : HEADER IMAGE -->



        <!-- START : PROFILE BLOCK -->
        <section class="dke-rdkd-rk3">
            <div class="sys-container fdlt-cont">

                <div class="kdsf-spc f-parent">
                    <ul class="ndk-nav">
                        <li>
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not">کسب درآمد!</span>
                            <a href="#">
                                <i class="fa fa-lock dkd-icon"></i>
                                تغییر رمز عبور
                            </a>
                        </li>
                        <li class="rksd-current">
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not blue red">4</span>
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li class="dkkd-logout">
                            <a href="#">
                                <i class="fa fa-power-off dkd-icon"></i>
                                خروج
                            </a>
                        </li>
                    </ul>
                    <div class="ndk-cont">
                        <div class="fldfd-spc">
                            
                            <div class="fksd-t-img" style="background-image:url('../../public/default/img/demo/profile-header-img.jpg');">
                                <div class="ddkf-sccp">
                                    <div class="fkd-tf">
                                        <h2 class="fkmd-iggg">
                                            <i class="fa fa-envelope"></i>
                                            ارسال پیام
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <div class="fle-ds-spc">



                                <!-- START : TABS -->
                                <ul class="kdcc-tab tb-border">
                                    <li>
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-cloud-upload"></i>
                                            صندوق دریافت
                                            <span class="kdf-gtr">12</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-cloud-download"></i>
                                            صندوق ارسال
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-envelope"></i>
                                            ارسال پیام
                                        </a>
                                    </li>
                                </ul>
                                <!-- END : TABS -->



                                <!-- START : FORM -->
                                <form class="frm-qa-send" method="post" action="#qa-send">
                                    <div class="cmo-row">
                                        <input class="frm-el-inpt el-title ieel---title-inpt trs-2" type="text" placeholder="عنوان پیام">
                                    </div>
                                    <div class="cmo-row">
                                        <textarea class="frm-el-inpt ieel---content-inpt trs-2" placeholder="متن پیام"></textarea>
                                    </div>
                                    <div class="cmo-row dld-tls">
                                        <a href="#" class="common-button cmb-icon cmb-cl-red">
                                            <i class="fa fa-reply"></i>
                                            انصراف
                                        </a>
                                        <button class="common-button cmb-icon cmb-cl-green prtn-submitter">
                                            <i class="fa fa-check"></i>
                                            ارسال پیام
                                        </button>
                                    </div>
                                </form>
                                <!-- END : FORM -->



                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- START : PROFILE BLOCK -->



        <!-- END : PAGE CONTENT -->
