<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : PROFILE BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont">

        <div class="kdsf-spc f-parent">
            <ul class="ndk-nav">
                <li class="">
                    <a href="index.php?c=panel&a=profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user-secret dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <span class="dkd-not"></span>
                    <a href="index.php?c=panel&a=change-password&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-lock dkd-icon"></i>
                        تغییر رمز عبور
                    </a>
                </li>
                <li>
                    <a href="index.php?c=panel&a=basket-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-shopping-cart dkd-icon"></i>
                        سبد خرید
                    </a>
                </li>
                <li>
                    <a href="index.php?c=panel&a=basket-pay&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-list-ul dkd-icon"></i>
                        دوره های خریداری شده
                    </a>
                </li>
                <li class="rksd-current">
                    <span class="dkd-not blue red">ویرایش اطلاعات</span>
                    <a href="index.php?c=panel&a=edit-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user dkd-icon"></i>
                        ویرایش اطلاعات
                    </a>
                </li>
                <li class="dkkd-logout">
                    <a href="index.php?c=panel&a=logout">
                        <i class="fa fa-power-off dkd-icon"></i>
                        خروج
                    </a>
                </li>
            </ul>
            <div class="ndk-cont">
                <div class="fldfd-spc">

                    <div class="fksd-t-img" style="background-image:url('../../public/default/img/demo/profile-header-img.jpg');">
                        <div class="ddkf-sccp">
                            <div class="fkd-tf">
                                <h2 class="fkmd-iggg">
                                    <i class="fa fa-money"></i>
                                    تصحیح پروفایل
                                </h2>
                            </div>
                        </div>
                    </div>

                    <form class="fle-ds-spc frm-edit-profile" enctype="multipart/form-data" method="post" action="index.php?c=panel&a=edit-profile&id=<?php echo $_SESSION['user_id']; ?>">

                        <div class="fkrf-sc-cls">
                            <div class="kffg-img-h">
                                <div class="kd-img-box">
                                    <img src="<?php echo $result['image']; ?>">
                                    <span class="kfdf-shell"></span>
                                </div>
                                <div class="dfkd-tls">
                                    <label class="fdglf-slct-label">
                                        <input type="file" class="edit-profile---img-inpt" name="image">
                                        <span class="kf-dgg-btn">
                                                    <i class="fa fa-upload"></i>
                                                    انتخاب عکس جدید
                                                </span>
                                    </label>
                                </div>
                            </div>
                            <div class="kfc-ddgg-cnt">
                                <div class="fvkf-wrapper">

                                    <table class="fkr-clrr-tlb" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                        <span class="fkrd-i">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                <span class="kdf-txt fkd-title">
                                                            نام :
                                                        </span>
                                            </td>
                                            <td>
                                                <input class="frm-el-inpt ksre---name-inpt trs-2" type="text" name="frm[name]" placeholder="نام" value="<?php echo $result['name']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                        <span class="fkrd-i">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                <span class="kdf-txt fkd-title">
                                                            نام خانوادگی :
                                                        </span>
                                            </td>
                                            <td>
                                                <input class="frm-el-inpt ksre---last-name-inpt trs-2" name="frm[lastname]" type="text" placeholder="نام خانوادگی" value="<?php echo $result['lastname']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                        <span class="fkrd-i">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                <span class="kdf-txt fkd-title">
                                                            شماره همراه :
                                                        </span>
                                            </td>
                                            <td>
                                                <input class="frm-el-inpt ksre---mobile-inpt trs-2" type="text" name="frm[phone]" placeholder="شماره همراه" value="<?php echo $result['phone']; ?>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                        <span class="fkrd-i">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                <span class="kdf-txt fkd-title">
                                                            ایمیل (نام کاربری) :
                                                        </span>
                                            </td>
                                            <td>
                                                        <span class="kdf-txt fmkd-value">
                                                            <?php echo $result['email']; ?>
                                                        </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>


                        <div class="mrodl-tls">
                            <a href="index.php?c=panel&a=profile&id=<?php echo $_SESSION['user_id']; ?>" class="common-button cmb-icon cmb-cl-red">
                                <i class="fa fa-mail-forward"></i>
                                انصراف
                            </a>
                            <button class="common-button cmb-icon cmb-cl-green prtn-submitter" type="submit" name="btn">
                                <i class="fa fa-check"></i>
                                ذخیره تغییرات
                            </button>
                        </div>


                    </form>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- START : PROFILE BLOCK -->



<!-- END : PAGE CONTENT -->




