
        <!-- START : PAGE CONTENT -->


        <!-- START : HEADER IMAGE -->
        <section class="kr-ddhrr-ss" style="background-image:url('img/demo/header-img.jpg');">
            <span class="fker-edge"></span>
            <div class="sys-container fkre-sc-cont">
                <div class="rlpr-brd-crm"></div>
                <ul class="ldkie-ccl">
                    <li>
                        <a href="#" class="trs-2">صفحه نخست</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">فایل های آموزشی</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">طراحی سایت</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                </ul>
            </div>
        </section>
        <!-- END : HEADER IMAGE -->



        <!-- START : PROFILE BLOCK -->
        <section class="dke-rdkd-rk3">
            <div class="sys-container fdlt-cont">

                <div class="kdsf-spc f-parent">
                    <ul class="ndk-nav">
                        <li>
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not">کسب درآمد!</span>
                            <a href="#">
                                <i class="fa fa-lock dkd-icon"></i>
                                تغییر رمز عبور
                            </a>
                        </li>
                        <li class="rksd-current">
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not blue red">4</span>
                            <a href="#">
                                <i class="fa fa-user dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li class="dkkd-logout">
                            <a href="#">
                                <i class="fa fa-power-off dkd-icon"></i>
                                خروج
                            </a>
                        </li>
                    </ul>
                    <div class="ndk-cont">
                        <div class="fldfd-spc">
                            
                            <div class="fksd-t-img" style="background-image:url('img/demo/profile-header-img.jpg');">
                                <div class="ddkf-sccp">
                                    <div class="fkd-tf">
                                        <h2 class="fkmd-iggg">
                                            <i class="fa fa-money"></i>
                                            پرسش و پاسخ
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <div class="fle-ds-spc">



                                <!-- START : TABS -->
                                <ul class="kdcc-tab">
                                    <li class="active">
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-cloud-upload"></i>
                                            صندوق دریافت
                                            <span class="kdf-gtr">12</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-cloud-download"></i>
                                            صندوق ارسال
                                        </a>
                                    </li>
                                    <li>
                                        <a class="trs-2" href="#">
                                            <i class="fa fa-envelope"></i>
                                            ارسال پیام
                                        </a>
                                    </li>
                                </ul>
                                <!-- END : TABS -->


                                <!-- START : INBOX TABLE -->
                                <div class="a-editor-block">

                                    <table border="0" cellpadding="0" cellspacing="0" class="common-tbl c-tbl-gray">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    #
                                                    ردیف
                                                </th>
                                                <th scope="col">
                                                    عنوان
                                                </th>
                                                <th scope="col">
                                                    خلاصه
                                                </th>
                                                <th scope="col">
                                                    تاریخ
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="c-tbl-active">
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        آموزش کامل PHP
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="trs-2" href="#">
                                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                                                    </a>
                                                </td>
                                                <td>
                                                    1395-02-05
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <!-- END : INBOX TABLE -->


                                <ul class="cmn-pagination gray">
                                    <a class="cmn-p-ln cmn-p-w-auto trs-2" href="#">قبلی</a>
                                    <a class="cmn-p-ln trs-2" href="#">1</a>
                                    <a class="cmn-p-ln trs-2 active" href="#">2</a>
                                    <a class="cmn-p-ln trs-2" href="#">3</a>
                                    <a class="cmn-p-ln trs-2" href="#">4</a>
                                    <a class="cmn-p-ln cmn-p-w-auto trs-2" href="#">بعدی</a>
                                </ul>



                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- START : PROFILE BLOCK -->



        <!-- END : PAGE CONTENT -->
