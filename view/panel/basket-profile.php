 <!-- START : PAGE CONTENT -->


        <!-- START : HEADER IMAGE -->
        <section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
            <span class="fker-edge"></span>
            <div class="sys-container fkre-sc-cont">
                <div class="rlpr-brd-crm"></div>
                <ul class="ldkie-ccl">
                    <li>
                        <a href="#" class="trs-2">صفحه نخست</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">فایل های آموزشی</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a href="#" class="trs-2">طراحی سایت</a>
                        <i class="fa fa-angle-left"></i>
                    </li>
                </ul>
            </div>
        </section>
        <!-- END : HEADER IMAGE -->



        <!-- START : PROFILE BLOCK -->
        <section class="dke-rdkd-rk3">
            <div class="sys-container fdlt-cont">

                <div class="kdsf-spc f-parent">
                    <ul class="ndk-nav">
                        <li>
                            <a href="index.php?c=panel&a=profile&id=<?php echo $_SESSION['user_id']; ?>">
                                <i class="fa fa-user-secret dkd-icon"></i>
                                اطلاعات پروفایل
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not"></span>
                            <a href="index.php?c=panel&a=change-password&id=<?php echo $_SESSION['user_id']; ?>">
                                <i class="fa fa-lock dkd-icon"></i>
                                تغییر رمز عبور
                            </a>
                        </li>
                        <li class="rksd-current">
                            <a href="index.php?c=panel&a=basket-profile&id=<?php echo $_SESSION['user_id']; ?>">
                                <i class="fa fa-shopping-cart dkd-icon"></i>
                                سبد خرید
                            </a>
                        </li>
                        <li>
                            <a href="index.php?c=panel&a=basket-pay&id=<?php echo $_SESSION['user_id']; ?>">
                                <i class="fa fa-list-ul dkd-icon"></i>
                                دوره های خریداری شده
                            </a>
                        </li>
                        <li>
                            <span class="dkd-not blue red">ویرایش اطلاعات</span>
                            <a href="index.php?c=panel&a=edit-profile&id=<?php echo $_SESSION['user_id']; ?>">
                                <i class="fa fa-user dkd-icon"></i>
                                ویرایش اطلاعات
                            </a>
                        </li>
                        <li class="dkkd-logout">
                            <a href="index.php?c=panel&a=logout">
                                <i class="fa fa-power-off dkd-icon"></i>
                                خروج
                            </a>
                        </li>
                    </ul>
                    <div class="ndk-cont">
                        <div class="fldfd-spc">
                            
                            <div class="fksd-t-img" style="background-image:url('../../public/default/img/demo/profile-header-img.jpg');">
                                <div class="ddkf-sccp">
                                    <div class="fkd-tf">
                                        <h2 class="fkmd-iggg">
                                            <i class="fa fa-money"></i>
                                            سبد خرید
                                        </h2>
                                    </div>
                                </div>
                            </div>

                            <div class="fle-ds-spc">


                                <!-- START : CARD TABLE -->
                                <div class="a-editor-block">
                                    <table border="0" cellpadding="0" cellspacing="0" class="common-tbl">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    ردیف
                                                </th>
                                                <th scope="col">
                                                    <i class="fa fa-tags"></i>
                                                    نام محصول
                                                </th>
                                                <th scope="col">
                                                    <i class="fa fa-money"></i>
                                                    قیمت
                                                </th>
                                                <th scope="col">
                                                    <i class="fa fa-remove"></i>
                                                    حذف از سبد
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            foreach ($result as $key=>$basket):
                                            if (@$basket['status'] == 0) {
                                            $res = $pro->pro_basket_list($basket['pro_id']);
                                        ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo $res['title']; ?></td>
                                                <td><?php echo number_format($res['price']); ?> تومان</td>
                                                <td>
                                                    <a href="index.php?c=basket&a=delete&id=<?php echo $basket['id']; ?>">
                                                        <button class="common-button cmb-cl-red">
                                                            حذف محصول
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php } endforeach; ?>
                                        </tbody>
                                    </table>

                                </div>
                                <!-- END : CARD TABLE -->


                                <div class="fkrd-vvt f-parent">
                                    <div class="flff-btrr rkrd-vv">
                                        <div class="dkre-spc">
                                            <div class="fkldfd-dss a-editor-block">
                                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان :

                                                <div class="tld-drl-tls">
                                                    <input class="frm-el-inpt lrde---presenter-email-inpt trs-2" type="text" placeholder="ایمیل معرف شما">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flff-btrr rkrd-rr">
                                        <div class="dkre-spc">


                                            <!-- START : FACTOR -->
                                            <div class="fkrd-factor">
                                                <div class="kdf-row">
                                                    <span class="ke-drrr-tt dkrd-label">
                                                        جمع کل
                                                    </span>
                                                    <?php
                                                    foreach ($class->basket_list_pro($_SESSION['user_id']) as $product){
                                                        @$total += $product['price'];
                                                    }
                                                    ?>
                                                    <span class="ke-drrr-tt dkrd-value">
                                                         <?php
                                                            echo @number_format($total);
                                                          ?> تومان
                                                    </span>
                                                </div>
                                                <div class="kdf-row">
                                                    <span class="ke-drrr-tt dkrd-label">
                                                        مبلغ پرداختی
                                                    </span>
                                                    <span class="ke-drrr-tt dkrd-value color blue">
                                                         <?php
                                                             echo @number_format($total);
                                                          ?> تومان
                                                    </span>
                                                </div>
                                            </div>
                                            <!-- END : FACTOR -->


                                            <div class="lrxt-tls">
                                                <a href="index.php?c=basket&a=basket-pay&id=<?php echo $_SESSION['user_id']; ?>" class="common-button cmb-icon">
                                                    <i class="fa fa-money"></i>
                                                    پرداخت
                                                </a>
                                            </div>


                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- START : PROFILE BLOCK -->

        <!-- END : PAGE CONTENT -->
