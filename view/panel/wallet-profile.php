
<!-- START : PROFILE BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont">

        <div class="kdsf-spc f-parent">
            <ul class="ndk-nav">
                <li>
                    <a href="#">
                        <i class="fa fa-user dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <span class="dkd-not">کسب درآمد!</span>
                    <a href="#">
                        <i class="fa fa-lock dkd-icon"></i>
                        تغییر رمز عبور
                    </a>
                </li>
                <li class="rksd-current">
                    <a href="#">
                        <i class="fa fa-user dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <span class="dkd-not blue red">4</span>
                    <a href="#">
                        <i class="fa fa-user dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li class="dkkd-logout">
                    <a href="#">
                        <i class="fa fa-power-off dkd-icon"></i>
                        خروج
                    </a>
                </li>
            </ul>
            <div class="ndk-cont">
                <div class="fldfd-spc">

                    <div class="fksd-t-img" style="background-image:url('img/demo/profile-header-img.jpg');">
                        <div class="ddkf-sccp">
                            <div class="fkd-tf">
                                <h2 class="fkmd-iggg">
                                    <i class="fa fa-money"></i>
                                    کیف پول
                                </h2>
                            </div>
                        </div>
                    </div>

                    <form class="fle-ds-spc frm-increase-wallet" method="post" action="#increase-wallet">



                        <!-- START : TABLE -->
                        <div class="fkrd-factor">
                            <div class="kdf-row">
                                        <span class="ke-drrr-tt dkrd-label">
                                            موجودی فعلی
                                        </span>
                                <span class="ke-drrr-tt dkrd-value color1 blue1">
                                            200,000 تومان
                                        </span>
                            </div>
                            <div class="kdf-row">
                                        <span class="ke-drrr-tt dkrd-label">
                                            مبلغ افزایش موجودی
                                        </span>
                                <span class="ke-drrr-tt input-h dkrd-value">
                                            <input class="frm-el-inpt feer---amount-inpt trs-2" type="text" placeholder="مبلغ (تومان)">
                                        </span>
                            </div>
                        </div>
                        <!-- END : TABLE -->


                        <div class="lrxt-tls">
                            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter">
                                <i class="fa fa-sign-in"></i>
                                افزایش موجودی
                            </button>
                        </div>



                    </form>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- START : PROFILE BLOCK -->



<!-- END : PAGE CONTENT -->