<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : MIDDLE BLOCK -->
<section class="fkfg-lgn-pup on-ground trs-2">
    <form class="frm-change-pass krrd-cont trs-2" method="post" action="index.php?c=panel&a=change-password&id=<?php echo $_SESSION['user_id']; ?>">

        <div class="fkf-rvvr">

            <div class="ddet-spc">

                <div class="dlgf-head dh-dark-text dh-blue-color">
                    <h1 class="fgfl-vv">
                        تغییر رمز عبور
                    </h1>
                </div>

                <div class="fdkr-rdsc no-border">
                    <div class="fdss-rw">
                        <input class="frm-el-inpt change-pass---old-inpt trs-2" name="frm[oldpassword]" type="password" placeholder="رمز عبور فعلی">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt change-pass---new-inpt trs-2" type="password" placeholder="رمز عبور جدید">
                        <p class="frm-err-h"></p>
                    </div>
                    <div class="fdss-rw">
                        <input class="frm-el-inpt change-pass---repeat-inpt trs-2" name="frm[newpassword]" type="password" placeholder="تکرار رمز عبور جدید">
                        <p class="frm-err-h"></p>
                        <input type="submit" class="hidden-elem">
                    </div>
                </div>

            </div>
        </div>

        <div class="kft-tls f-parent">
            <button class="common-button cmb-icon cmb-cl-green fkew-lft prtn-submitter" name="btn" type="submit">
                <i class="fa fa-sign-in"></i>
                ذخیره تنظیمات
            </button>
        </div>

    </form>
</section>
<!-- END : MIDDLE BLOCK -->



<!-- END : PAGE CONTENT -->

