<!-- START : PAGE CONTENT -->


<!-- START : HEADER IMAGE -->
<section class="kr-ddhrr-ss" style="background-image:url('../../public/default/img/demo/header-img.jpg');">
    <span class="fker-edge"></span>
    <div class="sys-container fkre-sc-cont">
        <div class="rlpr-brd-crm"></div>
        <ul class="ldkie-ccl">
            <li>
                <a href="#" class="trs-2">صفحه نخست</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">فایل های آموزشی</a>
                <i class="fa fa-angle-left"></i>
            </li>
            <li>
                <a href="#" class="trs-2">طراحی سایت</a>
                <i class="fa fa-angle-left"></i>
            </li>
        </ul>
    </div>
</section>
<!-- END : HEADER IMAGE -->



<!-- START : PROFILE BLOCK -->
<section class="dke-rdkd-rk3">
    <div class="sys-container fdlt-cont">

        <div class="kdsf-spc f-parent">
            <ul class="ndk-nav">
                <li>
                    <a href="index.php?c=panel&a=profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user-secret dkd-icon"></i>
                        اطلاعات پروفایل
                    </a>
                </li>
                <li>
                    <span class="dkd-not"></span>
                    <a href="index.php?c=panel&a=change-password&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-lock dkd-icon"></i>
                        تغییر رمز عبور
                    </a>
                </li>
                <li>
                    <a href="index.php?c=panel&a=basket-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-shopping-cart dkd-icon"></i>
                        سبد خرید
                    </a>
                </li>
                <li class="rksd-current">
                    <a href="index.php?c=panel&a=basket-pay&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-list-ul dkd-icon"></i>
                        دوره های خریداری شده
                    </a>
                </li>
                <li>
                    <span class="dkd-not blue red">ویرایش اطلاعات</span>
                    <a href="index.php?c=panel&a=edit-profile&id=<?php echo $_SESSION['user_id']; ?>">
                        <i class="fa fa-user dkd-icon"></i>
                        ویرایش اطلاعات
                    </a>
                </li>
                <li class="dkkd-logout">
                    <a href="index.php?c=panel&a=logout">
                        <i class="fa fa-power-off dkd-icon"></i>
                        خروج
                    </a>
                </li>
            </ul>
            <div class="ndk-cont">
                <div class="fldfd-spc">

                    <div class="fksd-t-img" style="background-image:url('../../public/default/img/demo/profile-header-img.jpg');">
                        <div class="ddkf-sccp">
                            <div class="fkd-tf">
                                <h2 class="fkmd-iggg">
                                    <i class="fa fa-money"></i>
                                    محصولات خریداری شده
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="fle-ds-spc">


                        <!-- START : FAVORITE TABLE -->
                        <div class="a-editor-block">
                            <table border="0" cellpadding="0" cellspacing="0" class="common-tbl">
                                <thead>
                                <tr>
                                    <th scope="col">
                                        ردیف
                                    </th>
                                    <th scope="col">
                                        <i class="fa fa-tags"></i>
                                        نام محصول
                                    </th>
                                    <th scope="col">
                                        <i class="fa fa-picture-o"></i>
                                        عکس دوره
                                    </th>
                                    <th scope="col">
                                        <i class="fa fa-money"></i>
                                        قیمت
                                    </th>
                                    <th scope="col">
                                        <i class="fa fa-dashboard"></i>
                                        زمان
                                    </th>
                                    <th scope="col">
                                        <i class="fa fa-download"></i>
                                        دانلود آموزش
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($result as $key=>$value): ?>
                                <tr>
                                    <td><?php echo $key+1; ?></td>
                                    <td><a href="index.php?c=pro&a=detail&id=<?php echo $value['id']; ?>&procat=<?php echo $value['cat_id']; ?>"><?php echo $value['title']; ?></a></td>
                                    <td><img src="<?php echo $value['image']; ?>" width="50"></td>
                                    <td><?php echo number_format($value['price']); ?> تومان</td>
                                    <td><?php echo $value['time']; ?></td>
                                    <td>
                                        <a href="index.php?c=pro&a=detail&id=<?php echo $value['id']; ?>&procat=<?php echo $value['cat_id']; ?>">
                                        <button class="common-button cmb-cl-red">
                                                رفتن به آموزش
                                        </button>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- END : FAVORITE TABLE -->



                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- START : PROFILE BLOCK -->



<!-- END : PAGE CONTENT -->



