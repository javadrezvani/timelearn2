<?php
require_once 'admin/model/Mpro.php';
require_once 'admin/model/Muser.php';
require_once 'admin/model/Mbasket.php';
$basket=new basket();
$panel=new user();
$pro=new pro();

switch($action){
    case 'profile':
        $id=$_GET['id'];
        $result=$panel->users_profile($id);
        if (isset($_SESSION['user_id'])){
            if ($_SESSION['user_id']==$id){
                $mypanel=$panel->users_profile($id);
            }else{
                header("location:index.php?c=user&a=login");
            }
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;
    case 'edit-profile':
        $id=$_GET['id'];
        if (isset($_SESSION['user_id'])){
            if ($_SESSION['user_id']==$id){
                $result=$panel->users_profile($id);
                if (isset($_POST['btn'])){
                    $data=$_POST['frm'];
                    $oldpic=$result['image'];
                    $panel->edit_my_profile($data,$id,'image',$oldpic);
                    header("location:index.php?c=panel&a=profile&id=$_SESSION[user_id]");
                }
            }
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'basket-profile':
        $id=$_GET['id'];
        if (isset($_SESSION['user_id'])){
            if ($_SESSION['user_id']==$id) {
                $user_id=$_SESSION['user_id'];
                $result=$basket->basket_list($user_id);
            }else{
                header("location:index.php?c=panel&a=profile&id=$_SESSION[user_id]");
            }
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'change-password': 
        $id=$_GET['id'];
        if (isset($_SESSION['user_id'])){
            if ($_SESSION['user_id']==$id){
                $pass=$panel->users_profile($id);
                if (isset($_POST['btn'])){
                    $data=$_POST['frm'];
                    if ($pass['password']==sha1($data['oldpassword'])){
                        $panel->edit_password($data,$id);
                        header("location:index.php?c=panel&a=profile&id=$_SESSION[user_id]");
                    }else{
                        header("location:index.php?c=panel&a=profile&id=$_SESSION[user_id]&password=error");
                    }
                }
            }else{
                header("location:index.php?c=panel&a=profile&id=$_SESSION[user_id]");
            }
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'basket-pay':
        if (isset($_SESSION['user_id'])){
            $id=$_GET['id'];
            if ($id==$_SESSION['user_id']){
                $result=$basket->basket_pay_list_pro($id);
            }
        }
    break;

    case 'logout':
        session_destroy();
        header("location:index.php");
    break;
}

require_once "view/".$controller."/".$action.".php";