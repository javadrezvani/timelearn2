<?php
require_once 'admin/model/Mbasket.php';
$basket=new basket();

switch($action){
    case 'basket-profile':
    $id=$_GET['id'];
    if (isset($_SESSION['user_id'])){
            if ($_SESSION['user_id']==$id){
                $user_id=$_SESSION['user_id'];
                $result=$basket->basket_list($user_id);
            }else{
                header("location:index.php?c=panel&a=basket-profile&id=$_SESSION[user_id]");
            }
    }else{
        header("location:index.php?c=user&a=login");
    }
    break;
    case 'add':
        if (isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $pro_id=$_GET['pro_id'];
            $basket->basket_add($user_id,$pro_id);
            header("location:index.php?c=panel&a=basket-profile&id=$_SESSION[user_id]");
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'omit':
        $id=$_GET['id'];
        $user_id=$_SESSION['user_id'];
        $basket->basket_omit($id,$user_id);
        header("location:index.php?c=panel&a=basket-profile&id=$_SESSION[user_id]");
    break;

    case 'ajax':
        $pro_id=$_GET['pro_id'];
        $quantity=$_GET['quantity'];
        $basket->basket_ajax($pro_id,$quantity);
    break;

    case 'delete':
        $id=$_GET['id'];
        $basket->basket_delete($id);
        header("location:index.php?c=panel&a=basket-profile&id=$_SESSION[user_id]");
    break;

    case 'check':
        if (isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $result=$basket->basket_list($user_id);
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'basket-pay':
        if (isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $result=$basket->basket_pay($user_id);
            header("location:index.php?c=panel&a=basket-pay&id=$user_id");
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

    case 'nahaei':
        if (isset($_SESSION['user_id'])){
            $user_id=$_SESSION['user_id'];
            $result=$basket->basket_nahaei($user_id);
            header("location:index.php?c=index&a=index");
        }else{
            header("location:index.php?c=user&a=login");
        }
    break;

}

require_once "view/".$controller."/".$action.".php";