<?php
ob_start();
session_start();
    include_once 'public/include/config.php';
    include_once 'view/layout/header.php';

    function uploader($file,$dir,$folder,$name,$exts){
        @$file=$_FILES[$file];
        if(!file_exists($folder)){
            @mkdir($dir.$folder);
        }
        $filename=$file['name'];
        $array=explode(".",$filename);
        $ext=end($array);
        $newname=$name."-".rand().".".$ext;
        $from=$file['tmp_name'];
        $to=$dir.$folder."/".$newname;
        foreach ($exts as $ext){
            if($file['type']==$ext){
                move_uploaded_file($from,$to);
                return $to;
            }
        }
    }

    $controller=@$_GET['c']?$_GET['c']:'index';
    $action=@$_GET['a']?$_GET['a']:'index';
    if(file_exists('Controller/C'.$controller.'.php')){
        require_once 'Controller/C'.$controller.'.php';
    }
    
    include_once 'view/layout/footer.php';
?>