/**
 **
 **  VALIDATOR
 **
 **/
var PATTERNS = {
    NAME : /^([a-zA-Zآ-ی]|\s)*$/,
    EMAIL : /([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
    MOBILE : /^\d{11}$/,
    DIGIT : /^\d+$/,
    POSTAL_CODE : /^\d{10}$/
}


var formValidator = function(form, autoAttachSubmit){
    this.form = form;
    this.fields = [];

    if(autoAttachSubmit){
        this.attachFormSubmitEvent();
    }
}
formValidator.prototype.addField = function(elem, settings) {
    var newFieldIndex = this.fields.length;
    this.fields[newFieldIndex] = new validatorField(elem, settings);
    return newFieldIndex;
};
formValidator.prototype.validate = function() {

    var results = [];

    for(index in this.fields){
        var fieldRes = this.fields[index].validate();
        results.push(fieldRes);
    }

    return results.indexOf(false) >= 0 ? false : true;

}
formValidator.prototype.attachFormSubmitEvent = function() {
    var dupThis = this;
    if(this.form){
        this.form.submit(function(){
            return dupThis.validate();
        });
    }
}








/**
 **
 **  VALIDATOR FIELD
 **
 **/
var REQUIRE_TYPES = {
    REQUIRE : "require",
    NOTICE : "notice",
    INEFFECTIVE : "ineffective"
}
var VALIDATE_RESULT = {
    NOTREQUIRED : 0,
    MATCHED : 1,
    EMPTY : 2,
    NOTMATCHED : 3,
    OVER_MAX_LENGTH : 4,
    UNDER_MIN_LENGTH : 5
}



var validatorField = function(elem, settings){
    this.elem = elem;
    this.settings = settings;
}
validatorField.prototype.getValue = function(){
    return this.elem.val();
}
validatorField.prototype.validateResult = function(){

    if(this.settings.requireType == "ineffective"){
        return VALIDATE_RESULT.NOTREQUIRED;
    }

    var value = this.getValue();



    // empty checking
    if(this.settings.forceFill && !value){
        return VALIDATE_RESULT.EMPTY;
    }




    // length matching
    if(this.settings.minLength && value.length < this.settings.minLength){
        return VALIDATE_RESULT.UNDER_MIN_LENGTH;
    }
    if(this.settings.maxLength && value.length > this.settings.maxLength){
        return VALIDATE_RESULT.OVER_MAX_LENGTH;
    }



    // pattern matching
    var pattRes = this.settings.pattern ? this.settings.pattern.test(value) : true;



    // if(this.settings.forceFill){
    //     if(pattRes && value){
    //         return VALIDATE_RESULT.MATCHED;
    //     }
    //     else if(!value){
    //         return VALIDATE_RESULT.EMPTY;
    //     }
    //     else if(!pattRes){
    //         return VALIDATE_RESULT.NOTMATCHED;
    //     }
    // }
    // else{
    //     return pattRes ? VALIDATE_RESULT.MATCHED : VALIDATE_RESULT.NOTMATCHED;
    // }


    return pattRes ? VALIDATE_RESULT.MATCHED : VALIDATE_RESULT.NOTMATCHED;

}
validatorField.prototype.validate = function(){

    var resultCode = this.validateResult();


    switch(resultCode){
        case 0 :
            return true;
            break;
        case 1 :
            this.onSuccess();
            return true;
            break;
        case 2 :
            this.onEmpty();
            return false;
            break;
        case 3 :
            this.onError();
            return false;
            break;
        case 4:
            this.onOverMaxLength();
            return false;
            break;
        case 5 :
            this.onUnderMinLength();
            return false;
            break;
        default :
            return false;
    }


}
validatorField.prototype.onSuccess = function(){
}
validatorField.prototype.onError = function(){
}
validatorField.prototype.onEmpty = function(){
}
validatorField.prototype.onOverMaxLength = function(){
}
validatorField.prototype.onUnderMinLength = function(){
}