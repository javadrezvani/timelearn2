$(document).ready(function(){














    /**
     **  START : LOGIN POPUP VALIDATE
     **/
    var loginPopupValidator = new formValidator($(".fkf-fv-cnt"), true);




    // add name field
    var loginPopupUsernameFieldIndex = loginPopupValidator.addField(
        $(".kft---username-inpt"),
        {
            pattern : PATTERNS.NAME,
            requireType : REQUIRE_TYPES.REQUIRE,
            forceFill : true
        }
    );
    loginPopupValidator.fields[loginPopupUsernameFieldIndex].onSuccess = function(){
        this.elem.removeClass("frm-el-err");
    };
    loginPopupValidator.fields[loginPopupUsernameFieldIndex].onError = function(){
        this.elem.addClass("frm-el-err");
    };
    loginPopupValidator.fields[loginPopupUsernameFieldIndex].onEmpty = loginPopupValidator.fields[loginPopupUsernameFieldIndex].onError;





    // add pass field
    var loginPopupPassFieldIndex = loginPopupValidator.addField(
        $(".kft---pass-inpt"),
        {
            pattern : "",
            requireType : REQUIRE_TYPES.REQUIRE,
            forceFill : true,
            minLength : 4
        }
    );
    loginPopupValidator.fields[loginPopupPassFieldIndex].onSuccess = function(){
        this.elem.removeClass("frm-el-err");
    };
    loginPopupValidator.fields[loginPopupPassFieldIndex].onError = function(){
        this.elem.addClass("frm-el-err");
    };
    loginPopupValidator.fields[loginPopupPassFieldIndex].onOverMaxLength = loginPopupValidator.fields[loginPopupPassFieldIndex].onError;
    loginPopupValidator.fields[loginPopupPassFieldIndex].onUnderMinLength = loginPopupValidator.fields[loginPopupPassFieldIndex].onError;
    loginPopupValidator.fields[loginPopupPassFieldIndex].onEmpty = loginPopupValidator.fields[loginPopupPassFieldIndex].onError;
    /**
     **  END : LOGIN POPUP VALIDATE
     **/





















    /**
     **  START : LOGIN PAGE VALIDATE
     **/
    if($(".frm-on-ground-user").length){

        var loginPageValidator = new formValidator($(".frm-on-ground-user"), true);


        // add name field
        var loginPageUsernameFieldIndex = loginPageValidator.addField(
            $(".user-page---username-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        loginPageValidator.fields[loginPageUsernameFieldIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        loginPageValidator.fields[loginPageUsernameFieldIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        loginPageValidator.fields[loginPageUsernameFieldIndex].onEmpty = loginPageValidator.fields[loginPageUsernameFieldIndex].onError;





        // add pass field
        var loginPagePassFieldIndex = loginPageValidator.addField(
            $(".user-page---pass-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 4
            }
        );
        loginPageValidator.fields[loginPagePassFieldIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        loginPageValidator.fields[loginPagePassFieldIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        loginPageValidator.fields[loginPagePassFieldIndex].onOverMaxLength = loginPageValidator.fields[loginPagePassFieldIndex].onError;
        loginPageValidator.fields[loginPagePassFieldIndex].onUnderMinLength = loginPageValidator.fields[loginPagePassFieldIndex].onError;
        loginPageValidator.fields[loginPagePassFieldIndex].onEmpty = loginPageValidator.fields[loginPagePassFieldIndex].onError;
    }
    /**
     **  END : LOGIN PAGE VALIDATE
     **/





















    /**
     **  START : CHANGE PASSWORD PAGE
     **/
    if($(".frm-change-pass").length){

        var changePasswordPageValidator = new formValidator($(".frm-change-pass"), true);




        // add old field
        var changePassOldFieldIndex = changePasswordPageValidator.addField(
            $(".change-pass---old-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                // minLength : 4
            }
        );
        changePasswordPageValidator.fields[changePassOldFieldIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassOldFieldIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassOldFieldIndex].onEmpty = changePasswordPageValidator.fields[changePassOldFieldIndex].onError;
        changePasswordPageValidator.fields[changePassOldFieldIndex].onOverMaxLength = changePasswordPageValidator.fields[changePassOldFieldIndex].onError;
        changePasswordPageValidator.fields[changePassOldFieldIndex].onUnderMinLength = changePasswordPageValidator.fields[changePassOldFieldIndex].onError;








        // add new field
        var changePassNewFieldIndex = changePasswordPageValidator.addField(
            $(".change-pass---new-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 4
            }
        );
        changePasswordPageValidator.fields[changePassNewFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassNewFieldIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassNewFieldIndex].onEmpty = changePasswordPageValidator.fields[changePassNewFieldIndex].onError;
        changePasswordPageValidator.fields[changePassNewFieldIndex].onOverMaxLength = changePasswordPageValidator.fields[changePassNewFieldIndex].onError;
        changePasswordPageValidator.fields[changePassNewFieldIndex].onUnderMinLength = function(){
            this.elem.next(".frm-err-h").html("حداقل تعداد رمز عبور 4 کاراکتر می باشد");
            this.elem.addClass("frm-el-err");
        };








        // add new field
        var changePassRepeatFieldIndex = changePasswordPageValidator.addField(
            $(".change-pass---repeat-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 4
            }
        );
        changePasswordPageValidator.fields[changePassRepeatFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassRepeatFieldIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        changePasswordPageValidator.fields[changePassRepeatFieldIndex].onEmpty = changePasswordPageValidator.fields[changePassRepeatFieldIndex].onError;
        changePasswordPageValidator.fields[changePassRepeatFieldIndex].onOverMaxLength = changePasswordPageValidator.fields[changePassRepeatFieldIndex].onError;
        changePasswordPageValidator.fields[changePassRepeatFieldIndex].onUnderMinLength = function(){
            this.elem.next(".frm-err-h").html("حداقل تعداد رمز عبور 4 کاراکتر می باشد");
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : CHANGE PASSWORD PAGE
     **/





















    /**
     **  START : RETRIVE PASSWORD PAGE
     **/
    if($(".frm-retrive-pass").length){

        var retrivePasswordPageValidator = new formValidator($(".frm-retrive-pass"), true);



        // add new field
        var retrivePassEmailFieldIndex = retrivePasswordPageValidator.addField(
            $(".retrive-pass---email-inpt"),
            {
                pattern : PATTERNS.EMAIL,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        retrivePasswordPageValidator.fields[retrivePassEmailFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        retrivePasswordPageValidator.fields[retrivePassEmailFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("ایمیل خود را به صورت صحیح وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        retrivePasswordPageValidator.fields[retrivePassEmailFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("ایمیل خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : RETRIVE PASSWORD PAGE
     **/

























    /**
     **  START : NEWSLETTER POPUP VALIDATE
     **/
    if($(".drlr-dgglr-frm").length){


        var newsletterPopupValidator = new formValidator($(".drlr-dgglr-frm"), true);


        // add email field
        var newsletterFormEmailInputIndex = newsletterPopupValidator.addField(
            $(".rrl---email-inpt"),
            {
                pattern : PATTERNS.EMAIL,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        newsletterPopupValidator.fields[newsletterFormEmailInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        newsletterPopupValidator.fields[newsletterFormEmailInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        newsletterPopupValidator.fields[newsletterFormEmailInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : NEWSLETTER POPUP VALIDATE
     **/

























    /**
     **  START : SHOPPING CARD EMAIL VALIDATE
     **/
    if($(".frm-shopping-card").length){


        var shoppingCardValidator = new formValidator($(".frm-shopping-card"), true);


        // add email field
        var presenterEmailInputIndex = shoppingCardValidator.addField(
            $(".lrde---presenter-email-inpt"),
            {
                pattern : PATTERNS.EMAIL,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : false
            }
        );
        shoppingCardValidator.fields[presenterEmailInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        shoppingCardValidator.fields[presenterEmailInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        shoppingCardValidator.fields[presenterEmailInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : SHOPPING CARD EMAIL VALIDATE
     **/





















    /**
     **  START : CASH REQUEST VALIDATE
     **/
    if($(".frm-cash-request").length){


        var cashRequestValidator = new formValidator($(".frm-cash-request"), true);




        // add owner field
        var cashReqOwnerInputIndex = cashRequestValidator.addField(
            $(".mker---card-owner-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        cashRequestValidator.fields[cashReqOwnerInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqOwnerInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqOwnerInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add bank field
        var cashReqBankInputIndex = cashRequestValidator.addField(
            $(".mker---card-bank-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        cashRequestValidator.fields[cashReqBankInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqBankInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqBankInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add num field
        var cashReqNumInputIndex = cashRequestValidator.addField(
            $(".mker---card-num-inpt"),
            {
                pattern : PATTERNS.DIGIT,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 16,
                maxLength : 16
            }
        );
        cashRequestValidator.fields[cashReqNumInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqNumInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        cashRequestValidator.fields[cashReqNumInputIndex].onEmpty = cashRequestValidator.fields[cashReqNumInputIndex].onError;
        cashRequestValidator.fields[cashReqNumInputIndex].onOverMaxLength = cashRequestValidator.fields[cashReqNumInputIndex].onError;
        cashRequestValidator.fields[cashReqNumInputIndex].onUnderMinLength = cashRequestValidator.fields[cashReqNumInputIndex].onError;


    }
    /**
     **  END : CASH REQUEST VALIDATE
     **/





















    /**
     **  START : SIGNUP PAGE
     **/
    if($(".frm-signup").length){

        var signupPageValidator = new formValidator($(".frm-signup"), true);






        // add name field
        var signupNameFieldIndex = signupPageValidator.addField(
            $(".signup---name-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        signupPageValidator.fields[signupNameFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupNameFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("نام خود را به صورت صحیح وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupNameFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("نام خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };






        // add last name field
        var signupLastNameFieldIndex = signupPageValidator.addField(
            $(".signup---last-name-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        signupPageValidator.fields[signupLastNameFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupLastNameFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("نام خانوادگی خود را به صورت صحیح وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupLastNameFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("نام خانوادگی خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };






        // add email field
        var signupEmailFieldIndex = signupPageValidator.addField(
            $(".signup---email-inpt"),
            {
                pattern : PATTERNS.EMAIL,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        signupPageValidator.fields[signupEmailFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupEmailFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("ایمیل خود را به صورت صحیح وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupEmailFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("ایمیل خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };






        // add mobile field
        var signupMobileFieldIndex = signupPageValidator.addField(
            $(".signup---mobile-inpt"),
            {
                pattern : PATTERNS.MOBILE,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        signupPageValidator.fields[signupMobileFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupMobileFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("شماره همراه خود را به صورت صحیح وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupMobileFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("شماره همراه خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };






        // add gender field
        var signupGenderFieldIndex = signupPageValidator.addField(
            $(".signup---gender-inpt"),
            {
                // pattern : /^(male|female)$/,
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        signupPageValidator.fields[signupGenderFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupGenderFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("خطایی رخ داده است");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupGenderFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("جنسیت خود را انتخاب نمایید");
            this.elem.addClass("frm-el-err");
        };






        // add pass field
        var signupPassFieldIndex = signupPageValidator.addField(
            $(".signup---pass-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 4
            }
        );
        signupPageValidator.fields[signupPassFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("خطایی رخ داده است");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("رمز عبور خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassFieldIndex].onUnderMinLength = function(){
            this.elem.next(".frm-err-h").html("حداقل تعداد رمز عبور 4 کاراکتر می باشد");
            this.elem.addClass("frm-el-err");
        };






        // add pass field
        var signupPassRepeatFieldIndex = signupPageValidator.addField(
            $(".signup---pass-repeat-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true,
                minLength : 4
            }
        );
        signupPageValidator.fields[signupPassRepeatFieldIndex].onSuccess = function(){
            this.elem.next(".frm-err-h").html("");
            this.elem.removeClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassRepeatFieldIndex].onError = function(){
            this.elem.next(".frm-err-h").html("خطایی رخ داده است");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassRepeatFieldIndex].onEmpty = function(){
            this.elem.next(".frm-err-h").html("تکرار رمز عبور خود را وارد نمایید");
            this.elem.addClass("frm-el-err");
        };
        signupPageValidator.fields[signupPassRepeatFieldIndex].onUnderMinLength = function(){
            this.elem.next(".frm-err-h").html("حداقل تعداد رمز عبور 4 کاراکتر می باشد");
            this.elem.addClass("frm-el-err");
        };





    }
    /**
     **  END : SIGNUP PAGE
     **/

























    /**
     **  START : SHOPPING CARD EMAIL VALIDATE
     **/
    if($(".frm-increase-wallet").length){


        var increaseWalletValidator = new formValidator($(".frm-increase-wallet"), true);


        // add amount field
        var increaseWalletAmountInputIndex = increaseWalletValidator.addField(
            $(".feer---amount-inpt"),
            {
                pattern : PATTERNS.DIGIT,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        increaseWalletValidator.fields[increaseWalletAmountInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        increaseWalletValidator.fields[increaseWalletAmountInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        increaseWalletValidator.fields[increaseWalletAmountInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : SHOPPING CARD EMAIL VALIDATE
     **/

























    /**
     **  START : EDIT PROFILE
     **/
    if($(".frm-edit-profile").length){


        var editProfileValidator = new formValidator($(".frm-edit-profile"), true);




        // add name field
        var editProfileNameInputIndex = editProfileValidator.addField(
            $(".ksre---name-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        editProfileValidator.fields[editProfileNameInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileNameInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileNameInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };





        // add last name field
        var editProfileLastNameInputIndex = editProfileValidator.addField(
            $(".ksre---last-name-inpt"),
            {
                pattern : PATTERNS.NAME,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        editProfileValidator.fields[editProfileLastNameInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileLastNameInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileLastNameInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };





        // add last name field
        var editProfileMobileInputIndex = editProfileValidator.addField(
            $(".ksre---mobile-inpt"),
            {
                pattern : PATTERNS.MOBILE,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        editProfileValidator.fields[editProfileMobileInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileMobileInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        editProfileValidator.fields[editProfileMobileInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };


    }
    /**
     **  END : EDIT PROFILE
     **/

























    /**
     **  START : QA SEND
     **/
    if($(".frm-qa-send").length){


        var qaSendValidator = new formValidator($(".frm-qa-send"), true);




        // add title field
        var qaSendTitleInputIndex = qaSendValidator.addField(
            $(".ieel---title-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        qaSendValidator.fields[qaSendTitleInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        qaSendValidator.fields[qaSendTitleInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        qaSendValidator.fields[qaSendTitleInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add content field
        var qaSendContentInputIndex = qaSendValidator.addField(
            $(".ieel---content-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        qaSendValidator.fields[qaSendContentInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        qaSendValidator.fields[qaSendContentInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        qaSendValidator.fields[qaSendContentInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




    }
    /**
     **  END : QA SEND
     **/

























    /**
     **  START : SEND COMMENT
     **/
    if($(".cmtn-frm").length){


        var sendCommentValidator = new formValidator($(".cmtn-frm"), true);




        // add name field
        var sendCommentNameInputIndex = sendCommentValidator.addField(
            $(".erow---name-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        sendCommentValidator.fields[sendCommentNameInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentNameInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentNameInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add email field
        var sendCommentEmailInputIndex = sendCommentValidator.addField(
            $(".erow---email-inpt"),
            {
                pattern : PATTERNS.EMAIL,
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        sendCommentValidator.fields[sendCommentEmailInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentEmailInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentEmailInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add content field
        var sendCommentContentInputIndex = sendCommentValidator.addField(
            $(".erow---comment-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        sendCommentValidator.fields[sendCommentContentInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentContentInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentContentInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };




        // add captcha field
        var sendCommentCaptchaInputIndex = sendCommentValidator.addField(
            $(".erow---captcha-inpt"),
            {
                pattern : "",
                requireType : REQUIRE_TYPES.REQUIRE,
                forceFill : true
            }
        );
        sendCommentValidator.fields[sendCommentCaptchaInputIndex].onSuccess = function(){
            this.elem.removeClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentCaptchaInputIndex].onError = function(){
            this.elem.addClass("frm-el-err");
        };
        sendCommentValidator.fields[sendCommentCaptchaInputIndex].onEmpty = function(){
            this.elem.addClass("frm-el-err");
        };





    }
    /**
     **  END : SEND COMMENT
     **/
















});