$(document).ready(function(){





    /**
     **
     **  navigation
     **
     **/
    $(".rktt-b-nav li").each(function(){
        if($(this).find(".ttkd-subm").length){
            $(this).addClass("parent");
        }
    });
    $(".tktmk-li").each(function(){
        if($(this).find(".gt-spc").length){
            $(this).addClass("rtt-norm-nav");
        }
    });








    /**
     **
     **  MOBILE NAV
     **
     **/
    var navElems = [
        ".mini-flexnav",
        ".org-flexnav"
    ];

    for(navkey in navElems){
        var loopNav = $(navElems[navkey]);
        if(loopNav.length){
            loopNav.flexNav({
                'animationSpeed':     250,            // default for drop down animation speed
                'transitionOpacity':  true,           // default for opacity animation
                'buttonSelector':     navElems[navkey] + '-menu-button', // default menu button class name
                'hoverIntent':        false,          // Change to true for use with hoverIntent plugin
                'hoverIntentTimeout': 150,            // hoverIntent default timeout
                'calcItemWidths':     false,          // dynamically calcs top level nav item widths
                'hover':              true            // would you like hover support?      
            });
        }
    }








    /**
     **
     **  search box
     **
     **/
    $(".gflr-rv").focus(function(){
        $(".mfgr-se-bl").addClass("fkr-show-box");
    });
    $(document).click(function(e){
        if(
            !$(".mfgr-se-bl").is($(e.target)) &&
            !$(".mfgr-se-bl").find($(e.target)).length
        ){
            $(".mfgr-se-bl").removeClass("fkr-show-box");
        }
    });








    /**
     **
     **  particles
     **
     **/
    if(typeof particlesActive !== "undefined" && particlesActive && $("#particles-js").length){
        particlesJS('particles-js',
            {
                "particles": {
                    "number": {
                        "value": 60,
                        "density": {
                            "enable": true,
                            "value_area": 800
                        }
                    },
                    "color": {
                        "value": "#ffffff"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#ffffff"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.8,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.4,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 2,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 0.5,
                        "direction": "none",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "grab"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 200,
                            "line_linked": {
                                "opacity": 0.4
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            }

        );
    }









    /**
     **
     **  video player, index & editor block
     **
     **/
    $(".a-editor-block video").each(function(){
        var width=$(this).width();
        var height=$(this).height();
        $(this).wrap("<div class='flowplayer' data-swf='flowplayer.swf' style='width:"+width+"px;'></div>");
    });
    if($(".flowplayer").length){
        $(".flowplayer").flowplayer();
    }









    /**
     **
     **  star rate
     **
     **/
    resetStarRates();
    resetStarRateEvent();

    function resetStarRates(){
        $(".common-star-rate").each(function(){
            resetSingleStarRate($(this));
        });
    }

    function resetStarRateEvent(){
        $(".common-star-rate.csr-active").each(function(){

            $(this).find("i").unbind("click mouseenter mouseleave");

            $(this).find("i").hover(function(){
                var iconFull = $(this).parent().attr("data-icon-full");
                var iconNull = $(this).parent().attr("data-icon-null");
                $(this).removeClass(iconNull).addClass(iconFull);
                $(this).prevAll("i").removeClass(iconNull).addClass(iconFull);
                $(this).nextAll("i").removeClass(iconFull).addClass(iconNull);
            }, function(){
                resetSingleStarRate($(this).parent());
                resetStarRateEvent();
            });

        });
    }

    function resetSingleStarRate(starRate){
        starRate.html("");
        var value = parseInt(starRate.attr("data-value"));
        var max = parseInt(starRate.attr("data-max"));

        var iconFull = starRate.attr("data-icon-full");
        var iconNull = starRate.attr("data-icon-null");

        for(i=0 ; i < max ; i++){
            if(i < value){
                loopIcon = iconFull;
            }
            else{
                loopIcon = iconNull;
            }
            starRate.append("<i class='" + loopIcon + "'></i>");
        }
    }








    /**
     **
     **  comment slider
     **
     **/
    if($(".cmnt-slider").length){
        var cmtnSlider = $(".cmnt-slider").dragItemSlider({
            itemDirection:'rtl',
            switchDuration:500,
            onDragSwitchDuration:100,
            appendBulletSwitchs:false,
            appendNextPrevSwitch:false,
            switchDelay:1000,
            autoPlay:false,
            loopType:'normal',
            itemMovement:'next',
            draggable:true,
            dragSwitchMaxDistance:150,
            dragSwitchMinDistance:20,
            dragEndFactor:0.6
        });
        $(".fkdd-ccl.kfg-next").click(function(){
            cmtnSlider.switchNext();
        });
        $(".fkdd-ccl.kfg-prev").click(function(){
            cmtnSlider.switchBack();
        });
    }








    /**
     **
     **  COMMON POPUP
     **
     **/
    $(".common-popup-o-btn").click(function(){
        var targetPopup = $(this).attr("data-target-popup");
        showPopup(targetPopup);
    });
    $(".common-popup-cls-btn").click(function(){
        var targetPopup = $(this).attr("data-target-popup");
        closePopup(targetPopup);
    });
    $(".common-popup").click(function(e){
        if(
            !$(this).find(".common-popup-cont").is($(e.target))
            && !$(this).find(".common-popup-cont").find($(e.target)).length
        ){
            closePopup($(this));
        }
    });
    function showPopup(targetPopup){
        $(targetPopup).addClass("cpp-show");
    }
    function closePopup(targetPopup){
        $(targetPopup).removeClass("cpp-show");
    }








    /**
     **
     **  PARENT SUBMITTER
     **
     **/
    $(".prtn-submitter").click(function(){
        $(this).parents("form:first").submit();
    });








    /**
     **
     **  EDIT PROFILE IMAGE
     **
     **/
    $(".edit-profile---img-inpt").change(function(){
        var value = $(this).val();
        var parent = $(this).parent(".fdglf-slct-label");
        if(value){
            parent.addClass("file-choosed");
        }
        else{
            parent.removeClass("file-choosed");
        }
    });












    /**
     **
     **  COURSE DETAIL
     **
     **/

    // odd comments
    $(".rorts-cmnts .cmtn-itm:nth-child(2n)").addClass("cmtn-odd");


    // similar slider
    if($(".vdls-slid").length){
        var similarSlider = $(".vdls-slid").dragItemSlider({
            itemDirection:'rtl',
            switchDuration:500,
            onDragSwitchDuration:100,
            appendBulletSwitchs:false,
            appendNextPrevSwitch:false,
            switchDelay:1000,
            autoPlay:false,
            loopType:'normal',
            itemMovement:'next',
            draggable:true,
            dragSwitchMaxDistance:150,
            dragSwitchMinDistance:20,
            dragEndFactor:0.6
        });
        $(".sim-ctrl .ctrl.ct-next").click(function(){
            similarSlider.switchNext();
        });
        $(".sim-ctrl .ctrl.ct-prev").click(function(){
            similarSlider.switchBack();
        });
    }












    /**
     **
     **  OOOOOO
     **
     **/




});