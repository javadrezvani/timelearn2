/*
 * Drag Item Slider - jQuery plugin for slide & dragging horizontal items
 * Copyright (c) 2013-2014 FarsRadio.com
 */
(function($){
	
	$.fn.dragItemSlider=function(options){
		
		var defaults={
			
			itemDirection:'ltr',
			appendBulletSwitchs:true,
			switchBulletCentWidth:false,
			appendNextPrevSwitch:true,
			switchDuration:400,
			onDragSwitchDuration:400,
			switchDelay:5000,
			autoPlay:true,
			cssTransitionClass:'css-trs',
			loopType:'normal',
			itemMovement:'next',
			stopOnHover:true,
			firstItem:1,
			draggable:true,
			dragSwitchMaxDistance:500,
			dragSwitchMinDistance:0,
			dragValidTimer:5000,
			dragEndFactor:0.3,
			lazyLoadSRCElement:'',
			lazyLoadOriginalImg:'',
			lazyLoadOnLoadImgClass:'loaded',
			viewedClass:'item-viewed',
			easing:'easeOutQuad'
			
		};
		
		var settings=$.extend({},defaults,options);
		
		//new plugin code, the best!
		var plugin;
		//new plugin code, the best!
		
		this.each(function(){

            var hasTouch='ontouchstart' in window;
			
			var dragSlider=$(this);
			$(dragSlider).addClass('drag-slider');
			$(dragSlider).find('.ds-dragger').addClass(settings.cssTransitionClass);
			
			var global={
				itemNumber:0,
				itemWidth:0,
				itemCentWidth:0,
				inViewItemNumber:0,
				relatorWidth:0,
				relatorPos:{
					x:0,
					y:0
				},
				draggerPos:{
					x:0,
					y:0
				},
				draggerMarginDirection:'margin-right',
				last_in_view_index:settings.firstItem-1
			}
			
			if(settings.itemDirection=='ltr'){
				
				$(dragSlider).addClass('ds-ltr');
				$(dragSlider).find('.ds-item').addClass('ds-f-left');
				
				global.draggerMarginDirection='margin-left';
				
			}
			
			if(settings.switchDelay<=settings.switchDuration)
				settings.switchDelay=settings.switchDuration+100;
			
			if(settings.dragSwitchMaxDistance<100)
				settings.dragSwitchMaxDistance=100
			
			getItemNumberAppendSwitchs();
			
			function getItemNumberAppendSwitchs(){
				global.itemNumber=$(dragSlider).find('.ds-item').length;
				appendSwitch();
			}
			
			function appendSwitch(){
				
				if(settings.appendBulletSwitchs)
					$(dragSlider).append("<div class='ds-switch-container'></div>");
				
				$(dragSlider).find('.ds-item').each(function(index, element) {
					
					if(settings.appendBulletSwitchs){
						
						var switchWidth=(settings.switchBulletCentWidth)?(100/global.itemNumber):'';
						var switchHtml="<div class='ds-switch' style='width:"+switchWidth+"%'></div>";
						
						if(switchWidth=='')
							switchHtml="<div class='ds-switch'></div>";
						
						$(dragSlider).find('.ds-switch-container').append(switchHtml);
						
					}
					
                });
				
				if(settings.appendNextPrevSwitch){
					
					var rightSwitchRole='prev';
					var leftSwitchRole='next';
					
					if(settings.itemDirection=='ltr'){
						rightSwitchRole='next';
						leftSwitchRole='prev';
					}
					
					var nextPrevSwitchHtml="<div class='ds-controller right "+rightSwitchRole+"'></div>";
					nextPrevSwitchHtml+="<div class='ds-controller left "+leftSwitchRole+"'></div>";
					
					$(dragSlider).find('.ds-viewer').append(nextPrevSwitchHtml);
					
				}
				
			}
			
			$(window).load(function(e) {
				$(window).resize();
			});
			
			getRelatorWidth();
			$(window).resize(function(e) {
				getRelatorWidth();
            });
			
			function getRelatorWidth(){
				global.relatorWidth=$(dragSlider).find('.ds-relator').outerWidth(false);
			}
			
			getRelatorPos();
			$(window).resize(function(e) {
				getRelatorPos();
            });
			
			function getRelatorPos(){
				global.relatorPos.x=$(dragSlider).find('.ds-relator').offset().left;
				global.relatorPos.y=$(dragSlider).find('.ds-relator').offset().top;
			}
			
			getFixItemWidth();
			$(window).resize(function(e) {
				getFixItemWidth();
            });
			
			function getFixItemWidth(){
				removeItemDraggerStyleAttr();
				
				global.itemWidth=$(dragSlider).find('.ds-item').css('width');
				global.itemWidth=parseInt(global.itemWidth);
				
				fixItemWidthSetDraggerWidth();
			}
			
			function removeItemDraggerStyleAttr(){
				in_view_items=getInViewItemsIndex();
				settings.last_in_view_index=in_view_items[0];

				$(dragSlider).find('.ds-item').removeAttr('style');
				$(dragSlider).find('.ds-dragger').removeAttr('style').removeClass(settings.cssTransitionClass);
			}
			
			function fixItemWidthSetDraggerWidth(){
				$(dragSlider).find('.ds-item').css({
					'width':global.itemWidth
				});
				
				setDraggerWidth();
				setItemCentWidth();
			}
			
			function setDraggerWidth(){
				$(dragSlider).find('.ds-dragger').css({
					'width':global.itemNumber*global.itemWidth
				}).addClass(settings.cssTransitionClass);
			}
			
			function setItemCentWidth(){
				global.itemCentWidth=global.itemWidth/global.relatorWidth*100;
				global.inViewItemNumber=Math.round(100/global.itemCentWidth);
				
				if(global.inViewItemNumber>global.itemNumber){
					global.inViewItemNumber=global.itemNumber;
				}
			}
			
			function getInViewItemsIndex(){
				
				var inViewItemNumber=global.inViewItemNumber;
				var inViewItemIdArray=Array();
				
				var relatorLeft=global.relatorPos.x;
				var relatorWidth=global.relatorWidth;
				
				var arrayKey=0;
				var lastStatus;
				
				for(i=0;i<global.itemNumber;i++){
					
					var currentItemLeft=$(dragSlider).find('.ds-item').eq(i).offset().left;
					var currentItemWidth=global.itemWidth;
					
					var isInView=isInRange(relatorLeft,relatorLeft+relatorWidth,currentItemLeft+parseInt(currentItemWidth/2));
					
					if(isInView==1){
						inViewItemIdArray[arrayKey]=i;
						arrayKey++;
					}
					
					lastStatus=isInView;
					
				}
				
				if(lastStatus=='after' && inViewItemIdArray[0]==null){
					if(settings.itemDirection=='rtl')
						inViewItemIdArray[0]=global.itemNumber-1;
					else
						inViewItemIdArray[0]=0;
				}
				
				else if(lastStatus=='before' && inViewItemIdArray[0]==null){
					if(settings.itemDirection=='rtl')
						inViewItemIdArray[0]=0;
					else
						inViewItemIdArray[0]=global.itemNumber-1;
				}
				
				return inViewItemIdArray;
				
			}
			
			function inBorderStatus(){
				var inViewItems=getInViewItemsIndex();
				var index=inViewItems[0];
				
				if(index==0)
					return 'first';
				
				if(index==global.itemNumber-global.inViewItemNumber)
					return 'last';
				else
					return false;
			}
			
			//new code
			var maxInViewItemNumber=global.relatorWidth/global.itemWidth;
			
			if(global.itemNumber<maxInViewItemNumber)
				settings.draggable=false;
			//new code
			
			var itemsAreInAnimation=false;
			var itemsAnimationTimer;
			
			setItemAnimationTimer();
			$(window).resize(function(e) {
				setItemAnimationTimer();
            });
			function setItemAnimationTimer(){
				if(settings.autoPlay){
					unsetItemAnimationTimer();

					itemsAnimationTimer=setInterval(function(){
						switchItems();
					},settings.switchDelay);
				}
			}
			
			function unsetItemAnimationTimer(){
				clearInterval(itemsAnimationTimer);
			}
			
			var animationIsInReverse=false;
			function switchItems(){
				
				var status=inBorderStatus();
				
				if(settings.itemMovement=='next'){
					
					if(settings.loopType=='reverse' && animationIsInReverse && !status){
						switchToBack();
						return;
					}
					if(settings.loopType=='reverse' && status=='first'){
						animationIsInReverse=false;
						switchToNext();
						return;
					}
					if(settings.loopType=='reverse' && status=='last'){
						animationIsInReverse=true;
						switchToBack();
						return;
					}
					else{
						switchToNext();
						return;
					}
					
				}
				else if(settings.itemMovement=='back'){
					
					if(settings.loopType=='reverse' && animationIsInReverse && !status){
						switchToNext();
						return;
					}
					if(settings.loopType=='reverse' && status=='last'){
						animationIsInReverse=false;
						switchToBack();
						return;
					}
					if(settings.loopType=='reverse' && status=='first'){
						animationIsInReverse=true;
						switchToNext();
						return;
					}
					else{
						switchToBack();
						return;
					}
					
				}
				
			}
			
			function switchToNext(is_in_drag){

				is_in_drag=(typeof is_in_drag!='undefined')?(is_in_drag):(false);

				var inViewItems=getInViewItemsIndex();
				var index=inViewItems[0];
				
				if(index==global.itemNumber-global.inViewItemNumber){
					if(is_in_drag)
						index=global.itemNumber-2;
					else{
						index=-1;
					}
				}
				
				switchDraggerTo(index,settings.switchDuration,false);

			}
			
			function switchToBack(is_in_drag){

				is_in_drag=(typeof is_in_drag!='undefined')?(is_in_drag):(false);
				
				var inViewItems=getInViewItemsIndex();
				var index=inViewItems[0];
				
				if(index==0){
					if(is_in_drag)
						index=-1;
					else
						index=global.itemNumber-global.inViewItemNumber-1;
				}
				else
					index-=2;
				
				switchDraggerTo(index,settings.switchDuration,false);
				
			}
			
			var willBeActiveItems=Array();
			
			setWillBeActiveItems(goToDefaultItem(),0);
			function setWillBeActiveItems(firstIndex,duration){
				willBeActiveItems=Array();
				for(i=0;i<global.inViewItemNumber;i++){
					willBeActiveItems[i]=firstIndex+i;
				}
				onNewActiveItemsDefine(duration);
			}
			
			var ona_timer;
			$(window).resize(function(){
				clearTimeout(ona_timer);
				ona_timer=setTimeout(function(){
					onNewActiveItemsDefine(settings.switchDuration);
				},1000);
			});
			function onNewActiveItemsDefine(duration){
				var tallestHeight=getTallestNewItemHeight();
				
				// $(dragSlider).find('.ds-relator').stop().animate({
				// 	height:tallestHeight
				// },duration);

				$(dragSlider).find('.ds-relator').css({
					height:tallestHeight
				});
			}
			
			function getTallestNewItemHeight(){
				var height=0;
				for(index in willBeActiveItems){
					var currentHeight=$(dragSlider).find('.ds-item').eq(willBeActiveItems[index]).outerHeight(false);
					if(currentHeight>height)
						height=currentHeight;
				}
				return height;
			}
			
			var sdt_timer;
			function switchDraggerTo(index,duration,isInDrag){
				
				var animationJson=Array();
				
				if(global.itemNumber-1-index<global.inViewItemNumber){
					index=global.itemNumber-1-global.inViewItemNumber;
				}
				else if(index+1<0){
					index=-1;
				}



				// var end_legal_index = global.itemNumber - global.inViewItemNumber - 1;

				// // if(index + 1 > end_legal_index){
				// // 	index = end_legal_index - 1;
				// // }
				// // console.clear();
				// console.log(global.inViewItemNumber);
				


				var setHeightDuration=settings.switchDuration;
				
				if(isInDrag){
					setHeightDuration=settings.onDragSwitchDuration;
				}
				
				setWillBeActiveItems(index+1,setHeightDuration);
				animationJson[global.draggerMarginDirection]=(index+1)*global.itemWidth*-1;
				itemsAreInAnimation=true;
				setSwitchActiveTimer();
				
				$(dragSlider).find('.ds-dragger').css(animationJson);
				if(duration==0){
					$(dragSlider).find('.ds-dragger').removeClass(settings.cssTransitionClass);
				}

				clearTimeout(sdt_timer);
				sdt_timer=setTimeout(function(){
					itemsAreInAnimation=false;
					unsetSwitchActiveTimer();
					$(dragSlider).find('.ds-dragger').addClass(settings.cssTransitionClass);
					//lazy load
					checkForLazyLoad();
				},duration);
				
			}



			//lazy load
			function checkForLazyLoad(){
				
				if(settings.lazyLoadSRCElement==''){
					return;
				}
				
				var inViewItems = getInViewItemsIndex();
				
				for(key in inViewItems){
					
					var itemIndex=inViewItems[key];
					
					$(dragSlider).find('.ds-item').eq(itemIndex).each(function(index, element) {
                        
						if($(this).hasClass(settings.viewedClass)){
							return;
						}
						
						$(this).addClass(settings.viewedClass);



						// change src
						$(this).find(settings.lazyLoadSRCElement).each(function(){

							var imgSrc = $(this).data('src');
							$(this).attr('src',imgSrc);
							$(this).bind('load',function(e) {
	                            $(this).addClass(settings.lazyLoadOnLoadImgClass);
	                        });

						});


                    });
					
					
				}
				
			}



			
			$(dragSlider).find('.ds-controller').click(function(e) {
				if($(this).hasClass('next'))
					switchToNext();
				else
					switchToBack();
			});
			
			// $(dragSlider).find('.ds-switch').click(function(e) {
                
			// 	var index=$(this).index();
				
			// 	if(!itemsAreInAnimation){

			// 		unsetItemAnimationTimer();
			// 		index-=parseInt(global.inViewItemNumber/2);

			// 		if(global.inViewItemNumber%2==0)
			// 			index++;
					
			// 		switchDraggerTo(index-1,settings.switchDuration,false);
			// 		setItemAnimationTimer();
					
			// 	}
				
   //          });
			
			var mouseIsInDragger=false;
			$(dragSlider).hover(function(e){
				
				mouseIsInDragger=true;
				if(settings.autoPlay && !draggerClicked && settings.stopOnHover)
					unsetItemAnimationTimer();
				
			},function(e){
				
				mouseIsInDragger=false;
				if(!draggerClicked && settings.stopOnHover)
					setItemAnimationTimer();
				
			});
			
			goToDefaultItem();
			$(window).resize(function(e) {
				goToDefaultItem(true);
            });
			function goToDefaultItem(on_resize){

				on_resize=(typeof on_resize =='undefined')?false:true;

				if(!on_resize){
				
					var defaultItem=settings.firstItem;
					
					if(settings.firstItem=='last')
						defaultItem=global.itemNumber-1;
					else if(settings.firstItem=='middle')
						defaultItem=parseInt(global.itemNumber/2)-parseInt(global.inViewItemNumber/2);
					else
						defaultItem-=2;

					defaultItem-=2;

				}
				else{
					defaultItem=settings.last_in_view_index-1;
				}

				switchDraggerTo(defaultItem,0,false);
				return defaultItem;
				
			}
			
			var switchActiveTimer;
			var switchActiveTimerIntervalDuration=10;
			
			function setSwitchActiveTimer(){
				
				unsetSwitchActiveTimer();
				switchActiveTimer=setInterval(function(){
					setSwitchActive();
				},switchActiveTimerIntervalDuration);
				
			}
			
			function unsetSwitchActiveTimer(){
				clearInterval(switchActiveTimer);
			}
			
			setSwitchActive();
			$(window).resize(function(e) {
				setSwitchActive();
            });
			
			function setSwitchActive(){
				
				var inViewItems=getInViewItemsIndex();
				
				$(dragSlider).find('.ds-switch').removeClass('active');
				for(key in inViewItems){
					$(dragSlider).find('.ds-switch').eq(inViewItems[key]).addClass('active');
				}
				
			}
			
			var draggerClicked=false;
			var draggerClickedMouseX;
			var draggerClickedUpMouseX;
			var draggerClickedSidePos;
			var draggerIsInDragging=false;
			
			var unsetActiveSwitchTTimer;
			var mouseDownTime;
			
			if(settings.draggable){

				var eStart=hasTouch?'touchstart':'mousedown';
			   	var eMove=hasTouch?'touchmove':'mousemove';
			   	var eEnd=hasTouch?'touchend':'mouseup';
			  	var eCancel=hasTouch?'touchcancel':'mouseup';

		        if(hasTouch){
	                // $(dragSlider).find('.ds-dragger')[0].addEventListener(eStart,drag_start_event,false);
	                // window.addEventListener(eMove,drag_move_event,false);
	                // window.addEventListener(eEnd,drag_end_event,false);
	                // window.addEventListener(eCancel,drag_end_event,false);
	            }
	            else{
	                $(dragSlider).find('.ds-dragger').on(eStart,drag_start_event);
	                window.addEventListener(eMove,drag_move_event);
	                window.addEventListener(eEnd,drag_end_event);
	            }
			
				// $(dragSlider).find('.ds-dragger').bind('mousedown',drag_start_event);
				// $(document).bind('mousemove',drag_move_event);
				// $(document).bind('mouseup',drag_end_event);
				
				$(dragSlider).find('a').click(function(e) {
					if(draggerIsInDragging){
						return false;
					}
	            });
				
			}

			// drag event
			function drag_start_event(e){

				if(e.preventDefault!==undefined)
                	e.preventDefault();
				else
                	e.returnValue=false;
				
				clearTimeout(unsetActiveSwitchTTimer);
				
				mouseDownTime=new Date();
				
				draggerClickedMouseX=(hasTouch)?(e.touches[0].pageX):(e.pageX);

				draggerClicked=true;
				draggerClickedSidePos=$(dragSlider).find('.ds-dragger').css(global.draggerMarginDirection);
				
				setSwitchActiveTimer();
				unsetItemAnimationTimer();
				
				$('body').addClass('dragging');
				$(dragSlider).find('.ds-dragger').removeClass(settings.cssTransitionClass);

			}
			function drag_move_event(e){

				// console.log(e);

				if(!draggerClicked)
					return;
				
				draggerIsInDragging=true;
				
				var dragDistance=(hasTouch)?(e.touches[0].pageX):(e.pageX);
				dragDistance-=draggerClickedMouseX;
				
				if(settings.itemDirection=='rtl')
					dragDistance*=-1;
				
				var maxDraggerSidePos=(global.itemNumber*global.itemWidth)-global.relatorWidth;
				
				var newDraggerSidePos=dragDistance+parseInt(draggerClickedSidePos);
				
				if(newDraggerSidePos>0){
					newDraggerSidePos*=settings.dragEndFactor;
				}
				else if(newDraggerSidePos<maxDraggerSidePos*-1){
					
					var offset=newDraggerSidePos+maxDraggerSidePos;
					offset*=settings.dragEndFactor;
					newDraggerSidePos=maxDraggerSidePos*-1+offset;
					
				}
				
				var animationJson=Array();
				animationJson[global.draggerMarginDirection]=newDraggerSidePos;
				$(dragSlider).find('.ds-dragger').css(animationJson,0);

			}
			function drag_end_event(e){

				if(!draggerClicked)
					return;
					
				setTimeout(function(){
					draggerIsInDragging=false;
				},10);


				// fix chrome bug
				try{
					draggerClickedUpMouseX=(hasTouch)?(e.touches[0].pageX):(e.pageX);
				}
				catch(err){
					draggerClickedUpMouseX=(hasTouch)?(e.changedTouches[0].pageX):(e.pageX);
				}
				// fix chrome bug


				var draggedDistance=draggerClickedUpMouseX-draggerClickedMouseX;
				

				if(Math.abs(draggedDistance)<=10)
					draggerIsInDragging=false;
				
				var mouseUpTime=new Date();
				
				var dragDuration=mouseUpTime.getTime()-mouseDownTime.getTime();
				
				if(settings.dragSwitchMaxDistance){
					
					if(draggedDistance>=settings.dragSwitchMinDistance){
						
						if(draggedDistance<=settings.dragSwitchMaxDistance && dragDuration<=settings.dragValidTimer){
							
							if(settings.itemDirection=='ltr')
								switchToBack(true);
							else
								switchToNext(true);

						}
						else{
							var activeItems=getInViewItemsIndex();
							switchDraggerTo(activeItems[0]-1,settings.onDragSwitchDuration,true);
						}
					
					}
					else if(draggedDistance<=settings.dragSwitchMinDistance*-1){
						
						if(draggedDistance>=settings.dragSwitchMaxDistance*-1 && dragDuration<=settings.dragValidTimer){
							
							if(settings.itemDirection=='ltr')
								switchToNext(true);
							else
								switchToBack(true);
						
						}
						else{
							var activeItems=getInViewItemsIndex();
							switchDraggerTo(activeItems[0]-1,settings.onDragSwitchDuration,true);
						}
						
					}
					else{
						var activeItems=getInViewItemsIndex();
						switchDraggerTo(activeItems[0]-1,settings.onDragSwitchDuration,true);
					}
					
				}
				
				draggerClickedMouseX=0;
				draggerClicked=false;
				draggerClickedSidePos=0;
				
				clearTimeout(unsetActiveSwitchTTimer);
				
				var finalTimeOffsetActiveTimerReset=settings.switchDuration;
				
				if(settings.onDragSwitchDuration>settings.switchDuration)
					finalTimeOffsetActiveTimerReset=settings.onDragSwitchDuration;
				
				finalTimeOffsetActiveTimerReset+=100;
				
				unsetActiveSwitchTTimer=setTimeout(function(){
					unsetSwitchActiveTimer();
				},finalTimeOffsetActiveTimerReset);
				
				if(settings.autoPlay && !mouseIsInDragger)
					setItemAnimationTimer();
			
				$('body').removeClass('dragging');
				$(dragSlider).find('.ds-dragger').addClass(settings.cssTransitionClass);

			}
			// drag event
			
			function getNegOrPos(){
				return (Math.random()>0.5)?1:-1;
			}
			
			function getRandomInRange(minRange, maxRange){
				var randomVal;
				do{
					randomVal=Math.random()*maxRange;
				}while(randomVal<minRange || randomVal>maxRange);
				return randomVal;
			}
			
			function isInRange(one,two,number){
				
				if(number>=one && number<=two)
					return 1;
				else
					if(number>=two)
						return 'after';
					else if(number<=one)
						return 'before';
				
			}
			
			//new plugin code, the best!
			plugin={
				refreshHeight:function(){
					onNewActiveItemsDefine(settings.switchDuration);
				},
				refreshSettings:function(){
					// $(window).resize();
				},
				switchNext:function(){
					switchToNext();
				},
				switchBack:function(){
					switchToBack();
				}
			}
			//new plugin code, the best!
			
		});
		
		//new plugin code, the best!
		return plugin;
		//new plugin code, the best!
		
	}
	
}(jQuery));