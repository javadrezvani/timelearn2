/*
 * Alert Message - jQuery plugin for alert script message
 * Version: 1.0.1
 */

(function($){
    
	//$.alertMessage('title','context',0,'info || success || error');
	
	$.alertMessage=function(alert_title_char, alert_text_char, auto_close, customClass){
		
		var alert_shell_html="<div class='alert_shell'><div class='alert_block'><div class='head'><h1 class='alert_title'></h1><div class='close_button'></div></div><div class='content'><div class='text_block'></div></div></div></div>";
		
		var test_alert_shell=$('.alert_shell');
		
		if(!test_alert_shell.is('.alert_shell'))
			$('body').prepend(alert_shell_html);
		
		var alert_shell=$('.alert_shell');
		var alert_block=$('.alert_shell>.alert_block');
		var alert_title=$('.alert_shell>.alert_block>.head>.alert_title');
		var close_button=$('.alert_shell>.alert_block>.head>.close_button');
		var text_block=$('.alert_shell>.alert_block>.content>.text_block');
		
		alert_message(alert_title_char,alert_text_char,auto_close);
		
		function alert_message(alert_title_char,alert_text_char,auto_close){
			
			alert_title.html(alert_title_char);
			text_block.html(alert_text_char);
			
			setTimeout(function(){
				
				show_alert_shell();
				set_alert_block_middle();
				
				if(auto_close>=2000){
					
					setTimeout(function(){
						hide_alert_shell();
					},auto_close);
					
				}
					
			},100);
			
		}
		
		function show_alert_shell(){
			
			alert_shell.css('display','block');
			alert_shell.animate({opacity:1},300);
			alert_shell.addClass(customClass);
			alert_block.addClass('active');

			setKeyboardEvent();
			
		}
		
		function hide_alert_shell(){
			
			alert_shell.removeClass(customClass);
			alert_block.removeClass('active');
			alert_shell.animate({opacity:0},300,function(){
			alert_shell.css('display','none');
				
		});
			
		}
		
		function set_alert_block_middle(){
			
			var alert_block_height=alert_block.outerHeight(false);
			alert_block.css('margin-top',alert_block_height/-2);
			
		}
		
		close_button.click(function(e) {
			hide_alert_shell();
		});

		function setKeyboardEvent(){
			$(document).keydown(function(e){
				if(e.keyCode==27)
					hide_alert_shell();
			});
		}
		
	}
	
}(jQuery));